class TimerLogEntry:
    def __init__(self, date, timer_hash, round, end, action, description):
        self._date = date
        self._timer_hash = timer_hash
        self._round = round
        self._end = end
        self._action = action
        self._description = description

    def __repr__(self):
        return f"TimerLogEntry(timer_hash={self._timer_hash}, round={self._round}, end={self._end},action={self._action}, description={self._description})"

    def to_dict(self):
        return {
            "timer_hash": self._timer_hash,
            "round": self._round,
            "end": self._end,
            "action": self._action,
            "description": self._description,
        }


import csv

timer_logs = []


def add_log(date, timer_hash, round, end, action, description):
    timer_logs.append(TimerLogEntry(date, timer_hash, round, end, action, description))


def init_logs():
    with open("data/timer_logger.log", "r") as f:
        reader = csv.reader(f)
        for row in reader:
            add_log(row[0], row[1], row[2], row[3], row[4], row[5])
