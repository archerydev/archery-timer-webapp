from settings import settings
from PIL import ImageColor
import copy

default_colors = {
    "BLACK": (0, 0, 0, 255),
    "GREEN": (0, 255, 0, 255),
    "RED": (255, 0, 0, 255),
    "YELLOW": (255, 223, 0, 255),
    "ORANGE": (239, 127, 26, 255),
}

colors = copy.deepcopy(default_colors)
if settings.USE_REDIS:
    from controllers.redis_controller import RedisClient

    client = RedisClient()
    server = client.conn
    colors = server.hgetall("colors")
    if len(colors) == 0:
        colors = copy.deepcopy(default_colors)


def save_colors():
    if settings.USE_REDIS:
        server.hmset("colors", colors)


def get_color(color):
    if color in colors:
        return colors[color]
    return colors["RED"]


def set_color(color, value):
    colors[color] = value
    if settings.USE_REDIS:
        server.hmset("colors", colors)


def get_color_as_hex(color):
    return "#%02x%02x%02x%02x" % color


def colors_data_as_json():
    return [{"name": k, "color": get_color_as_hex(v)} for (k, v) in colors.items()]


def set_colors_from_json(colors_json):
    for c in colors_json:
        colors[c["name"]] = ImageColor.getrgb(c["color"])
    save_colors()


def set_colors(new_colors):
    global colors
    colors = new_colors
    save_colors()


def reset_colors():
    set_colors(copy.deepcopy(default_colors))
