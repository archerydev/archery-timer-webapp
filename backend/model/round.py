__author__ = "gpt"
import json

from .end import End


class Round(object):
    """
    ArcheryTimer round
    """

    def __init__(self, name, ends):
        self._name = name
        self._ends = ends

    @classmethod
    def normal(cls, name, num_ends, end_type_odd, end_type_even):
        ends = []
        for end_number in range(0, num_ends):
            if end_number % 2 == 1 and end_type_even != None:
                ends.append(end_type_even)
            else:
                ends.append(end_type_odd)
        return cls(name, ends)

    def get_num_ends(self):
        return len(self._ends)

    @property
    def ends(self):
        return self._ends

    def as_data(self):
        data = {}
        data["name"] = self._name
        data["num_ends"] = self.get_num_ends()
        data["ends"] = [end.as_data() for end in self._ends]
        return data

    def as_json_string(self):
        return json.dumps(self.as_data())

    @classmethod
    def from_json_dict(cls, rd):
        if rd is None:
            return None
        return Round(rd["name"], [End.from_json_dict(end) for end in rd["ends"]])

    def add_end(self, end, pos):
        self._ends.insert(pos, end)
