from .phase import (
    Phase,
    Coloring,
    coloring_red,
    sound_2_end,
    coloring_30,
    sound_3_end,
    coloring_black,
    sound_10,
    sound_no_sound,
    sound_1_end,
    coloring_timer,
)
import json

__author__ = "gpt"


class End(object):
    """
    Clase que representa una entrada
    """

    def __init__(self, name, phases):
        self._name = name
        self._phases = phases

    @classmethod
    def four_archers(cls, init_time, first, last):
        a = first[0]
        b = first[1]
        c = last[0]
        d = last[1]
        phases = [
            Phase(
                "Inicio",
                10,
                10,
                coloring_black,
                a,
                coloring_red,
                b,
                coloring_red,
                sound_no_sound,
                False,
                True,
                "Próximo turno",
                first,
            ),
            Phase.same_letter_colors(
                "Llamada " + first,
                10,
                0,
                a,
                b,
                coloring_red,
                Coloring.sounds_number(10, 2),
                False,
            ),
            Phase.same_letter_colors(
                first,
                init_time,
                0,
                a,
                b,
                coloring_30,
                Coloring.sounds_number(init_time, 1),
            ),
            Phase.same_letter_colors(
                "Llamada " + last,
                10,
                0,
                c,
                d,
                coloring_red,
                Coloring.sounds_number(10, 2),
                False,
            ),
            Phase.same_letter_colors(
                last,
                init_time,
                0,
                c,
                d,
                coloring_30,
                Coloring.sounds_number(init_time, 1),
            ),
            Phase(
                "Fin",
                0,
                0,
                coloring_black,
                c,
                coloring_red,
                d,
                coloring_red,
                sound_3_end,
                False,
            ),
        ]
        return cls("4ArchersEnd", phases)

    @classmethod
    def two_archers(cls, init_time, first):
        a = first[0]
        b = first[1]
        phases = [
            Phase(
                "Inicio",
                10,
                10,
                coloring_black,
                a,
                coloring_red,
                b,
                coloring_red,
                sound_no_sound,
                False,
                True,
                "Próximo turno",
                first,
            ),
            Phase.same_letter_colors(
                "Llamada " + first,
                10,
                0,
                a,
                b,
                coloring_red,
                Coloring.sounds_number(10, 2),
                False,
            ),
            Phase.same_letter_colors(
                first,
                init_time,
                0,
                a,
                b,
                coloring_30,
                Coloring.sounds_number(init_time, 1),
            ),
            Phase(
                "Fin",
                0,
                0,
                coloring_black,
                a,
                coloring_red,
                b,
                coloring_red,
                sound_3_end,
                False,
            ),
        ]
        return cls("2ArchersEnd", phases)

    @classmethod
    def countdown(cls, init_time, name="", finish_text=""):
        phases = [
            Phase(
                "Timer",
                init_time,
                0,
                coloring_timer,
                "",
                coloring_black,
                "",
                coloring_black,
                sound_no_sound,
                True,
                True,
                name,
            ),
            Phase(
                "Fin",
                0,
                0,
                coloring_timer,
                "",
                coloring_black,
                "",
                coloring_black,
                sound_no_sound,
                False,
                False,
                finish_text,
            ),
        ]
        return cls("Coundown", phases)

    # @classmethod
    # def untie(cls, num_unties, init_time, first, last = None):
    #     a = first[0]
    #     b = first[1]
    #     if last != None:
    #         c = last[0]
    #         d = last[1]
    #         untie_second = Phase.same_letter_colors("Desempate " + last, init_time, 0, c, d, coloring_red, sound_1_end)
    #     untie_first = Phase.same_letter_colors("Desempate " + first, init_time, 0, a, b, coloring_red, sound_1_end)
    #     phases = []
    #     for _ in range(0, num_unties-1):
    #         phases.append(untie_first)
    #         if last != None:
    #             phases.append(untie_second)
    #     return cls("Untie", phases)

    @classmethod
    def classification(cls, division, classification):
        phases = []
        phases.append(
            Phase(
                division,
                0,
                0,
                coloring_black,
                "A",
                coloring_black,
                "B",
                coloring_black,
                sound_no_sound,
                False,
                True,
                division,
            )
        )
        classification.sort(key=lambda x: x[0], reverse=True)
        for (position, archer, club) in classification:
            if archer and archer != "":
                phases.append(Phase.classification_phase(position, archer, club))
        phases.append(
            Phase(
                "Podium",
                0,
                0,
                coloring_black,
                "A",
                coloring_black,
                "B",
                coloring_black,
                sound_no_sound,
                False,
                True,
                "Podium",
            )
        )
        return cls(division, phases)

    @property
    def phases(self):
        return self._phases

    @property
    def name(self):
        return self._name

    def get_phases_name_list(self):
        return [phase.name for phase in self._phases]

    def as_data(self):
        data = {}
        data["name"] = self._name
        data["phases"] = [phase.as_data() for phase in self._phases]
        return data

    def as_json_string(self):
        return json.dumps(self.as_data())

    @classmethod
    def from_json_dict(cls, ed):
        if ed is None:
            return None
        return End(ed["name"], [Phase.from_json_dict(phase) for phase in ed["phases"]])


if __name__ == "__main__":
    my_test = End.four_archers(240, "AB", "CD")
    print(my_test)
