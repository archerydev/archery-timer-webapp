import jwt
from datetime import datetime, timedelta

SECRET_KEY = (
    "super-secret"
)  # "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"
TOKEN_SUBJECT = "access"
ACCESS_TOKEN_EXPIRE_MINUTES = 300


class User:
    def __init__(self, id, username, password):
        self._user_id = id
        self._username = username
        self._password = password

    @property
    def password(self):
        return self._password

    @property
    def username(self):
        return self._username

    @property
    def user_id(self):
        return self._user_id

    def __repr__(self):
        return "User(id='{}')".format(self._user_id)

    def to_dict(self):
        return {"user_id": self._user_id, "username": self._username}


users = [User(1, "user", "password")]
username_table = {u.username: u for u in users}
userid_table = {u.user_id: u for u in users}

def validate_credentials(username, password):
    if username in username_table:
        if username_table.get(username).password == password:
            return ""
        return "wrong password"
    return "wrong user"

def create_access_token(*, data: dict, expires_delta: timedelta = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=300)
    # TODO: proper handle the jwt expiration
    to_encode.update({"exp": expire, "sub": TOKEN_SUBJECT})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt

def generate_token(username):
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"user": username}, expires_delta=access_token_expires
    )
    response_details = {
        "token": access_token.decode(),
        "token_type": "Bearer",
        "expiresIn": ACCESS_TOKEN_EXPIRE_MINUTES * 60,
    }
    return response_details
