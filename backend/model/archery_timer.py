import hashlib
import json
import time
from .end import End
from .round import Round
from collections import defaultdict

__author__ = "gpt"


TOURNAMENT = "Tournament_timer"
PLAYOFF = "PlayOff_timer"
WARMING = "Warming_timer"
ARROW = "Arrow_timer"
TIMER = "Normal_timer"
CLASSIFICATION = "Classification_timer"


class Status:
    STOPPED = 0
    RUNNING = 1
    PAUSED = 2


class ArcheryTimer(object):
    """
    Basic class that represents a Timer.
    """

    def __init__(
        self,
        name,
        rounds=[],
        num_letters=4,
        hash_code=None,
        init_number=-1,
        timer_type=TOURNAMENT,
    ):
        self._name = name
        self._rounds = rounds
        self._num_letters = num_letters
        if hash_code == None:
            hash = hashlib.sha1()
            hash.update(str(time.time()).encode("utf-8"))
            self._hash_code = hash.hexdigest()[:6]
        else:
            self._hash_code = hash_code
        self._init_number = init_number
        self._timer_type = timer_type

    @property
    def name(self):
        return self._name

    @property
    def rounds(self):
        return self._rounds

    @property
    def num_letters(self):
        return self._num_letters

    @property
    def hash_code(self):
        return self._hash_code

    @property
    def init_number(self):
        return self._init_number

    @property
    def timer_type(self):
        return self._timer_type

    def as_template(self):
        data = {}
        data["name"] = self._name
        data["timer_hash"] = self._hash_code
        data["rounds"] = len(self._rounds)
        data["ends"] = self._rounds[0].get_num_ends()
        data["letters"] = self._num_letters
        data["number"] = self._init_number
        data["timer_type"] = self._timer_type
        return data

    def as_data(self):
        data = {}
        data["name"] = self._name
        data["timer_hash"] = self._hash_code
        data["rounds"] = [round.as_data() for round in self._rounds]
        data["letters"] = self._num_letters
        data["number"] = self._init_number
        data["timer_type"] = self._timer_type
        return data

    def as_json_string(self):
        return json.dumps(self.as_template())

    @classmethod
    def from_json_dict(cls, td):
        r = [Round.from_json_dict(round) for round in td["rounds"]]
        return cls(
            td["name"],
            rounds=r,
            num_letters=td["letters"],
            hash_code=td["timer_hash"],
            init_number=td["number"],
            timer_type=td["timer_type"],
        )

    @classmethod
    def create_standard_tournament_timer(
        cls,
        num_rounds,
        num_ends,
        num_letters,
        init_time,
        name="Estandar Timer",
        hash_code=None,
    ):
        """
        Creates a Standard tournament with equal ends for each round
        :param num_rounds: int with the number of rounds for the tournament
        :param num_ends: int with the number of ends for each rounds (we asume same ends for each round)
        :param num_warming: int with the number of warming ends that are going to be on the first round
        :param num_letters: int > 0 and <= 4 with the number of archers for each round
        :param init_time: The duration of the shoot end.
        :return: The created tournamnet
        """
        if num_letters > 2:
            end1 = End.four_archers(init_time, "AB", "CD")
            end2 = End.four_archers(init_time, "CD", "AB")
        elif num_letters <= 2:
            end1 = End.two_archers(init_time, "AB")
            end2 = None

        rounds = []
        for i in range(0, num_rounds):
            rounds.append(Round.normal(str(i + 1), num_ends, end1, end2))

        return cls(name, rounds, num_letters, hash_code, init_time, TOURNAMENT)

    @classmethod
    def create_standard_warming_timer(
        cls, num_warmings, num_letters, init_time, name="Warming Timer", hash_code=None
    ):
        """
        Creates a Standard warming timer
        :param num_warmings: int with the number of warmings to generate
        :param num_letters: int > 0 and <= 4 with the number of archers
        :param init_time: The duration of the shoot end.
        :return: The created timer
        """
        if num_letters > 2:
            end1 = End.four_archers(init_time, "AB", "CD")
        elif num_letters <= 2:
            end1 = End.two_archers(init_time, "AB")

        rounds = []
        rounds.append(Round.normal("Warming", num_warmings, end1, None))

        return cls(name, rounds, num_letters, hash_code, init_time, WARMING)

    @classmethod
    def create_arrow_timer(
        cls, num_letters, init_time, name="Arrow Timer", hash_code=None
    ):
        """
        Creates a Standard arrow timer
        :param num_letters: int > 0 and <= 4 with the number of archers
        :param init_time: The duration of the shoot end.
        :return: The created timer
        """
        if num_letters > 2:
            end1 = End.four_archers(init_time, "RE", "RE")
        elif num_letters <= 2:
            end1 = End.two_archers(init_time, "RE")

        rounds = []
        rounds.append(Round.normal("Arrow", 1, end1, None))

        return cls(name, rounds, num_letters, hash_code, init_time, ARROW)

    @classmethod
    def create_timer(cls, init_time, name="Timer", end_text="", hash_code=None):
        """
        Creates a Standard countdown timer
        :param init_time: The duration of the timer.
        :return: The created timer
        """
        end1 = End.countdown(init_time, name, end_text)

        rounds = []
        rounds.append(Round.normal("Timer", 1, end1, None))

        return cls(name, rounds, 2, hash_code, init_time, TIMER)

    @classmethod
    def load_classification(
        cls, classification, name="Classification", end_text="", hash_code=None
    ):
        """
        Creates a Standard countdown timer
        :param init_time: The duration of the timer.
        :return: The created timer
        """
        rounds = []
        ends = [
            End.classification(division, classification[division])
            for division in classification
            if division != None
        ]
        if len(ends) > 0:
            rounds.append(Round("classification", ends))
            return ArcheryTimer(name, rounds, 2, hash_code, -1, CLASSIFICATION)
        return None

    @classmethod
    def load_classification_from_json(cls, class_json, hash_code=None):
        divisions = defaultdict(list)
        class_json.sort(key=lambda x: x["div_order"])
        for c in class_json:
            divisions[c["division"]].append((c["order"], c["contender"], c["club"]))
        return cls.load_classification(divisions, hash_code)

    @classmethod
    def create_standard_playoff_timer(
        cls, min_number, num_letters, init_time, name="Playoff Timer", hash_code=None
    ):

        if num_letters > 2:
            end1 = End.four_archers(init_time, "AB", "CD")
        elif num_letters <= 2:
            end1 = End.two_archers(init_time, "AB")
        end1._name = str(min_number)
        rounds = []
        rounds.append(Round.normal("Playoff", 1, end1, None))

        return cls(name, rounds, num_letters, hash_code, init_time, PLAYOFF)

    def add_end_playoff_timer(self, min_number, num_letters):
        round = self.rounds[0]
        last_end = round.ends[round.get_num_ends() - 1]

        if min_number != None:
            end_name = str(int(last_end.name) + 1)
            init_number = self.init_number
        else:
            end_name = "Untie"
            init_number = 40

        if num_letters > 2:
            if round.get_num_ends() % 2 == 0:
                end1 = End.four_archers(init_number, "AB", "CD")
            else:
                end1 = End.four_archers(init_number, "CD", "AB")
        elif num_letters <= 2:
            end1 = End.two_archers(init_number, "AB")

        end1._name = end_name
        round.ends.append(end1)
