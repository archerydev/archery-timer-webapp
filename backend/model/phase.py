import json


class Status:
    STOPPED = 0
    RUNNING = 1
    PAUSED = 2


class Coloring(object):
    def __init__(self, name, limits, colors, is_sound=False):
        self._name = name
        self._limits = limits
        self._colors = colors
        self._is_sound = is_sound

    @classmethod
    def sounds_number(cls, number, sounds):
        return cls(f"{sounds}_{number}", [number], [sounds, 0], True)

    def get_color(self, number, status=Status.STOPPED):
        """
        Given a time value, returns the color
        """
        if self._is_sound:
            return "BLACK"
        if status != Status.RUNNING:
            return "RED"

        i = 0
        for limit in self._limits:
            if number <= limit:
                return self._colors[i]
            i = i + 1
        return self._colors[i]

    def get_sounds(self, number, status):
        """
        Given a number, returns the number of beeps
        """
        if not self._is_sound:
            return 0

        if status == Status.STOPPED:
            return 0
        i = 0
        for limit in self._limits:
            if number == limit:
                return self._colors[i]
            i = i + 1
        return self._colors[i]

    def as_data(self):
        data = {}
        data["name"] = self._name
        data["limits"] = self._limits
        data["colors"] = self._colors
        return data

    def as_json_string(self):
        return json.dumps(self.as_data())

    @classmethod
    def from_json_dict(cls, cd, is_sound=False):
        return Coloring("dummy", cd["limits"], cd["colors"], is_sound)


coloring_30 = Coloring("30", [30], ["YELLOW", "GREEN"], False)
coloring_red = Coloring("red", [], ["RED"], False)
coloring_black = Coloring("black", [], ["BLACK"], False)
coloring_timer = Coloring("timer", [60, 300], ["RED", "YELLOW", "GREEN"], False)
sound_10 = Coloring("sound_10", [10, 0], [2, 1, 0], True)
sound_1_end = Coloring("sound_1_end", [0], [1, 0], True)
sound_2_end = Coloring("sound_2_end", [0], [2, 0], True)
sound_3_end = Coloring("sound_3_end", [0], [3, 0], True)
sound_3_end_phase = Coloring("sound_3_end", [0], [3, 0], True)
sound_no_sound = Coloring("no_sound", [], [0], True)


class Phase(object):
    """
    One of the phases of an end
    """

    def __init__(
        self,
        name,
        init_value,
        end_value,
        number_coloring,
        first_letter,
        first_letter_coloring,
        second_letter,
        second_letter_coloring,
        sounds_coloring,
        can_be_finished=True,
        start_stopped=False,
        text=None,
        secondary_text=None,
    ):
        """
        :param name: string, Descripcion del Turno
        init_value: int, El numero inicial del turno
        end_value: int, El numero final del turno
        number_coloring: Coloring para el numero
        first_letter: char, el valor de la primera letra
        first_letter_coloring: Coloring para la primera letra
        second_letter: char, el valor de la segunda letra
        second_letter_coloring: Coloring para la segunda letra
        sound_limits_values: array de ints, representa los valores a los que el marcador debe emitir un sonido
        sounds: array de ints, representa el numero de sonidos que debe emitir en los valores dados por la variable anterior.
                A diferencia de los colores solo emitira el sonido cuando el valor sea exactamente igual, no por intervalos
        start_stopped: bool, true if the phase must stop the timer at the begining, false otherwise
        """
        self._name = name
        self._init_value = init_value
        self._end_value = end_value
        self._number_coloring = number_coloring
        self._first_letter = first_letter
        self._first_letter_coloring = first_letter_coloring
        self._second_letter = second_letter
        self._second_letter_coloring = second_letter_coloring
        self._sounds_coloring = sounds_coloring
        self._can_be_finished = can_be_finished
        self._start_stopped = start_stopped
        self._text = text
        self._secondary_text = secondary_text

    @classmethod
    def same_letter_colors(
        cls,
        name,
        init_value,
        end_value,
        first_letter,
        second_letter,
        panel_coloring,
        sounds_coloring,
        can_be_finished=True,
        start_stopped=False,
        text=None,
        secondary_text=None,
    ):
        return cls(
            name,
            init_value,
            end_value,
            panel_coloring,
            first_letter,
            panel_coloring,
            second_letter,
            panel_coloring,
            sounds_coloring,
            can_be_finished,
            start_stopped,
            text,
            secondary_text,
        )

    @classmethod
    def classification_phase(cls, order, archer, club):
        return cls(
            order,
            0,
            0,
            coloring_red,
            "A",
            coloring_red,
            "B",
            coloring_red,
            sound_no_sound,
            False,
            True,
            archer,
            club,
        )

    @property
    def name(self):
        return self._name

    @property
    def init_value(self):
        return self._init_value

    @property
    def end_value(self):
        return self._end_value

    @property
    def first_letter(self):
        return self._first_letter

    @property
    def second_letter(self):
        return self._second_letter

    @property
    def number_coloring(self):
        return self._number_coloring

    @property
    def first_letter_coloring(self):
        return self._first_letter_coloring

    @property
    def second_letter_coloring(self):
        return self._second_letter_coloring

    @property
    def sounds_coloring(self):
        return self._sounds_coloring

    @property
    def can_be_finished(self):
        return self._can_be_finished

    @property
    def start_stopped(self):
        return self._start_stopped

    @property
    def text(self):
        return self._text

    @property
    def secondary_text(self):
        return self._secondary_text

    def as_data(self):
        data = {}
        data["name"] = self._name
        data["init_value"] = self._init_value
        data["end_value"] = self._end_value
        data["first_letter"] = self._first_letter
        data["first_letter_coloring"] = self._first_letter_coloring.as_data()
        data["second_letter"] = self._second_letter
        data["second_letter_coloring"] = self._second_letter_coloring.as_data()
        data["number_coloring"] = self._number_coloring.as_data()
        data["sounds_coloring"] = self._sounds_coloring.as_data()
        data["can_be_finished"] = self._can_be_finished
        data["start_stopped"] = self.start_stopped
        data["text"] = self._text
        data["secondary_text"] = self._secondary_text
        return data

    def as_json_string(self):
        return json.dumps(self.as_data())

    @classmethod
    def from_json_dict(cls, pd):
        return Phase(
            pd["name"],
            pd["init_value"],
            pd["end_value"],
            Coloring.from_json_dict(pd["number_coloring"]),
            pd["first_letter"],
            Coloring.from_json_dict(pd["first_letter_coloring"]),
            pd["second_letter"],
            Coloring.from_json_dict(pd["second_letter_coloring"]),
            Coloring.from_json_dict(pd["sounds_coloring"], is_sound=True),
            pd["can_be_finished"],
            pd["start_stopped"],
            pd["text"],
            pd["secondary_text"],
        )

    def fill_timer_order(self, timer_order, status=None):
        number = timer_order["number"]
        timer_order["number_color"] = "".join(
            self._number_coloring.get_color(number, status)
        )
        timer_order["turn1"] = "".join(self._first_letter)
        timer_order["turn1_color"] = "".join(
            self._first_letter_coloring.get_color(number, status)
        )
        timer_order["turn2"] = "".join(self._second_letter)
        timer_order["turn2_color"] = "".join(
            self._second_letter_coloring.get_color(number, status)
        )
        timer_order["sound"] = self._sounds_coloring.get_sounds(number, status)
        timer_order["text"] = self._text if status == 0 else None
        timer_order["secondary_text"] = self._secondary_text
