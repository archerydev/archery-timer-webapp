from settings import settings
import pathlib


class Club:
    # TODO: Add check if the image doesn't exist
    def __init__(self, id, name, image_path):
        self._id = id
        self._name = name
        self._image_path = (
            (settings.DATA_PATH / image_path).relative_to(settings.DATA_PATH).as_posix()
        )

    @property
    def name(self):
        return self._name

    @property
    def image_path(self):
        return self._image_path

    def __repr__(self):
        return f"Club(name={self._name}, image_path={self._image_path})"

    def to_dict(self):
        return {"id": self._id, "name": self._name, "image_path": self._image_path}

    @staticmethod
    def from_json(club_json):
        return Club(club_json["id"], club_json["name"], club_json["image"])


import csv

last_index = 0
clubs_dict = {}
csv.register_dialect(
    "myDialect", delimiter=";", quoting=csv.QUOTE_NONE, skipinitialspace=True
)
clubs_file = settings.DATA_PATH / "clubs.csv"


def save_to_disk():
    with clubs_file.open(encoding="utf8", mode="w", newline="") as f:
        writer = csv.writer(f, dialect="myDialect")
        for club in clubs_dict.values():
            writer.writerow(club.to_dict().values())


def add_club(name, image_path, index=None, save=True):
    global last_index, clubs_dict
    if index:
        club = Club(index, name, image_path)
        clubs_dict[index] = club
    else:
        club = Club(last_index, name, image_path)
        clubs_dict[last_index] = club
        last_index += 1
    if save:
        save_to_disk()
    return club


def update_club(index, name, image_path):
    updated_club = Club(index, name, image_path)
    clubs_dict[index] = updated_club
    save_to_disk()
    return clubs_dict[index]


def delete_club(index):
    deleted_club = clubs_dict.pop(index, None)
    save_to_disk()
    return deleted_club


def get_club_by_name(name):
    return next(
        iter([clubs_dict[key] for key in clubs_dict if clubs_dict[key].name == name]),
        clubs_dict[0],
    )


def init_clubs():
    global last_index
    clubs_file = settings.DATA_PATH / "clubs.csv"
    with clubs_file.open(encoding="utf8") as f:
        reader = csv.reader(f, delimiter=";")
        last_index = 0
        for row in reader:
            add_club(row[1], row[2], int(row[0]), save=False)
            if last_index <= int(row[0]):
                last_index = int(row[0]) + 1


def clubs_data_as_json():
    return [clubs_dict[key].to_dict() for key in clubs_dict]


init_clubs()
