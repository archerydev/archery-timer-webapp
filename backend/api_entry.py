from apispec import APISpec
from starlette.applications import Starlette
from starlette.middleware.authentication import AuthenticationMiddleware
from starlette.middleware.cors import CORSMiddleware
from starlette.staticfiles import StaticFiles
from starlette.responses import FileResponse
from starlette_jwt import JWTAuthenticationBackend
from starlette_apispec import APISpecSchemaGenerator
from apispec.ext.marshmallow import MarshmallowPlugin
from marshmallow import Schema, fields


import uvicorn
import logging

from api.user import user, config_spec as user_config
from api.timer import timer, config_spec as timer_config
from api.color import color
from api.canvas import canvas, config_spec as canvas_config
from api.log import log
from api.club import club
from api.system import system


from settings import settings

logger = logging.getLogger("uvicorn")
logger.setLevel(logging.DEBUG)
if settings.LOG_TO_FILE:
    from settings.settings import file_logger_file_handler

    logger.addHandler(file_logger_file_handler)


spec = APISpec(
    title="Archery Timer API",
    version="1.0",
    openapi_version="3.0.2",
    info={"description": "Archery Timer server control api"},
    plugins=[MarshmallowPlugin()],
)

jwt_scheme = {"type": "http", "scheme": "bearer", "bearerFormat": "JWT"}
spec.components.security_scheme("jwt", jwt_scheme)

schemas = APISpecSchemaGenerator(spec)

app = Starlette()
app.debug = settings.DEBUG


@app.middleware("http")
async def add_custom_header(request, call_next):
    response = await call_next(request)
    if response.status_code == 404:
        print("test")
        return FileResponse("angular/index.html")
    return response


app.add_middleware(
    AuthenticationMiddleware,
    backend=JWTAuthenticationBackend(
        secret_key=str(settings.SECRET_KEY), prefix="Bearer", username_field="user"
    ),
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["GET", "POST", "PATCH", "PUT", "DELETE", "OPTIONS"],
    allow_headers=[
        "Authorization",
        "Origin",
        "X-Requested-With",
        "Content-Type",
        "Accept",
    ],
)

app.mount("/api/user", user)
user_config(spec)
app.mount("/api/timer", timer)
timer_config(spec)
app.mount("/api/color", color)
app.mount("/api/canvas", canvas)
canvas_config(spec)
app.mount("/api/log", log)
app.mount("/api/club", club)
app.mount("/api/system", system)


@app.route("/api/schema", methods=["GET"], include_in_schema=False)
async def openapi_schema(request):
    return schemas.OpenAPIResponse(request=request)


app.mount("/api/data", StaticFiles(directory="data", html=False))

app.mount("/", StaticFiles(directory="angular", html=True))


# from sentry_asgi import SentryMiddleware
# import sentry_sdk
# sentry_sdk.init("https://3a11d61d9ea54c30ba5532314f2c1a9c@sentry.io/1408005")
# app.add_middleware(SentryMiddleware)


def run():
    if settings.DEBUG:
        uvicorn.run(app, host="0.0.0.0", port=8090)
    else:
        uvicorn.run(app, host="0.0.0.0", port=8090)
