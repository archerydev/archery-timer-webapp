
from settings.settings import logger
from controllers.main_controller import MainController
from controllers.zeroconf_controller import *
import api_entry

from settings import settings
from utils.helpers import update_angular_env
import sys
import traceback


def my_handler(type, value, tb):
    logger.exception("Uncaught exception: {0}".format(str(value)))
    logger.exception(traceback.print_tb(tb))

# Install exception handler
sys.excepthook = my_handler

import atexit

def exit_handler():
    stop_zeroconf()

if __name__ == "__main__":
    atexit.register(exit_handler)
    logger.info(f"The server ip is: {settings.IP}")
    update_angular_env(
        settings.IP, settings.SERVER_API, settings.SERVER_PORT, settings.USE_SSL
    )
    logger.info(f"Angular env file updated")
    register_zeroconf()
    main_controller = MainController.build()
    api_entry.run()
