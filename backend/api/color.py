from starlette.applications import Starlette
from starlette.responses import JSONResponse
from starlette.authentication import requires
from starlette.endpoints import WebSocketEndpoint, HTTPEndpoint
import json
from settings import settings

from model import colors

color = Starlette()
color.debug = settings.DEBUG


@color.route("/list", methods=["GET"], include_in_schema=False)
async def colors_list(request):
    return JSONResponse(colors.colors_data_as_json(), status_code=200)


@color.route("/reset", methods=["PUT"], include_in_schema=False)
async def reset_list(request):
    colors_data = await request.body()
    colors.reset_colors()
    return JSONResponse(colors.colors_data_as_json(), status_code=200)


@color.route("/list", methods=["PUT"], include_in_schema=False)
async def update_list(request):
    colors_data = await request.json()
    colors.set_colors_from_json(colors_data)
    return JSONResponse(colors.colors_data_as_json(), status_code=200)
