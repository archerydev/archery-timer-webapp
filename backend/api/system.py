from starlette.applications import Starlette
from starlette.responses import JSONResponse
from starlette.datastructures import UploadFile
import pathlib
import glob

import json
from settings import settings
from controllers import system_controller

system = Starlette()
system.debug = settings.DEBUG


@system.route("/date", methods=["GET"], include_in_schema=False)
async def system_date(request):
    return JSONResponse(system_controller.get_current_date(), status_code=200)


@system.route("/date", methods=["PUT"], include_in_schema=False)
async def set_system_date(request):
    date_data = await request.json()
    system_controller.set_current_date(date_data["date"])
    return JSONResponse(system_controller.get_current_date(), status_code=200)


# TODO: parametrize the filter .png to get all the files with the provided pattern
@system.route("/files", methods=["GET"], include_in_schema=False)
async def get_files(request):
    path = settings.DATA_PATH
    files = [f.relative_to(path).as_posix() for f in path.rglob("*.png")]
    return JSONResponse({"files": files}, status_code=200)


@system.route("/file", methods=["POST"], include_in_schema=False)
async def upload_file(request):

    form = await request.form()
    filename = form["files"].filename
    contents = await form["files"].read()
    file_path = settings.DATA_PATH / "images" / filename
    f = open(file_path, "wb")
    f.write(contents)
    f.close()
    return JSONResponse(
        {"file_path": file_path.relative_to(settings.DATA_PATH).as_posix()},
        status_code=200,
    )
