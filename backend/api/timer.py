from starlette.applications import Starlette
from starlette.responses import JSONResponse, FileResponse
from starlette.authentication import requires
from starlette.endpoints import WebSocketEndpoint, HTTPEndpoint
import json
from settings import settings
from utils.toasynciterator import to_async_iterable

from model.archery_timer import ArcheryTimer
from marshmallow import Schema, fields
from controllers.timer_controller import TimersManagement

if settings.DEBUG:
    # Filter websockets communication that avoid to read the log
    import logging

    logger = logging.getLogger("websockets")
    logger.setLevel(logging.ERROR)

    class NoWebsocketsFilter(logging.Filter):
        def filter(self, record):
            return not "'bytes': '<24576 bytes>'" in record.getMessage()

    logger2 = logging.getLogger("uvicorn")
    logger2.addFilter(NoWebsocketsFilter())


class TimerSchema(Schema):
    timer_type = fields.Str(validate=lambda s: s in ["tournament", "warming", "arrow"])
    rounds = fields.Int()
    ends = fields.Int()
    letters = fields.Int()
    duration = fields.Int()
    name = fields.Str()


class TimerPositionSchema(Schema):
    round = fields.Int()
    end = fields.Int()
    phase = fields.Int()
    number = fields.Int()


class PlayOffEnd(Schema):
    min_number = fields.Int()
    letters = fields.Int()


class ClassificationSchema(Schema):
    # TODO
    div_order = fields.Int()
    division = fields.Str()
    order = fields.Int()
    contender = fields.Str()
    club = fields.Str()


def config_spec(spec):
    spec.tag({"name": "timer", "description": "timer operations"})
    spec.components.schema("Timer", schema=TimerSchema)
    spec.components.schema("TimerPosition", schema=TimerPositionSchema)
    spec.components.schema("PlayOffEnd", schema=PlayOffEnd)


timer = Starlette()
timer.debug = settings.DEBUG


@timer.route("/current", methods=["GET"])
async def current(request):
    """
      summary: Obtain the current timer and timer order
      tags:
        - timer
      responses:
        200:
          description: successful operation.
      """
    manager = TimersManagement()
    if manager.active_timer:
        return JSONResponse(
            {
                "timer": manager.active_timer.as_data(),
                "timerorder": manager.active_controller.get_state(),
            },
            status_code=200,
        )
    return JSONResponse({"timer": None, "timerorder": None}, status_code=200)


@timer.route("/create", methods=["POST"])
@requires("authenticated")
async def create_timer(request):
    """
      summary: Creates and loads a new timer
      tags:
        - timer
      requestBody:
        required: true
        content:
          application/json:
            schema: TimerSchema
      responses:
        200:
          description: successful operation.
      security:
        - jwt: []
      """
    t = await request.json()
    timer_hash = None
    if t.__contains__("hash"):
        timer_hash = t["hash"]
    if t["timer_type"] == "tournament":
        timer = ArcheryTimer.create_standard_tournament_timer(
            t["rounds"],
            t["ends"],
            t["letters"],
            t["duration"],
            name=t["name"],
            hash_code=timer_hash,
        )
    elif t["timer_type"] == "warming":
        timer = ArcheryTimer.create_standard_warming_timer(
            t["ends"], t["letters"], t["duration"], name=t["name"], hash_code=timer_hash
        )
    elif t["timer_type"] == "arrow":
        timer = ArcheryTimer.create_arrow_timer(
            t["letters"], t["duration"], name=t["name"], hash_code=timer_hash
        )
    elif t["timer_type"] == "playoff":
        timer = ArcheryTimer.create_standard_playoff_timer(
            t["min_number"],
            t["letters"],
            t["duration"],
            name=t["name"],
            hash_code=timer_hash,
        )
    elif t["timer_type"] == "timer":
        timer = ArcheryTimer.create_timer(
            t["duration"],
            name=t["name"],
            end_text=t["name"] + " finalizado",
            hash_code=timer_hash,
        )
    manager = TimersManagement()
    manager.load_new_timer(timer)
    return JSONResponse({"timer": manager.active_timer.as_data()}, status_code=200)


@timer.route("/load_classification", methods=["POST"])
@requires("authenticated")
async def load_classification(request):
    """
      summary: Loads a classification creating a timer to navigate
      tags:
        - timer
      requestBody:
        required: true
        content:
          application/json:
            schema: ClassificationSchema
      responses:
        200:
          description: successful operation.
      security:
        - jwt: []
      """
    # TODO: integrate with load_timer
    t = await request.json()
    timer_hash = None
    if t.__contains__("hash"):
        timer_hash = t["hash"]
    timer = ArcheryTimer.load_classification_from_json(t, hash_code=timer_hash)
    if timer != None:
        manager = TimersManagement()
        manager.load_new_timer(timer)
        return JSONResponse({"timer": manager.loaded_timer.as_data()}, status_code=200)
    else:
        return JSONResponse(
            {"error": "The classification can't be loaded"}, status_code=400
        )


@timer.route("/move_position", methods=["POST"])
@requires("authenticated")
async def move_position(request):
    """
      summary: Moves the loaded timer to the provided position
      tags:
        - timer
      requestBody:
        required: true
        content:
          application/json:
            schema: TimerPositionSchema
      responses:
        200:
          description: successful operation.
        400:
          description: If there is no timer loaded or the given position is not valid for the loaded timer
      security:
        - jwt: []
      """
    manager = TimersManagement()
    if manager.active_timer == None:
        return JSONResponse(
            {"error": "Cannot set a state if there is no timer"}, status_code=400
        )
    # TODO: check that the state belongs to the timer
    t = await request.json()
    error = manager.active_controller.move_to_position(
        t["round"], t["end"], t["phase"], t["number"]
    )
    if error == "":
        return JSONResponse({"message": "Timer moved to position"}, status_code=200)
    return JSONResponse({"error": error}, status_code=400)


@timer.route("/add_playoff_end", methods=["POST"])
@requires("authenticated")
async def add_playoff_end(request):
    """
      summary: Adds a playoff end
      tags:
        - timer
      requestBody:
        required: true
        content:
          application/json:
            schema: PlayOffEnd
      responses:
        200:
          description: successful operation.
        400:
          description: If there is no timer loaded or the loaded timer is not a playoff type one.
      security:
        - jwt: []
      """
    manager = TimersManagement()
    if not manager.active_controller:
        return JSONResponse(
            {"error": "Cannot add and end if there is no timer"}, status_code=400
        )

    t = await request.json()
    error = manager.active_controller.add_end(t["min_number"], t["letters"])
    if error == "":
        return JSONResponse({"message": "Added new end"}, status_code=200)
    return JSONResponse({"error": error}, status_code=400)


@timer.route("/load_timer", methods=["POST"], include_in_schema=False)
@requires("authenticated")
async def load_timer(request):
    manager = TimersManagement()
    t = await request.json()
    timer = ArcheryTimer.from_json_dict(t["timer"])
    error = manager.active_controller.load_timer_with_order(timer, t["order"])
    if error == "":
        return JSONResponse({"message": "Timer moved to position"}, status_code=200)
    return JSONResponse({"error": error}, status_code=400)


@timer.route("/start", methods=["POST"])
@requires("authenticated")
async def start_timer(request):
    """
      summary: Start timer
      tags:
        - timer
      responses:
        200:
          description: successful operation.
        403:
          description: There is not valid user logged in
      security:
        - jwt: []
      """
    manager = TimersManagement()
    manager.active_controller.start()
    return JSONResponse(
        {"message": "Timer started", "status": "started"}, status_code=200
    )


@timer.route("/start_pause", methods=["POST"])
@requires("authenticated")
async def start_pause_timer(request):
    """
      summary: Start / pauses the timer
      tags:
        - timer
      responses:
        200:
          description: successful operation.
        403:
          description: There is not valid user logged in
      security:
        - jwt: []
      """
    manager = TimersManagement()
    message = manager.active_controller.start_pause()
    return JSONResponse({"message": message}, status_code=200)


@timer.route("/stop", methods=["POST"])
@requires("authenticated")
async def stop_timer(request):
    """
      summary: Stop the loaded timer
      tags:
        - timer

      responses:
        200:
          description: successful operation.
        403:
          description: There is not valid user logged in
      security:
        - jwt: []
      """
    manager = TimersManagement()
    manager.active_controller.stop()
    return JSONResponse(
        {"message": "Timer stopped", "status": "stopped"}, status_code=200
    )


@timer.route("/pause", methods=["POST"])
@requires("authenticated")
async def pause_timer(request):
    """
      summary: Pause the loaded timer
      tags:
        - timer

      responses:
        200:
          description: successful operation.
        403:
          description: There is not valid user logged in
      security:
        - jwt: []
      """
    manager = TimersManagement()
    manager.active_controller.pause()
    return JSONResponse(
        {"message": "Timer paused", "status": "paused"}, status_code=200
    )


@timer.route("/end_phase", methods=["POST"])
@requires("authenticated")
async def end_phase(request):
    """
      summary: Consume the current phase
      tags:
        - timer

      responses:
        200:
          description: successful operation.
        403:
          description: There is not valid user logged in
      security:
        - jwt: []
      """
    manager = TimersManagement()
    manager.active_controller.end_phase()
    return JSONResponse({"message": "Next phase called"}, status_code=200)


@timer.route("/clear", methods=["POST"])
@requires("authenticated")
async def clear_timer(request):
    """
      summary: Remove the loaded timer
      tags:
        - timer
      responses:
        200:
          description: successful operation.
      security:
        - jwt: []
      """
    manager = TimersManagement()
    manager.clear_active_timer()
    return JSONResponse({"message": "Timer removed"}, status_code=200)


@timer.route("/save", methods=["POST"], include_in_schema=False)
@requires("authenticated")
async def save_timer(request):
    """
      summary: Save the provided timer and order
      tags:
        - timer
      requestBody:
        required: true
        content:
          application/json:
            schema: TimerSchema
      responses:
        200:
          description: successful operation.
      security:
        - jwt: []
      """
    t = await request.json()
    timer = ArcheryTimer.from_json_dict(t["timer"])
    order = t["order"]
    manager = TimersManagement()
    manager.save_timer(timer, order)
    return JSONResponse({"message": "Timer saved"}, status_code=200)


@timer.route("/delete", methods=["POST"], include_in_schema=False)
@requires("authenticated")
async def delete_timer(request):
    t = await request.json()
    timer = ArcheryTimer.from_json_dict(t["timer"])
    manager = TimersManagement()
    manager.delete_timer(timer)
    return JSONResponse({"message": "Timer deleted"}, status_code=200)


@timer.route("/timers", methods=["GET"])
async def get_saved_timers(request):
    """
      tags:
        - timer
      responses:
        200:
          description: List of timers that are saved and can be restored.
      """
    manager = TimersManagement()
    timers = manager.get_saved_timers()
    timers_data = [
        {"timer": v[0].as_data(), "order": v[1]} for (k, v) in timers.items()
    ]
    return JSONResponse(timers_data, status_code=200)


@timer.route("/timers", methods=["PUT"], include_in_schema=False)
async def save_timers(request):
    t = await request.json()
    manager = TimersManagement()
    manager.set_saved_timers(t)
    return JSONResponse("test", status_code=200)


@timer.websocket_route("/ws")
async def updater(websocket):
    manager = TimersManagement()
    await websocket.accept()
    try:
        ai = manager.state_obs.pipe(to_async_iterable())
        async for temp in ai:
            await websocket.send_text(json.dumps({"timer": temp}))
    except Exception as e:
        error_message = "resetting connection: {}".format(e.args)
        print(error_message)
        # raise Exception(error_message)
    finally:
        await websocket.close()
