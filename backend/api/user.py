from starlette.applications import Starlette
from starlette.requests import Request
from starlette.responses import JSONResponse
from starlette.authentication import requires
import json
from settings import settings
from marshmallow import Schema, fields

from model import user as user_model

SECRET_KEY = (
    "super-secret"
)  # "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"
TOKEN_SUBJECT = "access"
ACCESS_TOKEN_EXPIRE_MINUTES = 300


class UserSchema(Schema):
    username = fields.Str()
    password = fields.Str()


def config_spec(spec):
    spec.tag({"name": "auth", "description": "Authorization operations"})
    spec.components.schema("User", schema=UserSchema)


user = Starlette()
user.debug = settings.DEBUG


# $ref: '#/components/schemas/user'
@user.route("/login", methods=["POST"])
async def login(request):
    """
    summary: Login user into the system
    tags:
      - auth
    requestBody:
      required: true
      content:
        application/json:
          schema: UserSchema

    responses:
      200:
        description: successful operation.
      400:
        description: Missing username / password parameter
      403:
        description: Bad username or password
    """
    if not request.json:
        return JSONResponse({"msg": "Missing JSON in request"}, status_code=400)
    data = await request.json()
    username = data.get("username", None)
    password = data.get("password", None)
    if not username:
        return JSONResponse({"msg": "Missing username parameter"}, status_code=400)
    if not password:
        return JSONResponse({"msg": "Missing password parameter"}, status_code=400)

    valid_user = user_model.validate_credentials(username, password)
    if valid_user != "":
        return JSONResponse({"msg": "Bad username or password"}, status_code=403)

    response_details = user_model.generate_token(username)

    return JSONResponse(response_details, status_code=200)
