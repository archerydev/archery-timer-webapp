from starlette.applications import Starlette
from starlette.endpoints import WebSocketEndpoint, HTTPEndpoint
from starlette.responses import JSONResponse, FileResponse
from starlette.authentication import requires
import asyncio
import time
from collections import deque
from settings import settings
from controllers.timer_controller import TimersManagement

# https://github.com/jizhang/logviewer/blob/master/server.py
NUM_LINES = 1000
HEARTBEAT_INTERVAL = 15  # seconds

log = Starlette()
log.debug = settings.DEBUG


@log.websocket_route("/ws")
async def log_updater(websocket):
    await websocket.accept()
    try:
        tail = (
            "tail" in websocket.query_params
            and websocket.query_params["tail"]
            and websocket.query_params["tail"][0] == "1"
        )
        with open(settings.TIMER_LOG_PATH) as f:

            content = "".join(deque(f, NUM_LINES))
            # content = conv.convert(content, full = False)
            await websocket.send_text(content)

            if tail:
                # last_heartbeat = time.time()
                while True:
                    content = f.read()
                    if content:
                        # content = conv.convert(content, full=False)
                        await websocket.send_text(content)
                    else:
                        await asyncio.sleep(1)

                    # hearbeat
                    # if time.time() - last_heartbeat > HEARTBEAT_INTERVAL:
                    #     try:
                    #         webscoekt
            else:
                await websocket.close()

    except Exception as e:
        error_message = "resetting connection: {}".format(e.args)
        print(error_message)
        await websocket.close()


@log.route("/clear", methods=["POST"])
@requires("authenticated")
async def clear_log(request):
    """
      summary: Clear the log file
      tags:
        - timer
      responses:
        200:
          description: Successfully cleared.
      security:
        - jwt: []
      """
    manager = TimersManagement()
    manager.clear_log()
    return JSONResponse("log cleared", status_code=200)


@log.route("/", methods=["GET"])
@requires("authenticated")
async def retrieve_log(request):
    """
      summary: Gets the entire log file
      tags:
        - timer
      responses:
        200:
          description: The file with the time logs
      security:
        - jwt: []
      """
    return FileResponse(settings.TIMER_LOG_PATH, status_code=200)


@log.route("/", methods=["PUT"])
@requires("authenticated")
async def add_log_entry(request):
    """
      summary: Adds a manual log entry
      tags:
        - timer
      responses:
        200:
          description: The file with the time logs
      security:
        - jwt: []
      """
    log_desc = await request.json()
    manager = TimersManagement()
    manager.add_log_entry("MANUAL ENTRY", log_desc["description"])
    return JSONResponse(log_desc, status_code=200)
