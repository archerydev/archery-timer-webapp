from starlette.applications import Starlette
from starlette.endpoints import WebSocketEndpoint
from starlette.responses import JSONResponse
from starlette.authentication import requires
from settings import settings
from controllers.canvas_controller import CanvasController
import uuid
import time

from marshmallow import Schema, fields


from utils.toasynciterator import to_async_iterable

canvas = Starlette()
canvas.debug = settings.DEBUG
import logging

logger = logging.getLogger("pc")

pcs = set()


class CanvasSettingsSchema(Schema):
    refresh_rate = fields.Int()


def config_spec(spec):
    spec.tag({"name": "canvas", "description": "canvas operations"})
    spec.components.schema("Canvas", schema=CanvasSettingsSchema)


@canvas.websocket_route("/ws")
async def canvas_updater(websocket):
    canvas_controller = CanvasController()
    await websocket.accept()
    try:
        ai = canvas_controller.observable.pipe(to_async_iterable())
        async for pixels in ai:
            await websocket.send_bytes(bytes(pixels))

    except Exception as e:
        error_message = "Exception on websocket: {}".format(e.args)
        print(error_message)
    finally:
        await websocket.close()


@canvas.route("/canvas_settings", methods=["GET"])
async def canvas_settings(request):
    """
      summary: Obtain the current canvas refresh rate
      tags:
        - canvas
      responses:
        200:
          description: successful operation.
      """
    canvas_controller = CanvasController()  # main_controller.get_canvas_controller()
    return JSONResponse(
        {"refresh_rate": canvas_controller.refresh_rate}, status_code=200
    )


@canvas.route("/canvas_settings", methods=["POST"])
@requires("authenticated")
async def set_canvas_settings(request):
    """
      summary: Changes the canvas settings
      tags:
        - canvas
      requestBody:
        required: true
        content:
          application/json:
            schema: CanvasSettingsSchema
      responses:
        200:
          description: successful operation.
      security:
        - jwt: []
      """

    c = await request.json()
    canvas_controller = CanvasController()
    canvas_controller.refresh_rate = c["refresh_rate"]
    return JSONResponse(
        {"refresh_rate": canvas_controller.refresh_rate}, status_code=200
    )


@canvas.route("/offer", methods=["POST"])
async def offer(request):
    from aiortcdc import RTCPeerConnection, RTCSessionDescription

    params = await request.json()
    offer = RTCSessionDescription(sdp=params["sdp"], type=params["type"])

    pc = RTCPeerConnection()
    pc_id = "PeerConnection(%s)" % uuid.uuid4()
    pcs.add(pc)

    def log_info(msg, *args):
        logger.info(pc_id + " " + msg, *args)

    # log_info("Created for %s", request.remote)

    @pc.on("datachannel")
    async def on_datachannel(channel):
        try:
            print("on_datachannel")
            canvas_controller = CanvasController()
            ai = canvas_controller.observable.pipe(to_async_iterable())
            async for pixels in ai:
                channel.send(bytes(pixels))
        finally:
            pass

    @pc.on("iceconnectionstatechange")
    async def on_iceconnectionstatechange():
        print("ICE connection state is %s", pc.iceConnectionState)
        log_info("ICE connection state is %s", pc.iceConnectionState)
        if pc.iceConnectionState == "failed":
            await pc.close()
            pcs.discard(pc)

    # handle offer
    await pc.setRemoteDescription(offer)

    # send answer
    answer = await pc.createAnswer()
    await pc.setLocalDescription(answer)
    return JSONResponse(
        {"sdp": pc.localDescription.sdp, "type": pc.localDescription.type},
        status_code=200,
    )
