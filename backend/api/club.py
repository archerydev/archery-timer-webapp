from starlette.applications import Starlette
from starlette.responses import JSONResponse
from starlette.authentication import requires
from starlette.endpoints import WebSocketEndpoint, HTTPEndpoint
import json
from settings import settings

from model import club as club_model

club = Starlette()
club.debug = settings.DEBUG


@club.route("/clubs/", methods=["GET"], include_in_schema=False)
async def club_list(request):
    return JSONResponse(club_model.clubs_data_as_json(), status_code=200)


@club.route("/club/", methods=["POST"], include_in_schema=False)
@requires("authenticated")
async def add_club(request):
    new_club_data = await request.json()
    created_club = club_model.add_club(new_club_data["name"], new_club_data["image"])
    return JSONResponse(created_club.to_dict(), status_code=200)


@club.route("/club/{id}", methods=["PUT"], include_in_schema=False)
@requires("authenticated")
async def update_club(request):
    club_data = await request.json()
    updated_club = club_model.update_club(
        club_data["id"], club_data["name"], club_data["image"]
    )
    return JSONResponse(updated_club.to_dict(), status_code=200)


@club.route("/club/{id}", methods=["DELETE"], include_in_schema=False)
@requires("authenticated")
async def delete_club(request):
    id = int(request.path_params["id"])
    deleted_club = club_model.delete_club(id)
    if deleted_club != None:
        return JSONResponse(deleted_club.to_dict(), status_code=200)
    return JSONResponse({"error": "The club doesn't exist"}, status_code=400)
