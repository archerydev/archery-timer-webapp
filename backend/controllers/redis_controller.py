from settings import settings
from utils.utils import Singleton
import redis

class RedisClient(metaclass=Singleton):
    def __init__(self):
        self.pool = redis.ConnectionPool(host=settings.DATABASE_URL, port=6379)

    @property
    def conn(self):
        if not hasattr(self, "_conn"):
            self.getConnection()
        return self._conn

    def getConnection(self):
        self._conn = redis.Redis(connection_pool=self.pool)
