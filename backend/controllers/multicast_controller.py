__author__ = "gpt"

import socket
import time
from threading import Thread
from utils.utils import Singleton

from settings.settings import logger

# http://troglobit.com/howto/multicast/
class MulticastController(Thread, metaclass=Singleton):
    """
    Class that continously multicast the current representation to the network using a thread.
   """

    MULTICAST_TTL = 2

    def __init__(self, group, port, start=True):
        """
        Prepare the controller.
        group: multicastgroup
        port: port where send the broadcast
        """
        Thread.__init__(self)
        logger.info("Init Multicast Controller")
        self.multicast_group = group
        self.multicast_port = port
        self.cs = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        self.cs.setsockopt(
            socket.IPPROTO_IP,
            socket.IP_MULTICAST_TTL,
            MulticastController.MULTICAST_TTL,
        )
        self.status = "stopped"
        if start:
            self.start_controller()

    def start_controller(self):
        """
        Initialize the thread
        """
        logger.info("Starting Multicast controller")
        self.status = "running"
        self.start()

    def stop(self):
        """
        Stop the thread. Once the thread is stopped it can not be restarted, so a new controller must be
        created
        """
        logger.info("Stopping Multicast controller")
        self.status = "stopped"

    def run(self):
        """
        The code executed when the thread is started.
        """
        while True:
            if self.status != "running":
                break

    def send_instant(self, pixels):
        """
        Write the instant_rep to the network
        """
        self.cs.sendto(pixels, (self.multicast_group, self.multicast_port))
