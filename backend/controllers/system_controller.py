import json
import datetime


def get_current_date():
    d = datetime.datetime.now().isoformat()
    j = json.dumps(d)
    return j


def set_current_date(date):
    import os
    import platform

    if platform.system() == "Linux":
        os.system("date -s %s" % date)
