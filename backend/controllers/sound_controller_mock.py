from utils.utils import Singleton
from threading import Thread
import time

from settings.settings import logger


class SoundController(metaclass=Singleton):
    def __init__(self):
        logger.info("init Sound Controller")

    def beep(self, duration=0.5, numbeeps=1):
        for i in range(0, numbeeps):
            logger.debug("Sound Controller: init beep of " + str(duration))
            time.sleep(duration)
            logger.debug("Sound Controller: end beep")
            time.sleep(0.3)

    def beep_async(self, duration=0.5, numbeeps=1):
        Thread(target=self.beep, args=(duration, numbeeps)).start()
