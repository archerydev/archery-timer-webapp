from threading import RLock
import hashlib
import time
import rx
from rx.subject import BehaviorSubject
from utils.chronometer import Chronometer
from model.end import End
from model.round import Round
from model.archery_timer import Status, ArcheryTimer
from utils.utils import Singleton
from settings import settings


import asyncio

__author__ = "gpt"

import logging
from logging import FileHandler, Formatter
from settings.settings import logger

timer_logger = logging.getLogger("timer_logger")
timer_logger.setLevel(logging.INFO)
timer_logger_file_handler = FileHandler(settings.TIMER_LOG_PATH)
timer_logger_file_handler.setFormatter(
    Formatter("%(asctime)s: %(message)s", "%Y-%m-%d %H:%M:%S")
)
timer_logger.addHandler(timer_logger_file_handler)


class TimerController(object):
    def __init__(self, timer, order=None):
        self.lock = RLock()
        self._timer = timer
        self._round_number = -1
        self._round = None
        self._end = None
        self._end_number = -1
        self._phase_number = -1
        self._phase = None
        self._number = -1
        self._finished_tournament = True
        self._status = Status.STOPPED
        self._last_state = self.get_state()
        self._state_obs = BehaviorSubject(self._last_state)
        self.load_order(order)
        self._timer_thread = Chronometer(
            self.receive_ticks, start=timer != None
        )  # the timer thread

    @property
    def finished(self):
        return self._finished_tournament

    @property
    def timer(self):
        return self._timer

    @property
    def state_obs(self):
        return self._state_obs

    def load_order(self, order):
        if order != None:
            self.move_to_position(
                order["round"], order["end"], order["phase"], order["number"]
            )

    def set_status(self, pstatus):
        with self.lock:
            self._status = pstatus

    def start_pause(self):
        logger.debug("Controller start pause")
        with self.lock:
            if self._status == Status.RUNNING:
                self.pause()
                return "Timer Paused"
            else:
                self.start()
                return "Timer Started"

    def start(self):
        """
            Start updating the timer
        """
        with self.lock:
            self._status = Status.RUNNING
        self.notify_change()
        self.add_log_entry("Timer Start")

    def pause(self):
        """
            Pause the timer
        """
        with self.lock:
            if self._status == Status.RUNNING:
                self.add_log_entry("Timer Pause")
                self._status = Status.PAUSED
            elif self._status == Status.PAUSED:
                self.add_log_entry("Timer Resume")
                self._status = Status.RUNNING
        self.notify_change()

    def stop(self):
        """
            Stop the timer
        """
        with self.lock:
            self.add_log_entry("Timer Stop")
            self._status = Status.STOPPED
        self.notify_change()

    def end_phase(self):
        """
        Finishes the current phase by setting the timer to the end of the phase value
        """
        logger.debug("Controller end phase")
        with self.lock:
            if self._phase != None:
                self.add_log_entry(f"{self._phase.name} ended")
                if self._phase.can_be_finished:
                    self.move_to_position(-1, -1, -1, self._phase.end_value)
        self.notify_change()

    def move_to_position(
        self, num_round, num_end, num_phase, num, new_status=Status.STOPPED
    ):
        """
        Sets the current values for the tournament status. It checks if the values are correct
        :param num_round: round to be set, -1 if it is unchanged
        :param num_end: end to be set. -1 if it is unchanged
        :param num_phase: phase to be set. -1 if it is unchanged
        :param num: number to show. -1 if it is unchanged
        :return:
        """
        if num_round != -1:
            if num_round < len(self.timer.rounds):
                self._round_number = num_round
                self._round = self.timer.rounds[self._round_number]
            else:
                logging.warning("Incorrect round")
                return "Incorrect round"
        if num_end != -1:
            if num_end < self._round.get_num_ends():
                self._end_number = num_end
                self._end = self._round.ends[self._end_number]
            else:
                logging.warning("Incorrect end")
                return "Incorrect end"
        if num_phase != -1:
            if num_phase < len(self._end.phases):
                self._phase_number = num_phase
                self._phase = self._end.phases[self._phase_number]
            else:
                logging.warning("Incorrect phase")
                return "Incorrect phase"
        if num != -1:
            if num <= self._phase.init_value:
                self._number = num
            else:
                logging.warning("Incorrect number")
                return "Incorrect number"
        else:
            self._number = self._phase.init_value
        self._finished_tournament = False
        self.notify_change()
        return ""

    def receive_ticks(self, timer_count):
        """
        Handles the ticks from the Chronometer.
        It performs the right calculus of time taking into account the start / pause /stop
        """
        with self.lock:
            if self._status == Status.RUNNING and self.timer != None:
                if self.advance_time(timer_count):
                    self._status = Status.STOPPED
                self.notify_change()

    def advance_time(self, timer_count) -> bool:
        """
        Advance the timer the given time. Moving to the next phases / ends / and rounds.
        :param timer_count: The amount of time in seconds to move the timer
        :return: True if the timer status must be stopped, false otherwise
        """
        if self._finished_tournament:
            return True
        stop_timer = False
        self._number -= timer_count
        if self._number < self._phase.end_value:
            self.add_log_entry(
                f"Phase {self._phase_number + 1} Finished", self._phase.name
            )
            self._phase_number = self._phase_number + 1
            if self._phase_number > len(self._end.phases) - 1:
                self.add_log_entry(f"End {self._end_number + 1} Finished")
                self._end_number += 1
                if self._end_number > self._round.get_num_ends() - 1:
                    self._end_number = 0
                    self.add_log_entry(f"Round {self._round_number + 1} Finished")
                    self._round_number += 1
                    if self._round_number > len(self.timer.rounds) - 1:
                        self._round_number = -1
                        self.add_log_entry(f"Timer {self.timer.name} Finished")
                        self._finished_tournament = True
                        # TODO: decide what to do at the tournament_end
                        return True
                    else:
                        self._round = self.timer.rounds[self._round_number]
                        self.add_log_entry(f"Round {self._round_number + 1} Started")
                self._end = self._round.ends[self._end_number]
                self.add_log_entry(f"End {self._end_number + 1} Loaded")
                self._phase_number = 0
            self._phase = self._end.phases[self._phase_number]
            self.add_log_entry(
                f"Phase {self._phase_number + 1} Started", self._phase.name
            )
            stop_timer = self._phase.start_stopped
            self._number = self._phase.init_value
        logging.debug(
            "time: "
            + str(self._number)
            + " phase: "
            + str(self._phase.name)
            + " end: "
            + str(self._end_number)
            + " round: "
            + str(self._round_number)
        )
        return stop_timer

    def notify_change(self):
        self._last_state = self.get_state()
        self._state_obs.on_next(self._last_state)

    def add_end(self, min_number, num_letters):
        if not self._timer:
            return "Cannot add and end if there is no timer"
        if self._timer.timer_type != "PlayOff_timer":
            return "Only Playoff timers can add playoff ends"
        self._timer.add_end_playoff_timer(min_number, num_letters)
        return self.move_to_position(0, len(self._timer.rounds[0].ends) - 1, 0, -1)

    def get_state(self):
        """
          Returns the current representation of the timer to be send as a broadcast.
          It includes the status parameter
          :param status:
          :return: string with the json representation of the timer
          """
        # TODO: decide what to do when the tournament has finished
        num_round = float("%.2f" % (self._number))
        data = {}
        data["timer_hash"] = self.timer.hash_code if self.timer else ""
        data["round"] = self._round_number if self.timer else 0
        data["num_rounds"] = len(self.timer.rounds) if self.timer else 0
        data["end"] = self._end_number
        data["end_name"] = self._end.name if self._end else ""
        data["num_ends"] = self._round.get_num_ends() if self._round else 0
        data["phase"] = self._phase_number
        data["phases_names"] = self._end.get_phases_name_list() if self._end else []
        data["can_be_finished"] = self._phase.can_be_finished if self._phase else False
        data["can_create_end"] = (
            self._timer
            and self._timer.timer_type == "PlayOff_timer"
            and self._finished_tournament
        )
        data["status"] = self._status if self.timer else Status.STOPPED
        data["number"] = num_round
        data["finished"] = self._finished_tournament

        data["number_color"] = None
        data["turn1"] = None
        data["turn1_color"] = None
        data["turn2"] = None
        data["turn2_color"] = None
        data["sound"] = 0
        data["text"] = None
        data["secondary_text"] = None
        if self._phase != None:
            self._phase.fill_timer_order(data, self._status)
        return data

    def add_log_entry(self, action, description=""):
        timer_hash = self.timer.hash_code if self.timer != None else ""
        round_log = (
            f"Round {self._round_number + 1}" if self._round_number != -1 else ""
        )
        end_log = f"End {self._end_number + 1}" if self._end_number != -1 else ""
        timer_logger.info(
            "{0:10}; {1:2}; {2:2}; {3:10}; {4:30};".format(
                timer_hash, round_log, end_log, action, description
            )
        )


EmptyTimerController = TimerController(None)
InitOrder = {"round": 0, "end": 0, "phase": 0, "number": -1}


class TimersManagement(metaclass=Singleton):
    def __init__(self):
        self._saved_timers = {}
        self._timer_obs = BehaviorSubject(None)
        self._state_obs = BehaviorSubject(None)
        self._active_timer_controller = None
        self._subscription = None
        self.active_controller = EmptyTimerController

    @property
    def active_timer_obs(self):
        return self._timer_obs

    @property
    def state_obs(self):
        return self._state_obs

    @property
    def active_timer(self):
        return self.active_controller.timer

    @property
    def active_controller(self):
        return self._active_timer_controller

    @active_controller.setter
    def active_controller(self, controller):
        if self._active_timer_controller:
            self._active_timer_controller.stop()
            if self._subscription:
                self._subscription.dispose()

        self._active_timer_controller = controller
        self._timer_obs.on_next(self._active_timer_controller.timer)
        self._subscription = self._active_timer_controller.state_obs.subscribe(
            on_next=lambda s: self.test(s)
        )

    def test(self, state):
        self._state_obs.on_next(state)

    def clear_active_timer(self):
        """
        Clears the timer reseting all data
        :return:
        """
        self.active_controller.stop()
        self.active_controller = EmptyTimerController

    def load_new_timer(self, timer):
        """
        loads the given timer as the ongoing tournament for the timer
        :param timer:
        :return:
        """
        self.load_timer_with_order(timer, InitOrder)

    def load_timer_with_order(self, timer, order):
        if self.active_controller:
            if not self.active_controller.finished:
                self.active_controller.stop()
                self.save_timer(self.active_timer, self.active_controller.get_state())

        self.active_controller = TimerController(timer, order)
        if timer.hash_code in self._saved_timers:
            self._saved_timers.pop(timer.hash_code)

    def restore_timer(self, timer_hash):
        if timer_hash in self._saved_timers:
            self.load_timer_with_order(
                self._saved_timers[timer_hash].timer,
                self._saved_timers[timer_hash].order,
            )
            self._saved_timers.pop(timer_hash)

    def save_timer(self, timer, order):
        self._saved_timers[timer.hash_code] = (timer, order)

    def delete_timer(self, timer):
        if timer.hash_code in self._saved_timers:
            self._saved_timers.pop(timer.hash_code)

    def set_saved_timers(self, timers):
        self._saved_timers = {}
        for t in timers:
            self.save_timer(ArcheryTimer.from_json_dict(t["timer"]), t["order"])

    def get_saved_timers(self):
        return self._saved_timers

    def start_pause(self):
        logging.debug("Start_pause")
        if self._active_timer_controller:
            self._active_timer_controller.start_pause()

    def end_phase(self):
        logging.debug("end_phase")
        if self._active_timer_controller:
            self._active_timer_controller.end_phase()

    def clear_log(self):
        with open("data/timer_logger.log", "w"):
            pass
