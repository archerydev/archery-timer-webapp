import numpy as np
from threading import RLock
from utils.utils import Singleton
from threading import Thread
import time


from settings.settings import logger


class AnimationController(Thread, metaclass=Singleton):
    """
      Class that continously refresh the leds hardware using a thread.
    """

    lock = RLock()

    def __init__(self, refresh_rate, start=True):
        """
        Prepare the controller.

        refres_rate: the frequency of refreshing of the leds
        """
        Thread.__init__(self)
        logger.info("Init Animation Controller")
        self.refresh_rate = refresh_rate

        self.animations = []
        if start:
            self.start_controller()

    def start_controller(self):
        """
        Initialize the thread
        """
        logger.info("Starting Animation controller")
        self.status = "running"
        self.start()

    def stop(self):
        """
        Stop the thread. Once the thread is stopped it can not be restarted, so a new controller must be
        created
        """
        logger.info("Stopping Animation controller")
        self.status = "stopped"

    def run(self):
        """
        The code executed when the thread is started.
        """
        while self.status == "running":
            for animation in self.animations:
                animation.step()
            time.sleep(self._sleep_time)

    @property
    def refresh_rate(self):
        return self._refresh_rate

    @refresh_rate.setter
    def refresh_rate(self, refresh_rate):
        self._refresh_rate = refresh_rate
        self._sleep_time = 1.0 / self._refresh_rate

    def clear_animations(self):
        with AnimationController.lock:
            self.animations = []

    def remove_animation(self, animation):
        with AnimationController.lock:
            if animation in self.animations:
                self.animations.remove(animation)

    def add_animation(self, drawable, pos=-1):
        with AnimationController.lock:
            if pos == -1:
                self.animations.append(drawable)
            else:
                self.animations.insert(pos, drawable)
