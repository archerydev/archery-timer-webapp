from utils.utils import Singleton
from settings import settings
from gpiozero import DigitalOutputDevice
from settings.settings import logger

class SoundController(metaclass=Singleton):
    def __init__(self):
        logger.info("init Sound Controller")
        self.relay = DigitalOutputDevice(settings.SOUND_GPIO, active_high=False)

    def beep(self, duration=0.5, numbeeps=1):
        self.relay.blink(duration, 0.3, numbeeps, False)

    def beep_async(self, duration=0.5, numbeeps=1):
        self.relay.blink(duration, 0.3, numbeeps)
