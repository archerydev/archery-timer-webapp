from gpiozero import Button
from utils.utils import Singleton
from settings.settings import logger

class ButtonController(metaclass=Singleton):
    def __init__(self):
        logger.info("Init Button Controller")
        self.button_play = Button(23)
        self.button_next = Button(24)

    def register_callbacks(self, function_play, function_next):
        self.button_play.when_pressed = function_play
        self.button_next.when_pressed = function_next

def play():
    logger.debug("play")

def end_phase():
    logger.debug("end_phase")


if __name__ == "__main__":
    buttonController = ButtonController()
    buttonController.register_callbacks(play, end_phase)

