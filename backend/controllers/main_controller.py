from utils.utils import Singleton

from controllers.canvas_controller import CanvasController

from model.club import get_club_by_name

from controllers.timer_controller import TimersManagement
from controllers.animation_controller import AnimationController

from model.colors import get_color
from model.archery_timer import (
    ARROW,
    PLAYOFF,
    TOURNAMENT,
    WARMING,
    TIMER,
    CLASSIFICATION,
)

from drawables.timer_drawables import (
    TournamentTimerDrawable,
    TimerDrawable,
    ClassificationDrawable,
)
from drawables.image_drawable import ImageDrawable
from drawables.drawables_collection import *
from settings import settings

import os

class MainController(metaclass=Singleton):
    def __init__(
        self,
        canvas_controller,
        sound_controller=None,
        multicast_controller=None,
        button_controller=None,
        animation_controller=None,
    ):
        self._canvas_controller = canvas_controller
        self._sound_controller = sound_controller
        self._multicast_controller = multicast_controller
        if self._multicast_controller != None:
            self._canvas_controller.observable.subscribe(
                on_next=lambda pixels: self._multicast_controller.send_instant(pixels)
            )
        self._num_cols = settings.NUM_COLS
        self._num_rows = settings.NUM_ROWS
        self._animation_controller = animation_controller
        self._drawable = None
        self._animation = None
        self._timers_management = TimersManagement()

        if button_controller != None:
            self._button_controller = button_controller
            self._button_controller.register_callbacks(
                self._timers_management.start_pause,
                self._timers_management.end_phase,
            )

        self._timers_management.active_timer_obs.subscribe(
            on_next=lambda t: self.on_new_timer(t)
        )
        self._timers_management.state_obs.subscribe(
            on_next=lambda i: self.on_new_instant(i)
        )
        if settings.SHOW_IP:
            self.show_ip(settings.IP)

    def show_ip(self, ip):
        if self._animation:
            self._animation.stop()

        (_, self._animation) = animate_text2(
            ip,
            "9x15.pil",
            get_color("RED"),
            30,
            self._animation_controller,
            self._canvas_controller,
            drawable_init=lambda d: d.center_y(64),
        )

    def on_new_timer(self, timer):

        if self._drawable:
            self._canvas_controller.remove_drawable(self._drawable)
            self._drawable.dispose()
            self._drawable = None

        if self._animation:
            self._animation.stop()

        if timer != None:
            if timer.timer_type == CLASSIFICATION:
                self._canvas_controller.remove_drawable(self._drawable)
                self._drawable = ClassificationDrawable(
                    self._timers_management.state_obs,
                    get_color,
                    self._animation_controller,
                    self._canvas_controller,
                    get_club_by_name,
                )
            elif timer.timer_type == TIMER:
                self._drawable = TimerDrawable(
                    self._num_cols,
                    self._timers_management.state_obs,
                    get_color,
                    self._animation_controller,
                    self._canvas_controller,
                )
                self._canvas_controller.set_drawable(self._drawable)
            else:
                self._drawable = TournamentTimerDrawable(
                    self._num_cols,
                    self._num_rows,
                    self._timers_management.state_obs,
                    get_color,
                    self._animation_controller,
                    self._canvas_controller,
                    timer.timer_type,
                )
                self._canvas_controller.set_drawable(self._drawable)
        else:
            club = get_club_by_name("CDETA Rivas Vaciamadrid")
            if club != None:
                self._animation = get_club_animation(
                    club,
                    self._animation_controller,
                    self._canvas_controller,
                    start=True,
                )

    def on_new_instant(self, i):
        if i == None:
            return
        if "sound" in i and i["sound"] > 0:
            self._sound_controller.beep_async(0.5, i["sound"])

    @classmethod
    def build(cls):

        if settings.SOUND_GPIO == -1:
            from .sound_controller_mock import SoundController
            button_controller = None
        else:
            from controllers.sound_controller import SoundController
            from controllers.button_controller import ButtonController

            button_controller = ButtonController()

        canvas_controller = CanvasController(
            settings.NUM_ROWS, settings.NUM_COLS, settings.CANVAS_REFRESH_RATE
        )
        animation_controller = AnimationController(settings.CANVAS_REFRESH_RATE)
        sound_controller = SoundController()

        multicast_controller = None
        if settings.RUN_MULTICAST:
            from .multicast_controller import MulticastController

            multicast_controller = MulticastController(
                settings.MULTICAST_IP, settings.MULTICAST_PORT
            )

        return cls(
            canvas_controller,
            sound_controller,
            multicast_controller,
            button_controller,
            animation_controller,
        )
