#!/usr/bin/env python3

""" Example of announcing a service (in this case, a fake HTTP server) """

import argparse

import socket
from settings import settings

from zeroconf import ServiceInfo, Zeroconf

from settings.settings import logger

desc = {'version':"0.70.0",'name':'ArcheryTimer'}
info = ServiceInfo(
        "_archerytimer._tcp.local.",
        "Server._archerytimer._tcp.local.",
        addresses=[socket.inet_aton(settings.IP)],
        port=8090,
        properties=desc,
        server="ArcheryTimer.local.",
    )

zeroconf = Zeroconf()
def stop_zeroconf():
      """Stop Zeroconf."""
      logger.info("Stopping Zeroconf...")
      zeroconf.unregister_service(info)
      zeroconf.close()

def register_zeroconf():
    logger.info("   Registering service...")
    zeroconf.register_service(info)
    logger.info("   Registration done.")

if __name__ == "__main__":
    register_zeroconf()

