import numpy as np
from threading import RLock
from utils.utils import Singleton
from threading import Thread
from PIL import Image, ImageDraw
import time
from collections import defaultdict
from rx.subject import BehaviorSubject

from settings.settings import logger


class CanvasController(Thread, metaclass=Singleton):
    """
      Class that continously refresh the leds hardware using a thread.
    """

    lock = RLock()

    def __init__(self, num_rows, num_cols, refresh_rate, start=True):
        """
        Prepare the controller.

        refres_rate: the frequency of refreshing of the leds
        """
        Thread.__init__(self)
        logger.info("Init Canvas Controller")
        self.refresh_rate = refresh_rate

        self.img = np.zeros((num_rows, num_cols, 4), dtype=np.uint8)
        self.pil_im = Image.fromarray(self.img, "RGB")
        self.canvas = ImageDraw.Draw(self.pil_im)
        self._drawablesdict = defaultdict(int)
        self._canvas_observable = BehaviorSubject(self.get_pixels())
        if start:
            self.start_controller()

    @property
    def observable(self):
        return self._canvas_observable

    def get_pixels(self):
        return (np.array(self.pil_im)).flatten()

    def start_controller(self):
        """
        Initialize the thread
        """
        logger.info("Starting canvas controller")
        self.status = "running"
        self.start()

    def stop(self):
        """
        Stop the thread. Once the thread is stopped it can not be restarted, so a new controller must be
        created
        """
        logger.info("Stopping canvas controller")
        self.status = "stopped"

    def run(self):
        """
        The code executed when the thread is started.
        """
        while self.status == "running":
            self.paint()
            pixels = self.get_pixels()
            self._canvas_observable.on_next(pixels)
            time.sleep(self._sleep_time)

    @property
    def refresh_rate(self):
        return self._refresh_rate

    @refresh_rate.setter
    def refresh_rate(self, refresh_rate):
        self._refresh_rate = refresh_rate
        self._sleep_time = 1.0 / self._refresh_rate

    def clear_canvas(self):
        self.canvas.rectangle(
            [0, 0, self.canvas.im.size[0], self.canvas.im.size[1]], fill=(0, 0, 0, 255)
        )

    def paint(self):
        self.clear_canvas()
        with CanvasController.lock:
            for drawable in self._drawablesdict:
                drawable.draw(self.pil_im, self.canvas)

    def remove_drawable(self, drawable):
        with CanvasController.lock:
            if drawable == None:
                return
            logger.debug("remove: " + str(drawable) + " " + str(drawable._uuid))
            self._drawablesdict[drawable] -= 1
            if self._drawablesdict[drawable] <= 0:
                self._drawablesdict.pop(drawable)

    def add_drawable(self, drawable):
        with CanvasController.lock:
            logger.debug("added: " + str(drawable) + " " + str(drawable._uuid))
            self._drawablesdict[drawable] += 1

    def set_drawable(self, drawable):
        with CanvasController.lock:
            logger.debug("set_drawable: " + str(drawable) + " " + str(drawable._uuid))
            self._drawablesdict.clear()
            self.add_drawable(drawable)

    def clear_drawable(self, drawable):
        with CanvasController.lock:
            if drawable == None:
                return
            self._drawablesdict.pop(drawable, None)

    def clear_drawables(self):
        with CanvasController.lock:
            self._drawablesdict.clear()
