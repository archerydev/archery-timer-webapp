from threading import Thread
import time


class Chronometer(Thread):
    """[summary]
    Provides a exact tick execution of the provided function on the frequency given
    Takes into account the time to exectute the function to try to tick on the
    same time always
    """

    def __init__(self, function, frequency=0.1, start=True):
        """[summary]

        Arguments:
            function {any} -- The function that should be called on each tick

        Keyword Arguments:
            frequency {float} -- The tick frequency (default: {0.1})
        """
        Thread.__init__(self)
        self._frequency = frequency
        self._callback = function
        self._status = "stopped"
        if start:
            self.init_timer()

    def init_timer(self):
        self._status = "running"
        self.start()

    def stop(self):
        self._status = "stopped"

    def run(self):
        d0 = time.time()
        last_time = d0
        while self._status == "running":
            d0 = time.time()
            step = d0 - last_time
            self._callback(step)
            last_time = d0
            adjust = last_time - time.time()
            time.sleep(self._frequency - adjust)
