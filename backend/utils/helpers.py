# from settings.settings import get_logger


def get_ip():
    try:
        import netifaces as ni

        default_gateway = ni.gateways()["default"][ni.AF_INET][1]
        return ni.ifaddresses(default_gateway)[ni.AF_INET][0]["addr"]
    except IOError:
        pass
        # get_logger().error("error getting the default interface")


def update_angular_env(ip, server_api, server_port, use_ssl):
    angular_url = server_api
    if server_api == "auto":
        angular_url = ip
    http = "http"
    ws = "ws"
    port = server_port
    if port != "":
        port = f":{port}"
    if use_ssl:
        http = f"{http}s"
        ws = f"{ws}s"

    with open("angular/env.js", "w+") as file:
        file.write("(function (window) {\n")
        file.write(" window.__env = window.__env || {};\n")
        file.write(" // API url\n")
        file.write(f" window.__env.apiUrl = '{http}://{angular_url}{port}/api';\n")
        file.write(f" window.__env.wsUrl = '{ws}://{angular_url}{port}/api';\n")
        file.write("\n")
        file.write("} (this));\n")
