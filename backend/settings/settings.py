from starlette.config import Config
from starlette.datastructures import Secret
from pathlib import Path
from utils.helpers import get_ip

config = Config(".env")

DEBUG = config("DEBUG", cast=bool, default=False)
SECRET_KEY = config("SECRET_KEY", cast=Secret)

DATABASE_URL = config("DATABASE_URL", cast=str)
USE_REDIS = config("USE_REDIS", cast=bool, default=False)
RUN_MULTICAST = config("RUN_MULTICAST", cast=bool, default=False)
MULTICAST_IP = config("MULTICAST_IP", cast=str, default="226.0.0.1")
MULTICAST_PORT = config("MULTICAST_PORT", cast=int, default=9999)
SOUND_GPIO = config("SOUND_GPIO", cast=int, default=-1)
SHOW_IP = config("SHOW_IP", cast=bool, default=False)

SERVER_PORT = config("SERVER_PORT", cast=str, default="8090")
SERVER_API = config("SERVER_API", cast=str, default="localhost")
USE_SSL = config("USE_SSL", cast=bool, default=False)
TIMER_LOG_FILE = config("TIMER_LOG_FILE", cast=str, default="timer_logger.log")
NUM_ROWS = config("NUM_ROWS", cast=int, default=64)
NUM_COLS = config("NUM_COLS", cast=int, default=128)
CANVAS_REFRESH_RATE = config("NUM_COLS", cast=int, default=24)
LOG_TO_FILE = config("LOG_TO_FILE", cast=bool, default=True)

###########################################

DATA_PATH = Path.cwd() / "data"
TIMER_LOG_PATH = DATA_PATH / TIMER_LOG_FILE

IP = get_ip()

from logging import FileHandler, StreamHandler, Formatter
import logging

log_formatter = Formatter("%(levelname)s: %(asctime)s: %(message)s")


log_handler = StreamHandler()
log_handler.setFormatter(log_formatter)

logger = logging.getLogger("archery_timer")
if DEBUG:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)
logger.addHandler(log_handler)

if LOG_TO_FILE:
    file_logger_file_handler = FileHandler(DATA_PATH / "logs.log")
    file_logger_file_handler.setFormatter(log_formatter)
    logger.addHandler(file_logger_file_handler)


def get_logger():
    return logger
