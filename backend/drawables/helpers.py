import os
from math import ceil
from PIL import ImageFont, Image
import datetime
from math import floor
from settings import settings

current_dir_path = os.path.dirname(os.path.abspath(__file__))


def get_font(fontName):
    return ImageFont.load(os.path.join(current_dir_path, "assets", "fonts", fontName))


def get_club_image_path(club):
    return settings.DATA_PATH / club.image_path


def get_image(imageName):
    return Image.open(
        os.path.join(current_dir_path, "assets", "images", imageName)
    ).convert("RGBA")


def get_images_from_sprite(
    imageName, num_images, image_height, image_width, image_offset
):
    image = get_image(imageName)
    return [
        image.crop(
            (image_offset * i, 1, image_offset * i + image_width, 1 + image_height)
        )
        for i in range(0, num_images)
    ]


def draw_text_centered(canvas, text, font, fill, pos_y):
    canvas.text(
        [center_posx(text, font, canvas.im.size[0]), pos_y], text, fill=fill, font=font
    )


def center_posx(text, font, width):
    textSize = font.getsize(text)
    return floor((width - textSize[0]) / 2)


def center_posy(text, font, height):
    textSize = font.getsize(text)
    return floor((height - textSize[1]) / 2)


def center_pos(text, font, width, height):
    textSize = font.getsize(text)
    return (floor((width - textSize[0]) / 2), floor((height - textSize[1]) / 2))


def time_representation(seconds):
    minutes, seconds = divmod(seconds, 60)
    return "{:02}:{:02}".format(int(minutes), int(seconds))


def draw_number_list(
    canvas,
    current,
    total,
    pos_x,
    pos_y,
    font,
    font_width,
    current_color,
    previous_color,
    next_color,
):
    pos = pos_x
    for i in range(total):
        if i < current:
            fill = previous_color
        elif i == current:
            fill = current_color
        else:
            fill = next_color
        if i + 1 < 10:
            canvas.text([pos, pos_y], str(i + 1), fill=fill, font=font)
        else:
            canvas.text([pos - 3, pos_y], "1", fill=fill, font=font)
            canvas.text([pos - 3 + font_width - 2, pos_y], "0", fill=fill, font=font)

        pos = pos + font_width
