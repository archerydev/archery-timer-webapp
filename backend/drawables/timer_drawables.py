from math import ceil
from .drawable import Drawable, ComposedDrawable
from .drawable2 import TextDrawable
from .helpers import draw_text_centered, get_font, draw_number_list, time_representation
from .drawables_collection import *

TOURNAMENT = "Tournament_timer"
PLAYOFF = "PlayOff_timer"
WARMING = "Warming_timer"
ARROW = "Arrow_timer"
TIMER = "Normal_timer"


class OrderDrawable(Drawable):
    def __init__(self, color_provider):
        super().__init__()
        self._order = None
        self._color_provider = color_provider

    def draw(self, pil_img, canvas, offset_x=0, offset_y=0):
        super(OrderDrawable, self).draw(pil_img, canvas, offset_x, offset_y)
        if self._order == None:
            return
        self.draw_order(pil_img, canvas)

    def set_order(self, order):
        self._order = order

    def draw_order(self, pil_img, canvas):
        pass


class ComposedOrderDrawable(OrderDrawable, ComposedDrawable):
    def __init__(self, color_provider):
        OrderDrawable.__init__(self, color_provider)
        ComposedDrawable.__init__(self)

    def set_order(self, order):
        OrderDrawable.set_order(self, order)
        for drawable in self._drawables:
            drawable.set_order(order)


class MainTimerDrawable(OrderDrawable):
    def __init__(self, color_provider):
        super().__init__(color_provider)
        self._letterFont = get_font("Crono_17x29_Bold.pil")
        self._numberFont = get_font("Crono_20x40_Bold.pil")

    def draw_order(self, pil_img, canvas):
        super(MainTimerDrawable, self).draw_order(pil_img, canvas)
        self.draw_time(canvas, -7)
        self.draw_turns(canvas, 2)

    def draw_time(self, canvas, pos_y):

        fill = self._color_provider(self._order["number_color"] or "BLACK")
        number_text = str(ceil(self._order["number"]))
        pos = 103
        if self._order["number"] > 99:
            pos = 47
        elif self._order["number"] > 9:
            pos = 75
        canvas.text([pos, pos_y], number_text, fill=fill, font=self._numberFont)

    def draw_turns(self, canvas, pos_y):

        if self._order["turn1"] == None or self._order["turn2"] == None:
            return
        canvas.text(
            [-2, pos_y],
            self._order["turn1"],
            fill=self._color_provider(self._order["turn1_color"]),
            font=self._letterFont,
        )
        canvas.text(
            [18, pos_y],
            self._order["turn2"],
            fill=self._color_provider(self._order["turn2_color"]),
            font=self._letterFont,
        )


class RoundEndDrawable(OrderDrawable):
    def __init__(self, color_provider, round_text=None):
        super().__init__(color_provider)
        self._roundFont = get_font("Crono_9x16.pil")
        self._font_width = 10
        self._round_text = round_text
        self._pos_y = 44

    def draw_order(self, pil_img, canvas):
        super(RoundEndDrawable, self).draw_order(pil_img, canvas)
        num_rounds = self.draw_round(canvas, self._pos_y)
        pos = self.draw_end(canvas, self._pos_y)
        self.draw_separator(canvas, self._pos_y, pos, num_rounds)

    def draw_round(self, canvas, pos_y):
        if self._round_text == None:
            draw_number_list(
                canvas,
                self._order["round"],
                self._order["num_rounds"],
                -4,
                pos_y,
                self._roundFont,
                self._font_width,
                self._color_provider("GREEN"),
                self._color_provider("RED"),
                self._color_provider("ORANGE"),
            )
            return self._order["num_rounds"]
        else:
            canvas.text(
                [0, pos_y],
                self._round_text,
                fill=self._color_provider("ORANGE"),
                font=self._roundFont,
            )
            return len(self._round_text)

    def draw_separator(self, canvas, pos_y, ends_pos, rounds_size):
        pos_separator = (
            rounds_size * self._font_width
            - 4
            + (ends_pos - rounds_size * self._font_width - 4) / 2
        )
        canvas.text(
            [pos_separator, pos_y],
            "-",
            fill=self._color_provider("GREEN"),
            font=self._roundFont,
        )

    def draw_end(self, canvas, pos_y):
        num_chars = self._order["num_ends"]
        if self._order["num_ends"] > 9:
            num_chars = num_chars + 1
        pos = canvas.im.size[0] - num_chars * self._font_width

        draw_number_list(
            canvas,
            self._order["end"],
            self._order["num_ends"],
            pos,
            pos_y,
            self._roundFont,
            self._font_width,
            self._color_provider("GREEN"),
            self._color_provider("RED"),
            self._color_provider("ORANGE"),
        )
        return pos


class OrderTextDrawable(OrderDrawable):
    def __init__(self, color_provider):
        super().__init__(color_provider)
        self._font = get_font("Crono_9x16.pil")
        self._pos_y = 44

    def draw_order(self, pil_img, canvas):
        super(OrderTextDrawable, self).draw_order(pil_img, canvas)
        if self._order["end_name"] == "Untie":
            text = "DESEMPATE"
        else:
            text = f'{self._order["end_name"]} PUNTOS'
        draw_text_centered(
            canvas,
            text,
            self._font,
            self._color_provider(self._order["number_color"] or "BLACK"),
            self._pos_y,
        )


class TournamentTimerDrawable(OrderDrawable):
    def __init__(
        self,
        num_cols,
        num_rows,
        order_observable,
        color_provider,
        animator_controller,
        canvas_controller,
        timer_type=None,
    ):
        super().__init__(color_provider)
        self._drawable = None
        self._main_drawable = ComposedOrderDrawable(color_provider)
        self._main_drawable.add_drawable(MainTimerDrawable(color_provider))
        self._timer_type = timer_type
        if timer_type == WARMING:
            self._main_drawable.add_drawable(RoundEndDrawable(color_provider, "CAL"))
        elif timer_type == PLAYOFF:
            self._main_drawable.add_drawable(OrderTextDrawable(color_provider))
        else:
            self._main_drawable.add_drawable(RoundEndDrawable(color_provider, None))

        self._num_cols = num_cols
        self._canvas_controller = canvas_controller
        self._animator_controller = animator_controller
        self._animation = None
        self._subscription = order_observable.subscribe(
            on_next=lambda o: self.set_order(o)
        )

    def __del__(self):
        print("deleted")

    def set_order(self, order):
        super(TournamentTimerDrawable, self).set_order(order)
        if self._order["finished"]:
            self._drawable = None
            if self._animation:
                self._animation.stop()
                self._animation = None
            self._animation = get_finished_tournament_drawable(
                self._timer_type,
                self._animator_controller,
                self._canvas_controller,
                self._color_provider,
            )
        elif self._order["text"]:
            self._drawable = None
            if self._animation:
                self._animation.stop()
                self._animation = None
            self._animation = get_next_end_animation(
                self._order["text"],
                self._color_provider,
                self._animator_controller,
                self._canvas_controller,
                self.get_next_end_secondary_text(),
            )
        else:
            if self._animation:
                self._animation.stop()
                self._animation = None
            self._drawable = self._main_drawable
            self._main_drawable.set_order(order)

    def get_next_end_secondary_text(self):
        secondary_text = self._order["secondary_text"]
        if self._timer_type == PLAYOFF:
            if secondary_text != None:
                if self._order["end_name"] != "Untie":
                    secondary_text = (
                        f"{secondary_text} - {self._order['end_name']} PUNTOS"
                    )
                else:
                    secondary_text = f"{secondary_text} - DESEMPATE"
        return secondary_text

    def draw_order(self, pil_img, canvas):
        super(TournamentTimerDrawable, self).draw_order(pil_img, canvas)
        if not self._drawable:
            return
        self._drawable.draw(pil_img, canvas)

    def dispose(self):
        self._subscription.dispose()
        if self._animation:
            self._animation.stop()


class ClockDrawable(Drawable):
    def __init__(self, color_provider, width):
        super().__init__()
        self._color_provider = color_provider
        self._drawable = TextDrawable(
            "", "Crono_17x29_Bold.pil", color_provider("GREEN"), 0, 25
        )
        self._width = width

    def set_order(self, order):
        self._drawable.text = time_representation(order["number"])
        self._drawable.center_x(self._width)
        self._drawable.fill = self._color_provider(order["number_color"])

    def draw(self, pil_img, canvas, offset_x=0, offset_y=0):
        super(ClockDrawable, self).draw(pil_img, canvas, offset_x, offset_y)
        self._drawable.draw(pil_img, canvas)


class TimerDrawable(OrderDrawable):
    def __init__(
        self,
        num_cols,
        order_observable,
        color_provider,
        animator_controller,
        canvas_controller,
    ):
        super().__init__(color_provider)
        self._text_drawable = None
        self._color_provider = color_provider
        self._drawable = None
        self._num_cols = num_cols
        self._time_drawable = None
        self._animator_controller = animator_controller
        self._canvas_controller = canvas_controller
        self._subscription = order_observable.subscribe(
            on_next=lambda o: self.set_order(o)
        )

    def create_main_drawable(self, text):
        (self._text_drawable, self._animation) = animate_text2(
            text,
            "9x15.pil",
            self._color_provider("GREEN"),
            0,
            self._animator_controller,
            self._canvas_controller,
        )
        self._time_drawable = ClockDrawable(self._color_provider, self._num_cols)

    def set_order(self, order):
        super(TimerDrawable, self).set_order(order)

        if order["finished"]:
            self._animation.stop()
            self._animation = None
            self._time_drawable = None
            (drawable, self._animation) = animate_text2(
                order["text"],
                "9x15.pil",
                self._color_provider("RED"),
                20,
                self._animator_controller,
                self._canvas_controller,
            )
        else:
            if self._time_drawable == None:
                self.create_main_drawable(order["text"])
            self._time_drawable.set_order(order)
            self._text_drawable.fill = self._color_provider(order["number_color"])

    def draw_order(self, pil_img, canvas):
        super(TimerDrawable, self).draw_order(pil_img, canvas)
        if not self._time_drawable:
            return
        self._time_drawable.draw(pil_img, canvas)

    def dispose(self):
        self._subscription.dispose()
        if self._animation:
            self._animation.stop()


class ClassificationDrawable(OrderDrawable):
    def __init__(
        self,
        order_observable,
        color_provider,
        animator_controller,
        canvas_controller,
        club_provider,
    ):
        super().__init__(color_provider)
        self._animation = None
        self._color_provider = color_provider
        self._time_drawable = None
        self._animator_controller = animator_controller
        self._canvas_controller = canvas_controller
        self._subscription = order_observable.subscribe(
            on_next=lambda o: self.set_order(o)
        )
        self._classification_order = None
        self._division = None
        self._phase = None
        self._club_provider = club_provider

    def set_order(self, order):
        super(ClassificationDrawable, self).set_order(order)
        if order["finished"]:
            if self._animation:
                self._animation.stop()
            self._canvas_controller.clear_drawables()
            self._animation = None
        elif order["end_name"] != self._division:
            self._division = order["end_name"]
            if self._animation:
                self._animation.stop()
            self._canvas_controller.clear_drawables()
            (text_drawable, self._animation) = animate_text2(
                self._division,
                "Crono_10x18.pil",
                self._color_provider("GREEN"),
                0,
                self._animator_controller,
                self._canvas_controller,
                drawable_init=lambda i: i.center_y(64),
            )
        elif order["phase"] != self._phase:
            self._phase = order["phase"]
            if self._animation:
                self._animation.stop()
            self._canvas_controller.clear_drawables()
            if order["phases_names"][self._phase] == "Podium":
                self._animation = get_medals_animation(
                    self._animator_controller, self._canvas_controller
                )
            elif order["phase"] != 0:
                club = self._club_provider(order["secondary_text"])
                if club != None:
                    self._animation = get_classification_animation(
                        order["phases_names"][self._phase],
                        order["text"],
                        club,
                        get_color,
                        self._animator_controller,
                        self._canvas_controller,
                    )

    def dispose(self):
        self._subscription.dispose()
        if self._animation:
            self._animation.stop()
