import uuid


class Drawable(object):
    def __init__(self, pos_x=0, pos_y=0):
        self.pos_x = pos_x
        self.pos_y = pos_y
        self._uuid = uuid.uuid4()

    def __eq__(self, other):
        return self.__class__ == other.__class__ and self._uuid == other._uuid

    def __hash__(self):
        return hash(self._uuid)

    @property
    def pos(self):
        return (self.pos_x, self.pos_y)

    @pos.setter
    def pos(self, value):
        (self.pos_x, self.pos_y) = value

    @property
    def pos_x(self):
        return self._pos_x

    @pos_x.setter
    def pos_x(self, value):
        self._pos_x = value

    @property
    def pos_y(self):
        return self._pos_y

    @pos_y.setter
    def pos_y(self, value):
        self._pos_y = value

    def draw(self, pil_img, canvas, offset_x=0, offset_y=0):
        pass


class ComposedDrawable(Drawable):
    def __init__(self):
        super().__init__()
        self._drawables = []

    def add_drawable(self, drawable):
        self._drawables.append(drawable)
        return self

    def get_drawables(self):
        return self._drawables

    def add_composed_drawable(self, drawable):
        self._drawables = self._drawables + drawable.get_drawables()

    def draw(self, pil_img, canvas, offset_x=0, offset_y=0):
        super(ComposedDrawable, self).draw(pil_img, canvas, offset_x, offset_y)
        for drawable in self._drawables:
            drawable.draw(
                pil_img, canvas, offset_x + self.pos_x, offset_y + self._pos_y
            )
