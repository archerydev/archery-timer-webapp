import time
from .helpers import get_image
from .drawable import Drawable
from math import floor


class ImageDrawable(Drawable):
    def __init__(self, image_path, pos_x=0, pos_y=0, scale=1, background=None):
        super().__init__(pos_x, pos_y)
        self._original_image = get_image(image_path)
        self._image = self._original_image
        self.scale = 1
        self._background = background

    @property
    def size(self):
        return self._image.size

    @property
    def scale(self):
        return self._scale

    @scale.setter
    def scale(self, value):
        self._scale = value
        if self._scale == 0:
            self._image = None
        else:
            self.scale_w = int(self._original_image.size[0] * self._scale)
            self.scale_h = int(self._original_image.size[1] * self._scale)
            if self.scale_w != 0 and self.scale_h != 0:
                self._image = self._original_image.resize((self.scale_w, self.scale_h))
            else:
                self._image = None

    @property
    def scale_w(self):
        return self._scale_w

    @scale_w.setter
    def scale_w(self, value):
        self._scale_w = value

    @property
    def scale_h(self):
        return self._scale_h

    @scale_h.setter
    def scale_h(self, value):
        self._scale_h = value

    def draw(self, pil_img, canvas, offset_x=0, offset_y=0):
        super(ImageDrawable, self).draw(pil_img, canvas, offset_x, offset_y)
        if self._image != None:
            if self._background:
                canvas.rectangle(
                    [
                        self._pos_x + offset_x,
                        self._pos_y + offset_y,
                        self._pos_x + offset_x + self._image.size[0],
                        self._pos_y + offset_y + self._image.size[1],
                    ],
                    fill=self._background,
                )
            pil_img.paste(
                self._image,
                (self.pos_x + offset_x, self.pos_y + offset_y),
                mask=self._image,
            )
