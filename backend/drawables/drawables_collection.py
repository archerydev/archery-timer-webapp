from easing_functions import *
from math import floor
from numpy.random import seed, randint, rand
from .drawable import ComposedDrawable
from .image_drawable import ImageDrawable
from .drawable2 import (
    TextDrawable,
    Animator2,
    SpriteDrawable2,
    StaticAnimation,
    ParallelAnimator,
    RectangleDrawable,
)

from model.colors import get_color
from .helpers import get_font, get_club_image_path

TOURNAMENT = "Tournament_timer"
PLAYOFF = "PlayOff_timer"
WARMING = "Warming_timer"
ARROW = "Arrow_timer"
TIMER = "Normal_timer"

seed(1)


def animate_text2(
    text,
    font,
    fill,
    pos_y,
    animation_controller,
    canvas_controller,
    max_cycles=-1,
    speed=5,
    start=True,
    drawable_init=None,
):
    text_drawable = TextDrawable(text, font, fill, pos_y=pos_y)
    if drawable_init != None:
        drawable_init(text_drawable)
    text_animation = Animator2(animation_controller, max_cycles)
    text_animation.build_animation(
        text_drawable,
        0,
        LinearInOut(start=128, end=-text_drawable.size[0], duration=30),
        "pos_x",
        lambda v: floor(v),
        on_start=canvas_controller.add_drawable,
        on_finished=canvas_controller.remove_drawable,
    )
    if start:
        text_animation.start()
    return (text_drawable, text_animation)


def get_next_end_animation(
    text,
    color_provider,
    animation_controller,
    canvas_controller,
    secondary_text=None,
    start=True,
):
    main_text = f'{text} {secondary_text or ""}'
    main_text_drawable = TextDrawable(
        main_text, "Crono_9x16.pil", color_provider("RED")
    ).center_y(64)
    (font_name, posy) = calculate_font_secondary(128, secondary_text)
    small_text = TextDrawable(text, "6x12.pil", color_provider("RED"), 0, 0).center_x(
        128
    )
    sec_text_drawable = TextDrawable(
        f'{secondary_text or ""}', font_name, color_provider("RED"), 0, posy
    ).center_x(128)
    animator = Animator2(
        animation_controller,
        1,
        on_start=canvas_controller.add_drawable,
        on_finished=canvas_controller.remove_drawable,
    )
    animator.build_animation(
        main_text_drawable,
        0,
        LinearInOut(start=128, end=-main_text_drawable.size[0], duration=30),
        "pos_x",
        lambda v: floor(v),
        3,
    )
    animator.build_animation(small_text, 90, StaticAnimation(duration=-1))
    animator.build_animation(sec_text_drawable, 90, StaticAnimation(duration=-1))
    if start:
        animator.start()
    return animator


def calculate_font_secondary(width, secondary_text):
    temp_Font = get_font("Crono_20x40_Bold.pil")
    font_name = "Crono_20x40_Bold.pil"
    posy = 10
    # TODO: patch for be able to show long text of playoff. needs proper implementation
    if secondary_text != None and temp_Font.getsize(secondary_text)[0] > width:
        font_name = "9x15.pil"
        posy = 20
    return (font_name, posy)


def get_finished_tournament_drawable(
    timer_type, animation_controller, canvas_controller, color_provider
):
    if timer_type == TOURNAMENT:
        fireworks = get_fireworks_animation(
            animation_controller, canvas_controller, -1, False
        )
        (drawable, text_animator) = animate_text2(
            "Torneo Finalizado",
            "9x15.pil",
            color_provider("GREEN"),
            40,
            animation_controller,
            canvas_controller,
            start=False,
        )
        animator = ParallelAnimator()
        animator.add_animator(fireworks)
        animator.add_animator(text_animator)
    else:
        if timer_type == PLAYOFF:
            finish_text = "Entrada Finalizada"
        elif timer_type == ARROW:
            finish_text = "Flecha Finalizada"
        else:
            finish_text = "Calentamiento Finalizado"
        (drawable, animator) = animate_text2(
            finish_text,
            "Crono_9x16.pil",
            color_provider("RED"),
            20,
            animation_controller,
            canvas_controller,
            start=False,
        )
    animator.start()
    return animator


def get_club_animation(
    club, animation_controller, canvas_controller, max_cycles=-1, speed=5, start=True
):
    return get_logo_animation(
        get_club_image_path(club),
        animation_controller,
        canvas_controller,
        max_cycles,
        speed,
        start,
    )


def get_logo_animation(
    filename,
    animation_controller,
    canvas_controller,
    max_cycles=-1,
    speed=5,
    start=True,
):
    logo_animated = Animator2(animation_controller, max_cycles, speed)
    logo = ImageDrawable(filename)
    scale = min((128 / logo.size[0]), (64 / logo.size[1]))
    logo.scale = scale
    size = logo.size
    image_right = 128 - size[0]
    image_center = floor(0 + 64 - size[0] / 2)
    logo_animated.build_animation(
        logo,
        0,
        QuadEaseInOut(start=0, end=scale, duration=30),
        "scale",
        on_start=canvas_controller.add_drawable,
    )
    logo_animated.build_animation(
        logo, 45, QuadEaseInOut(start=scale, end=0, duration=30), "scale"
    )
    logo_animated.build_animation(
        logo,
        0,
        QuadEaseInOut(start=image_right, end=image_center, duration=10),
        "pos_x",
        lambda v: floor(v),
    )
    logo_animated.build_animation(
        logo,
        10,
        QuadEaseInOut(start=image_center, end=image_right, duration=10),
        "pos_x",
        lambda v: floor(v),
    )
    logo_animated.build_animation(
        logo,
        20,
        QuadEaseInOut(start=image_right, end=image_center, duration=10),
        "pos_x",
        lambda v: floor(v),
    )
    logo_animated.build_animation(
        logo,
        45,
        QuadEaseInOut(start=image_center, end=image_right, duration=10),
        "pos_x",
        lambda v: floor(v),
    )
    logo_animated.build_animation(
        logo,
        55,
        QuadEaseInOut(start=image_right, end=image_center, duration=10),
        "pos_x",
        lambda v: floor(v),
    )
    logo_animated.build_animation(
        logo,
        65,
        QuadEaseInOut(start=image_center, end=image_right, duration=10),
        "pos_x",
        lambda v: floor(v),
        on_finished=canvas_controller.remove_drawable,
    )
    if start:
        logo_animated.start()
    return logo_animated


def get_fireworks_animation(
    animation_controller, canvas_controller, max_cycles=-1, start=True
):

    sprite_blue = SpriteDrawable2("blueshot.png")
    sprite_red = SpriteDrawable2("redshot.png")
    sprite_yellow = SpriteDrawable2("yellowshot.png")
    sprite_violet = SpriteDrawable2("violetshot.png")

    sprite_animator = Animator2(
        animation_controller,
        max_cycles,
        speed=20,
        on_finished=canvas_controller.remove_drawable,
    )
    sprite_animator.build_animation(
        sprite_blue,
        randint(0, 5, 1)[0],
        LinearInOut(start=0, end=7, duration=randint(15, 30, 1)[0]),
        "index",
        lambda v: floor(v),
        on_start=lambda d: firework_random_pos(d, canvas_controller),
    )
    sprite_animator.build_animation(
        sprite_red,
        randint(0, 5, 1)[0],
        LinearInOut(start=0, end=7, duration=randint(15, 30, 1)[0]),
        "index",
        lambda v: floor(v),
        on_start=lambda d: firework_random_pos(d, canvas_controller),
    )
    sprite_animator.build_animation(
        sprite_yellow,
        randint(0, 5, 1)[0],
        LinearInOut(start=0, end=7, duration=randint(15, 30, 1)[0]),
        "index",
        lambda v: floor(v),
        on_start=lambda d: firework_random_pos(d, canvas_controller),
    )
    sprite_animator.build_animation(
        sprite_violet,
        randint(0, 5, 1)[0],
        LinearInOut(start=0, end=7, duration=randint(15, 30, 1)[0]),
        "index",
        lambda v: floor(v),
        on_start=lambda d: firework_random_pos(d, canvas_controller),
    )
    if start:
        sprite_animator.start()
    return sprite_animator


def firework_random_pos(drawable, canvas_controller):
    drawable.pos = (randint(0, 100, 1)[0], randint(0, 40, 1)[0])
    canvas_controller.add_drawable(drawable)


def get_medals_animation(
    animation_controller, canvas_controller, max_cycles=1 - 1, start=True
):
    sprite_animator = Animator2(
        animation_controller,
        max_cycles,
        on_start=canvas_controller.add_drawable,
        on_finished=canvas_controller.remove_drawable,
    )

    podium = ImageDrawable("podium.png", 24, 39)
    sprite_second = SpriteDrawable2("second_medal.png", 16, 10, 7, 40, 40, 40)
    sprite_first = SpriteDrawable2("first_medal.png", 44, 4, 7, 40, 40, 40)
    sprite_third = SpriteDrawable2("third_medal.png", 70, 14, 7, 40, 40, 40)
    sprite_animator.build_animation(
        sprite_first,
        10,
        LinearInOut(start=0, end=6, duration=20),
        "index",
        lambda v: floor(v),
        5,
    )
    sprite_animator.build_animation(sprite_first, 110, StaticAnimation(duration=-1))
    sprite_animator.build_animation(
        sprite_second,
        5,
        LinearInOut(start=0, end=6, duration=10),
        "index",
        lambda v: floor(v),
        5,
    )
    sprite_animator.build_animation(sprite_second, 55, StaticAnimation(duration=-1))
    sprite_animator.build_animation(
        sprite_third,
        0,
        LinearInOut(start=0, end=6, duration=18),
        "index",
        lambda v: floor(v),
        5,
    )
    sprite_animator.build_animation(sprite_third, 90, StaticAnimation(duration=-1))
    sprite_animator.build_animation(podium, 0, StaticAnimation(duration=-1))
    if start:
        sprite_animator.start()
    return sprite_animator


def get_classification_animation(
    position,
    name,
    club,
    color_provider,
    animation_controller,
    canvas_controller,
    max_cycles=-1,
    speed=8,
    start=True,
):
    logo_animated = Animator2(animation_controller, max_cycles, speed)
    logo = ImageDrawable(get_club_image_path(club))
    position_drawable = TextDrawable(
        str(position), "Crono_20x40.pil", get_color("RED")
    ).center(128, 64)
    name_drawable = TextDrawable(
        name, "Crono_9x16.pil", (11, 128, 21, 255), 0
    ).center_y(32)
    rectangle_drawable = RectangleDrawable(128, 64, 0, 0, (255, 255, 255, 255))
    club_drawable = TextDrawable(club.name, "9x15.pil", (16, 37, 128, 255), 0).center_y(
        32, 32
    )
    scale = min((128 / logo.size[0]), (64 / logo.size[1]))
    logo.scale = scale
    size = logo.size
    image_right = 128 - size[0]
    image_center = floor(0 + 64 - size[0] / 2)
    image_left_out = 0 - size[0]
    name_center = name_drawable.center_x(128).pos[0]
    club_center = club_drawable.center_x(128).pos[0]
    position_drawable_center = position_drawable.pos[0]
    logo_animated.build_animation(
        rectangle_drawable,
        0,
        StaticAnimation(135),
        on_start=canvas_controller.add_drawable,
        on_finished=canvas_controller.remove_drawable,
    )
    logo_animated.build_animation(
        position_drawable,
        0,
        LinearInOut(start=255, end=0, duration=10),
        "fill",
        lambda v: (255, floor(v), floor(v), 255),
        on_start=canvas_controller.add_drawable,
    )
    logo_animated.build_animation(
        position_drawable,
        40,
        LinearInOut(start=0, end=255, duration=10),
        "fill",
        lambda v: (255, floor(v), floor(v), 255),
        on_finished=canvas_controller.remove_drawable,
    )
    logo_animated.build_animation(
        logo,
        20,
        QuadEaseInOut(start=0, end=scale, duration=30),
        "scale",
        on_start=canvas_controller.add_drawable,
    )
    logo_animated.build_animation(
        logo,
        20,
        QuadEaseInOut(start=image_right, end=image_center, duration=10),
        "pos_x",
        lambda v: floor(v),
    )
    logo_animated.build_animation(
        logo,
        30,
        QuadEaseInOut(start=image_center, end=image_right, duration=10),
        "pos_x",
        lambda v: floor(v),
    )
    logo_animated.build_animation(
        logo,
        40,
        QuadEaseInOut(start=image_right, end=image_center, duration=10),
        "pos_x",
        lambda v: floor(v),
    )
    logo_animated.build_animation(
        logo,
        65,
        QuadEaseInOut(start=image_center, end=image_left_out, duration=10),
        "pos_x",
        lambda v: floor(v),
        on_finished=canvas_controller.remove_drawable,
    )
    logo_animated.build_animation(
        name_drawable,
        62,
        LinearInOut(start=128, end=-name_drawable.size[0], duration=35),
        "pos_x",
        lambda v: floor(v),
        repeat=2,
        on_start=canvas_controller.add_drawable,
        on_finished=canvas_controller.remove_drawable,
    )
    logo_animated.build_animation(
        club_drawable,
        62,
        LinearInOut(start=128, end=-club_drawable.size[0], duration=35),
        "pos_x",
        lambda v: floor(v),
        repeat=2,
        on_start=canvas_controller.add_drawable,
        on_finished=canvas_controller.remove_drawable,
    )
    if start:
        logo_animated.start()
    return logo_animated
