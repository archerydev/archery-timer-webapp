from .drawable import Drawable
from .helpers import (
    get_font,
    center_pos,
    center_posx,
    center_posy,
    get_images_from_sprite,
)
import time
from collections import defaultdict, deque


class TextDrawable(Drawable):
    def __init__(self, text, font, fill, pos_x=0, pos_y=0):
        super().__init__(pos_x, pos_y)
        self.text = text
        self.font = font
        self.fill = fill
        self.text = text

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        self._text = value

    @property
    def fill(self):
        return self._fill

    @fill.setter
    def fill(self, value):
        self._fill = value

    @property
    def font(self):
        return self._font

    @font.setter
    def font(self, value):
        self._font = get_font(value)

    @property
    def size(self):
        return self._font.getsize(self.text)

    def center_x(self, width, x_offset=0):
        self.pos_x = center_posx(self.text, self.font, width) + x_offset
        return self

    def center_y(self, height, y_offset=0):
        self.pos_y = center_posy(self.text, self.font, height) + y_offset
        return self

    def center(self, width, height, x_offset=0, y_offset=0):
        pos = center_pos(self.text, self.font, width, height)
        (self.pos_x, self.pos_y) = (pos[0] + x_offset, pos[1] + y_offset)
        return self

    def draw(self, pil_img, canvas, offset_x=0, offset_y=0):
        super(TextDrawable, self).draw(pil_img, canvas, offset_x, offset_y)
        canvas.text(
            [self.pos_x + offset_x, self.pos_y + offset_y],
            self.text,
            fill=self.fill,
            font=self.font,
        )


class SpriteDrawable2(Drawable):
    def __init__(
        self,
        sprite_name,
        pos_x=0,
        pos_y=0,
        num_images=8,
        image_height=40,
        image_width=40,
        image_offset=42,
    ):
        super().__init__(pos_x, pos_y)
        self._images = get_images_from_sprite(
            sprite_name, num_images, image_height, image_width, image_offset
        )
        self.index = 0

    @property
    def num_images(self):
        return len(self._images)

    @property
    def index(self):
        return self._index

    @index.setter
    def index(self, value):
        self._index = value
        self._image = self._images[self._index]

    def draw(self, pil_img, canvas, offset_x=0, offset_y=0):
        super(SpriteDrawable2, self).draw(pil_img, canvas, offset_x, offset_y)
        pil_img.paste(
            self._image,
            (self.pos_x + offset_x, self.pos_y + offset_y),
            mask=self._image,
        )


class RectangleDrawable(Drawable):
    def __init__(self, num_cols, num_rows, pos_x=0, pos_y=0, fill=(0, 0, 0, 255)):
        super().__init__(pos_x, pos_y)
        self._num_cols = num_cols
        self._num_rows = num_rows
        self._fill = fill

    def draw(self, pil_img, canvas, offset_x=0, offset_y=0):
        super(RectangleDrawable, self).draw(pil_img, canvas, offset_x, offset_y)
        canvas.rectangle([0, 0, canvas.im.size[0], canvas.im.size[1]], fill=self._fill)


class Animation(object):
    def __init__(
        self,
        drawable,
        start_time,
        ease,
        attr_name=None,
        level_func=lambda v: v,
        repeat=1,
        on_start=None,
        on_finished=None,
    ):
        self._drawable = drawable
        self._start_time = start_time
        self._ease = ease
        self._attr_name = attr_name
        self._level_func = level_func
        self._repeat = repeat
        self._on_start = on_start
        self._on_finished = on_finished

    @property
    def drawable(self):
        return self._drawable

    @property
    def start_time(self):
        return self._start_time

    @property
    def ease(self):
        return self._ease

    @property
    def attr_name(self):
        return self._attr_name

    @property
    def level_func(self):
        return self._level_func

    @property
    def repeat(self):
        return self._repeat

    @property
    def on_start(self):
        return self._on_start

    @on_start.setter
    def on_start(self, value):
        self._on_start = value

    @property
    def on_finished(self):
        return self._on_finished

    @on_finished.setter
    def on_finished(self, value):
        self._on_finished = value

    def concatenate_animation(self, animation):
        animation.start_time += self.start_time + self.ease.duration
        return animation

    def clone_with_start_time(self, new_start_time):
        return Animation(
            self.drawable,
            new_start_time,
            self.ease,
            self.attr_name,
            self._level_func,
            self.repeat,
            self.on_start,
            self.on_finished,
        )


class Animator2(object):
    def __init__(
        self,
        animation_controller,
        max_cycles=-1,
        speed=5,
        on_start=None,
        on_finished=None,
    ):
        self._animation_seconds = 0
        self._animationSpeed = speed
        self._max_cycles = max_cycles
        self._num_rep = 0
        self._started = False
        self._animation_controller = animation_controller
        self._on_start = on_start
        self._on_finished = on_finished
        self._active_animations = []
        self._animations = []
        self._animationsdict = defaultdict(list)

    def set_speed(self, new_speed):
        self._animationSpeed = new_speed

    def restart(self):
        self._animationsdict.clear()
        for animation in self._animations:
            self.enquee_animation(animation)
        self._animation_seconds = 0
        self._eventqueue = deque(sorted(self._animationsdict))
        self._last_time = time.time()

    def start(self):
        if len(self._animations) == 0:
            return
        self.restart()
        self._num_rep = 0
        self._animation_controller.add_animation(self)
        self._started = True

    def stop(self):
        self._started = False
        for (
            drawable,
            start_time,
            ease,
            func,
            on_start,
            on_finished,
        ) in self._active_animations:
            if on_finished != None:
                on_finished(drawable)
        self._animation_controller.remove_animation(self)
        if self._num_rep > 0:
            self.finished = True

    def add_animation(self, animation):
        if animation.on_start == None:
            animation.on_start = self._on_start
        if animation.on_finished == None:
            animation.on_finished = self._on_finished
        self._animations.append(animation)

    def build_animation(
        self,
        drawable,
        start_time,
        ease,
        attr_name=None,
        level_func=lambda v: v,
        repeat=1,
        on_start=None,
        on_finished=None,
    ):
        animation = Animation(
            drawable,
            start_time,
            ease,
            attr_name,
            level_func,
            repeat,
            on_start,
            on_finished,
        )
        self.add_animation(animation)

    def merge_animator(self, animator):
        speed_adjust = self._animationSpeed / animator._animationSpeed
        for animation in animator.animations:
            animation.ease.duration *= speed_adjust
            self.add_animation(animation)

    def enquee_animation(self, animation):
        for i in range(0, animation.repeat):
            new_start_time = animation.start_time + animation.ease.duration * i
            if animation.attr_name != None:
                self._animationsdict[new_start_time].append(
                    (
                        animation.drawable,
                        new_start_time,
                        animation.ease,
                        lambda pos: setattr(
                            animation.drawable,
                            animation.attr_name,
                            animation.level_func(pos),
                        ),
                        animation.on_start,
                        animation.on_finished,
                    )
                )
            else:
                self._animationsdict[new_start_time].append(
                    (
                        animation.drawable,
                        new_start_time,
                        animation.ease,
                        None,
                        animation.on_start,
                        animation.on_finished,
                    )
                )

    @property
    def started(self):
        return self._started

    @property
    def finished(self):
        return self._finished

    @property
    def animations(self):
        return self._animations

    @finished.setter
    def finished(self, value):
        self._finished = value

    def step(self):
        d0 = time.time()
        step = (d0 - self._last_time) * self._animationSpeed
        self.advance_seconds(step)
        self._last_time = d0

    def advance_seconds(self, step):
        if not self.started:
            return
        self._animation_seconds = self._animation_seconds + step

        if len(self._eventqueue) == 0 and len(self._active_animations) == 0:
            self._num_rep = self._num_rep + 1
            if self._max_cycles == -1 or self._num_rep < self._max_cycles:
                self.restart()
            else:
                self.stop()
            return

        if len(self._eventqueue) > 0 and self._animation_seconds > self._eventqueue[0]:
            index = self._eventqueue.popleft()
            new_animations = self._animationsdict[index]
            for (
                drawable,
                start_time,
                animation,
                func,
                start,
                finish,
            ) in new_animations:
                if start != None:
                    start(drawable)
            self._active_animations += new_animations

        for i, (drawable, start_time, animation, func, start, finish) in enumerate(
            self._active_animations
        ):
            if (
                animation.duration != -1
                and start_time + animation.duration < self._animation_seconds
            ):
                del self._active_animations[i]
                if finish != None:
                    finish(drawable)
            elif func != None:
                func(animation.ease(self._animation_seconds - start_time))


class ParallelAnimator(object):
    def __init__(self):
        self._animators = []

    def add_animator(self, animator):
        self._animators.append(animator)

    def start(self):
        for animator in self._animators:
            animator.start()

    def stop(self):
        for animator in self._animators:
            animator.stop()


class StaticAnimation(object):
    def __init__(self, duration=-1):
        self.duration = duration

    def ease(self, seconds):
        pass
