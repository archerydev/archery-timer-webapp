// IMPORTANT: THIS FILE IS AUTO GENERATED! DO NOT MANUALLY EDIT OR CHECKIN!
/* tslint:disable */
export const VERSION = {
    "dirty": true,
    "raw": "v0.1.1-5-g3eca16b-dirty",
    "hash": "g3eca16b",
    "distance": 5,
    "tag": "v0.1.1",
    "semver": {
        "options": {
            "loose": false,
            "includePrerelease": false
        },
        "loose": false,
        "raw": "v0.1.1",
        "major": 0,
        "minor": 1,
        "patch": 1,
        "prerelease": [],
        "build": [],
        "version": "0.1.1"
    },
    "suffix": "5-g3eca16b-dirty",
    "semverString": "0.1.1+5.g3eca16b",
    "version": "0.0.0"
};
/* tslint:enable */
