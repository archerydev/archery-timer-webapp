export const environment = {
  production: true,
  apiUrl: "https://archerytimer.gpulido.com/api",
  wsUrl: "wss://archerytimer.gpulido.com/api",
};
