//https://www.jvandemo.com/how-to-use-environment-variables-to-configure-your-angular-application-without-a-rebuild/
(function (window) {

  window.__env = window.__env || {};
  // API url
  window.__env.apiUrl = 'http://localhost:8090/api';

  window.__env.wsUrl = 'ws://localhost:8090/api';

}(this));
