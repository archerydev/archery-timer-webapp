import { Component, Inject } from '@angular/core';
import { MdcDialogRef, MDC_DIALOG_DATA } from '@angular-mdc/web';

@Component({
    templateUrl: 'okcancel-dialog.component.html',
  })
export class OkCancelDialogComponent {
  mainText: string;
  secondaryText: string;
  buttonText: string;
  constructor(public dialogRef: MdcDialogRef<OkCancelDialogComponent>,
              @Inject(MDC_DIALOG_DATA) data: {mainText: string, secondaryText: string, buttonText: string}) {
      this.mainText = data.mainText;
      this.secondaryText = data.secondaryText;
      this.buttonText = data.buttonText;
  }

}
