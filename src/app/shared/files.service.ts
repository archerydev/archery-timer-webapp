import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as bdfcanvas from 'bdf-canvas';
import { Observable, of, from } from 'rxjs';
import { map, share, switchMap, mergeMap, tap } from 'rxjs/operators';
import { EnvService } from '../env.service';

@Injectable({ providedIn: 'root' })
export class FilesService {

    fonts = new Map<string, bdfcanvas.BDFFont>();
    images = new Map<string, ImageBitmap>();
    observables = new Map<string, Observable<any>>();
    constructor(private http: HttpClient, private envService: EnvService) {

    }

    getFont(fontName: string):Observable<any> {

        if (this.fonts.get(fontName)) {
          return of(this.fonts[fontName]);
        } else if (this.observables.get(fontName)) {
          return this.observables.get(fontName);
        } else {
            this.observables.set(fontName,
              this.http.get('./assets/' + fontName, {responseType: 'text'})
                  .pipe(
                      map(response => {
                        this.observables.delete(fontName);
                        this.fonts.set(fontName, new bdfcanvas.BDFFont(response));
                        return this.fonts.get(fontName);
                  })
                  , share()
                  )
                );
        }
        return this.observables.get(fontName);
      }

      getImage(imageName: string) {
        const image = this.images.get(imageName);
        if (image) {
          return of(this.images[imageName]);
        } else if (this.observables.get(imageName)) {
          return this.observables.get(imageName);
        } else {
          this.observables.set(imageName,
            this.http.get('./assets/' + imageName, {responseType: 'blob'})
              .pipe(
                    switchMap(d => from(createImageBitmap(d)))
                  , map(response  => {
                      this.observables.delete(imageName);
                      this.images.set(imageName, response);
                      return this.images.get(imageName);
                    })
                    , share()
                    )
                );
        }
        return this.observables.get(imageName);
      }

}
