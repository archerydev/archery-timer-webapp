import { Component, OnInit } from '@angular/core';
import { MdcDialogRef } from '@angular-mdc/web';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import * as fromRoot from '../app.reducer';
import * as TimerActions from '../timer/store/timer.actions';
import { Round, End, Phase } from '../timer/timer.model';
import { Timer, TimerOrder } from '../timer/timer-api.model';

const range = (start, end) => Array.from({length: (end - start)}, (v, k) => k + start);
@Component({
    templateUrl: 'setorder-dialog.component.html',
  })
export class SetOrderDialogComponent  implements OnInit {

  currentTimer$: Observable<Timer>;
  currentOrder$: Observable<TimerOrder>;
  isLoading$: Observable<boolean>;

  round: Round;
  end: End;
  phase: Phase;
  maxNumber = 240;
  minNumber = 0;


  orderForm = new FormGroup({
    round: new FormControl('', Validators.required),
    end: new FormControl('', Validators.required),
    phase: new FormControl('', Validators.required),
    number: new FormControl('', [Validators.required,
      (control: AbstractControl) => Validators.min(this.minNumber)(control),
      (control: AbstractControl) => Validators.max(this.maxNumber)(control)]
      )
  });

  constructor(public dialogRef: MdcDialogRef<SetOrderDialogComponent>,
              private store: Store<fromRoot.State>) { }

  timer: Timer;
  ngOnInit() {

    this.currentTimer$ = this.store.select(fromRoot.getCurrentTimer);
    this.currentOrder$ = this.store.select(fromRoot.getTimerOrder);
    this.store.select(fromRoot.getTimerOrder).subscribe(to =>
        this.orderForm.patchValue(to)
      );
    this.store.select(fromRoot.getCurrentTimer)
        .subscribe( timer => {
          this.timer = timer;
          this.round = this.timer.rounds[0];
          this.end = this.round.ends[0];
          this.phase = this.end.phases[0];
          this.minNumber = this.phase.end_value;
          this.maxNumber = this.phase.init_value;
        }
      );
  }

  onRoundChange(round: {index: number, value: string}) {
    if (this.timer) {
      this.round = this.timer.rounds[+round.value];
   //   this.orderForm.updateValueAndValidity();
    }

  }
  getEnds() {
    if (this.round) {
      return range(1, this.round.num_ends + 1);
    }
    return [0];
  }

  onEndChange(end: {index: number, value: string}) {
    if (this.round) {
      this.end = this.round.ends[+end.value];
    
    }
  }

  getPhases() {
    if (this.end) {
      return this.end.phases;
  }

    return [];

  }

  onPhaseChange(phase: {index: number, value: number}) {
    if (this.end) {
      this.phase = this.end.phases[+phase.value];
      this.minNumber = this.phase.end_value;
      this.maxNumber = this.phase.init_value;
      // this.orderForm.updateValueAndValidity();
    }
  }

  submit(): void {
    if (this.orderForm.invalid) {
      return;
    }
    const order: TimerOrder = {
      round: +this.orderForm.value.round,
      end: +this.orderForm.value.end,
      phase: +this.orderForm.value.phase,
      number: this.orderForm.value.number,
    };
    this.store.dispatch(new TimerActions.LoadTimerOrder(order));
    this.dialogRef.close();
  }

}
