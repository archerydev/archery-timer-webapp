import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from 'src/app/material.module';
import { ColorPickerModule } from 'ngx-color-picker';
import { OkCancelDialogComponent } from './okcancel-dialog.component';
import { SetOrderDialogComponent } from './setorder-dialog.component';
import { ArcheryModule } from '../club/archery.module';
import { UploadFileStoreModule } from '../upload-file/upload-file-store.module';

@NgModule({
  declarations: [
    OkCancelDialogComponent,
    SetOrderDialogComponent
  ],
  imports: [
    MaterialModule,
    CommonModule,
    ReactiveFormsModule,
    UploadFileStoreModule,
    ArcheryModule
  ],
  exports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    FlexLayoutModule,
    ColorPickerModule
  ],
  entryComponents: [OkCancelDialogComponent, SetOrderDialogComponent]
})
export class SharedModule {}
