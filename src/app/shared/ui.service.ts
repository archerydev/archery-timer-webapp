import { Injectable } from '@angular/core';
import { MdcSnackbar } from '@angular-mdc/web';


@Injectable({
    providedIn: 'root'
  })
export class UIService {

  constructor(private snackbar: MdcSnackbar) {}

  showSnackbar(message, action) {
    this.snackbar.open(message, action);
  }
}
