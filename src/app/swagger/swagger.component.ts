import { Component, AfterViewInit, ElementRef } from '@angular/core';
import SwaggerUI from 'swagger-ui';
import { EnvService } from '../env.service';

@Component({
  selector: 'app-swagger',
  templateUrl: './swagger.component.html',
  styleUrls: ['./swagger.component.scss']
})
export class SwaggerComponent implements AfterViewInit {

  constructor(private el: ElementRef, private env: EnvService) { }

  ngAfterViewInit() {

    const ui = SwaggerUI({
      url:  this.env.apiUrl + '/schema',
      domNode: this.el.nativeElement.querySelector('.swagger-container'),
      deepLinking: true,
      presets: [
        SwaggerUI.presets.apis
      ],
    });
  }
}
