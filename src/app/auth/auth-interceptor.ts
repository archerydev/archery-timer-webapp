import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { Injectable } from '@angular/core';

import * as fromRoot from '../app.reducer';
import { Store } from '@ngrx/store';
import { first, flatMap } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private store: Store<fromRoot.State>) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return this.store.select(fromRoot.getToken).pipe(
      first()
      , flatMap(token => {
        const authReq = !!token ? req.clone({
          setHeaders: { Authorization: 'Bearer ' + token },
        }) : req;
        return next.handle(authReq);
      })
    );
  }
  // intercept(req: HttpRequest<any>, next: HttpHandler) {
  //   return this.store.select(fromRoot.getToken).pipe(
  //     authToken => {
  //       if (authToken){
  //         const authRequest = req.clone({
  //           headers: req.headers.set('Authorization', "Bearer " + authToken)
  //         });
  //         return next.handle(authRequest);
  //       }
  //       return next.handle(req);
  //     }
  //   );

  // }
}
