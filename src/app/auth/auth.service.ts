import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';
import { EnvService } from '../env.service';

import { HttpClient } from '@angular/common/http';
import * as AuthActions from './store/auth.actions';
import { Store } from '@ngrx/store';
import * as fromRoot from '../app.reducer';

//const BACKEND_URL = environment.apiUrl + '/user/';
@Injectable({ providedIn: "root" })
export class AuthService {

  private tokenTimer: any;
  private BACKEND_URL: string;
  constructor(private env: EnvService, private store: Store<fromRoot.State>, private http: HttpClient) {
    this.BACKEND_URL = this.env.apiUrl + '/user/';
    this.store.select(fromRoot.getExpDate).subscribe(expDate => {
        if (!expDate) {
          clearTimeout(this.tokenTimer);
        } else {
            const now = new Date();
            const expiresIn = expDate.getTime() - now.getTime();
            if (expiresIn > 0) {
              this.setAuthTimer(expiresIn / 1000);
            } else {
              this.store.dispatch(new AuthActions.Logout());
            }
        }
    });
  }

  loginObs(authData: { username: string, password: string }): Observable<{ token: string, expiresIn: number }> {
    return this.http.post<{ token: string, expiresIn: number }>(this.BACKEND_URL + 'login', authData);
  }

  private setAuthTimer(duration: number) {
    this.tokenTimer = setTimeout(() => {
      this.store.dispatch(new AuthActions.Logout());
    }, duration * 1000);
  }


}
