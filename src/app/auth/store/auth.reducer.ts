import { Action } from '@ngrx/store';

import { AuthActions,
        SET_TOKEN,
        SIGNIN,
        LOGOUT} from './auth.actions';

export interface State {
  token: string;
  expDate: Date;
  authenticated: boolean;
}

const initialState: State = {
  token: null,
  expDate: null,
  authenticated: false
};

export function authReducer(state = initialState, action: AuthActions) {
  switch (action.type) {
    case SIGNIN:
      return {
        ...state,
        authenticated: true
      };
    case LOGOUT:
      return {
        ...state,
        token: null,
        expDate: null,
        authenticated: false
      };
    case SET_TOKEN:
      return {
        ...state,
        token: action.payload.token,
        expDate: action.payload.expDate

      };
    default:
      return state;
  }
}

export const getIsAuth = (state: State) => state.authenticated;
export const getExpDate = (state: State) => state.expDate;
export const getToken = (state: State) => state.token;
