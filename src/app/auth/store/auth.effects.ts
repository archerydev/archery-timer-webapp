import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { map, tap, switchMap, mergeMap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import * as fromRoot from '../../app.reducer';

import * as AuthActions from './auth.actions';
import * as UIActions from '../../shared/ui.actions';
import { environment } from 'src/environments/environment';
import { Action, Store } from '@ngrx/store';
import { UIService } from 'src/app/shared/ui.service';
import { AuthService } from '../auth.service';

export class EffectError implements Action {
    readonly type = '[Error] Effect Error';
}
@Injectable()
export class AuthEffects {

    @Effect()
    authSignin = this.actions$
        .pipe(
            ofType(AuthActions.TRY_SIGNIN)
            , map((action: AuthActions.TrySignin) => {
                return action.payload;
            })
            , switchMap((authData: { username: string, password: string }) => {
                this.store.dispatch(new UIActions.StartLoading());
                return this.authService.loginObs(authData)
                    .pipe(
                         map(response => response)
                        , mergeMap(response => {
                            const now = new Date();
                            const newExpDate = new Date(now.getTime() + response.expiresIn * 1000);
                            this.store.dispatch(new UIActions.StopLoading());
                            this.router.navigate(['/']);
                            return [
                                {
                                    type: AuthActions.SIGNIN
                                },
                                {
                                    type: AuthActions.SET_TOKEN,
                                    payload: { token: response.token, expDate: newExpDate }
                                }
                            ];
                        })
                        , catchError(error => {
                            console.log(error);
                            let msg = error.error.msg;
                            if (!msg) {
                                msg = error.message;
                            }
                            this.store.dispatch(new UIActions.StopLoading());
                            this.uiService.showSnackbar(msg, null);
                            return of(new EffectError());
                        })
                    );
            }
            )
        );

    @Effect({ dispatch: false })
    authLogout = this.actions$
        .pipe(
            ofType(AuthActions.LOGOUT)
            , tap(() => {
                this.router.navigate(['/']);
            }));

    constructor(private actions$: Actions,
        private authService: AuthService,
        private router: Router,
        private uiService: UIService,
        private store: Store<fromRoot.State>) { }
}
