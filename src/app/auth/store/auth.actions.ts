import { Action } from '@ngrx/store';

export const SET_AUTHENTICATED = '[Auth] Set Authenticated';
export const SET_UNAUTHENTICATED = '[Auth] Set Unauthenticated';
export const TRY_SIGNIN = '[Auth] Try Signin';
export const SIGNIN = '[Auth] Signin';
export const LOGOUT = '[Auth] Logout';
export const SET_TOKEN = '[Auth] Set Token';

export class TrySignin implements Action {
  readonly type = TRY_SIGNIN;

  constructor(public payload: {username: string, password: string}) {}
}


export class Signin implements Action {
  readonly type = SIGNIN;
}

export class Logout implements Action {
  readonly type = LOGOUT;
}

export class SetToken implements Action {
  readonly type = SET_TOKEN;

  constructor(public payload: {token: string, expDate: Date}) {}
}

export type AuthActions =  Signin | Logout | SetToken | TrySignin;
