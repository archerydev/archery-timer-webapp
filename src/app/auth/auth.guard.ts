import { CanActivate,
         ActivatedRouteSnapshot,
         RouterStateSnapshot,
         UrlTree,
         Router,
         CanLoad,
         Route} from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { take, map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import * as AuthActions from './store/auth.actions';


import * as fromRoot from '../app.reducer';

@Injectable()
export class AuthGuard implements CanActivate, CanLoad {

  constructor(private store: Store<fromRoot.State>, private router: Router ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) :
  boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {

    return this.store.select(fromRoot.getToken)
          .pipe(
            map(token => {
              const jwtHelper = new JwtHelperService();
              if (jwtHelper.isTokenExpired(token)){
                this.store.dispatch(new AuthActions.Logout())
                this.router.navigate(['/','auth','login']);
                return false;
              }
              return true;
            })
            , take(1)
            );

    //return this.store.select(fromRoot.getIsAuth).pipe(take(1));
  }

  canLoad(route: Route) {
    return this.store.select(fromRoot.getToken)
          .pipe(
            map(token => {
              const jwtHelper = new JwtHelperService();
              return !jwtHelper.isTokenExpired(token);

            })
            , take(1)
            );
    //return this.store.select(fromRoot.getIsAuth).pipe(take(1));
  }

}
