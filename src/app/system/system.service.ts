import { Injectable } from '@angular/core';
import { EnvService } from '../env.service';
import { HttpClient } from '@angular/common/http';



@Injectable({ providedIn: 'root' })
export class SystemService {

  private BACKEND_URL: string;

  constructor(private env: EnvService, private http: HttpClient) {
    this.BACKEND_URL = this.env.apiUrl + '/system/';

  }

  getSystemDateObs() {
    return this.http.get<string>(this.BACKEND_URL + 'date', {});
  }

   setSystemDate() {
    const myDate = new Date();
    this.http.put(this.BACKEND_URL + 'date', {date: myDate})
      .subscribe(() => {
      });
   }

  getFilesPathObs() {
    return this.http.get<{'files': string[]}>(this.BACKEND_URL + 'files', {});
  }

}
