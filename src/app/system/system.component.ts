import { Component, OnInit, OnDestroy } from '@angular/core';
import { SystemService } from './system.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-system',
  templateUrl: './system.component.html',
  styleUrls: ['./system.component.scss']
})
export class SystemComponent implements OnInit {

  systemDate: Date;


  constructor(private systemService: SystemService) { }

  ngOnInit() {
    this.systemService.getSystemDateObs().subscribe(d => this.systemDate = JSON.parse(d));
  }


  onSyncDateTime() {
    this.systemService.setSystemDate();
  }

}
