export class EnvService {

  //https://www.jvandemo.com/how-to-use-environment-variables-to-configure-your-angular-application-without-a-rebuild/

  // The values that are defined here are the default values that can
  // be overridden by env.js

  // API url
  public apiUrl = 'http://localhost:8090/api';

  public wsUrl = 'ws://localhost:8090/api';

  constructor() {
      console.log(window.location.origin);
  }

  getImageUrl(imageName: string) {
    return this.apiUrl + '/data/' + imageName;
  }

  getFilesUrl(){
    return this.apiUrl + '/data';
  }



}
