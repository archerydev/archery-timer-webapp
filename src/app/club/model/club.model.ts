export interface Club {
  id: number;
  name: string;
  image_path: string;
}

