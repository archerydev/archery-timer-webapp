import { Component, OnInit } from '@angular/core';
import { ClubEntityService } from './services/club-entity.service';
import { Observable } from 'rxjs';
import { Club } from './model/club.model';
import { tap } from 'rxjs/operators';
import { EnvService } from '../env.service';
import { SelectClubDialogComponent } from './select-club-dialog.component';
import { MdcDialog, MdcDialogConfig } from '@angular-mdc/web';
import { EditClubDialogComponent } from './club-create/edit-club-dialog.component';
import { UploadFileDialogComponent } from '../upload-file/upload-file-dialog.component';
import { FileListDialogComponent } from '../upload-file/file-list-dialog.component';
import * as fromRoot from '../app.reducer';
import { Store } from '@ngrx/store';


@Component({
  selector: 'app-club',
  templateUrl: './clubs.component.html',
  styleUrls: ['./clubs.component.scss']
})
export class ClubsComponent implements OnInit {

  isAuth$: Observable<boolean>;
  loading$: Observable<boolean>;
  clubs$: Observable<Club[]>;

  constructor(private store: Store<fromRoot.State>,
              private envService: EnvService,
              private clubService: ClubEntityService,  public dialog: MdcDialog) {
    this.loading$ = clubService.loading$;
  }

  getImageUrl(imageName) {
    return this.envService.getImageUrl(imageName);
  }

  ngOnInit() {
    this.isAuth$ = this.store.select(fromRoot.getIsAuth);
    this.reload();
  }

  reload() {
    this.clubs$ = this.clubService.entities$;
  }

  editClub(club: Club) {
    const dialogConfig = new MdcDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      dialogTitle:"Create Club",
      club,
      mode: 'update'
    };
    this.dialog.open(EditClubDialogComponent, dialogConfig);
  }
  onClick() {

    // this.dialog.open(EditClubDialogComponent, dialogConfig);
    // let dialogRef = this.dialog.open(SelectClubDialogComponent, {
    //   clickOutsideToClose: true,
    //   escapeToClose: true,
    // });
  }

  onAddClub() {

    const dialogConfig = new MdcDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      dialogTitle:"Create Club",
      mode: 'create'
    };

    this.dialog.open(EditClubDialogComponent, dialogConfig);
  }

}
