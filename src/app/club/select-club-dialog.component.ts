import { Component, Inject } from '@angular/core';
import { MdcDialogRef, MDC_DIALOG_DATA } from '@angular-mdc/web';
import { ClubEntityService } from './services/club-entity.service';
import { Observable } from 'rxjs';
import { Club } from './model/club.model';
import { EnvService } from '../env.service';

@Component({
    templateUrl: 'select-club-dialog.component.html',
  })
export class SelectClubDialogComponent {

  clubs$: Observable<Club[]>;
  constructor(public dialogRef: MdcDialogRef<SelectClubDialogComponent>,
              private clubService: ClubEntityService,
              private envService: EnvService) {
    this.clubs$ = this.clubService.getAll();
  }


  getImageUrl(imageName) {
    return this.envService.getImageUrl(imageName);
  }

  closeDialog(club): void {
    this.dialogRef.close(club);
  }
}
