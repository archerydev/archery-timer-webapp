import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from 'src/app/material.module';
import { SelectClubDialogComponent } from './select-club-dialog.component';
import { ClubsComponent } from './clubs.component';


import { ArcheryRoutingModule } from './archery-routing.module';
import { EntityDefinitionService, EntityMetadataMap } from '@ngrx/data';
import { ClubEntityService } from './services/club-entity.service';
import { ClubsResolver } from './services/clubs.resolver';
import { EditClubDialogComponent } from './club-create/edit-club-dialog.component';
import { SharedModule } from '../shared/shared.module';


const entityMetadata: EntityMetadataMap = {
  Club: {}
};


export const entityConfig = {
  entityMetadata
};

@NgModule({
  imports: [
    MaterialModule,
    FlexLayoutModule,
    CommonModule,
    ReactiveFormsModule,
    ArcheryRoutingModule
  ],
  declarations: [
    ClubsComponent,
    SelectClubDialogComponent,
    EditClubDialogComponent
  ],
  exports: [
    EditClubDialogComponent
  ],
  entryComponents: [SelectClubDialogComponent, EditClubDialogComponent],
  providers: [
    ClubEntityService,
    ClubsResolver
  ]
})
export class ArcheryModule {

  constructor( eds: EntityDefinitionService) {
    eds.registerMetadataMap(entityMetadata);
  }

}
