import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ClubEntityService } from './club-entity.service';
import { map, tap, filter, first } from 'rxjs/operators';

@Injectable()
export class ClubsResolver implements Resolve<boolean> {

    constructor(private clubsService: ClubEntityService) {

    }

    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {

          return this.clubsService.loaded$
              .pipe(
                  tap(loaded => {
                    if (!loaded) {
                      this.clubsService.getAll();
                    }
                  }),
                  filter(loaded => !!loaded),
                  first()
              );

  }

}
