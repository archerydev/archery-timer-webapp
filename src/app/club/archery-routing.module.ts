import { NgModule } from '@angular/core';
import { RouterModule,  Routes } from '@angular/router';

import { ClubsComponent } from './clubs.component';
import { ClubsResolver } from './services/clubs.resolver';

const routes: Routes = [
   { path: 'club',
    component: ClubsComponent,
    resolve: {
      clubs: ClubsResolver
    }}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],

})
export class ArcheryRoutingModule {}
