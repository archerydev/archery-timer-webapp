import { Component, OnInit, Inject } from '@angular/core';
import { Club } from '../model/club.model';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { mimeType } from './mime-type.validator';
import { MdcDialogRef, MDC_DIALOG_DATA, MdcDialog } from '@angular-mdc/web';
import { ClubEntityService } from '../services/club-entity.service';
import { EnvService } from 'src/app/env.service';
import { FilesService } from 'src/app/shared/files.service';
import { first } from 'rxjs/operators';
import { FileListDialogComponent } from 'src/app/upload-file/file-list-dialog.component';

@Component({
  selector: 'club-dialog',
  templateUrl: './edit-club-dialog.component.html',
  styleUrls: ['./edit-club-dialog.component.scss']
})
export class EditClubDialogComponent  {

  form: FormGroup;
  dialogTitle: string;
  club: Club;
  imagePreview: string;

  mode: 'create' | 'update';

  constructor(private fb: FormBuilder,
              private dialogRef: MdcDialogRef<EditClubDialogComponent>,
              @Inject(MDC_DIALOG_DATA) data,
              private clubService: ClubEntityService,
              private envService: EnvService,
              public dialog: MdcDialog) {


    this.dialogTitle = data.dialogTitle;
    this.club = data.club;
    this.mode = data.mode;

    const formControls = {
        name: ['', [Validators.required, Validators.minLength(3)]],
        image: ['', [Validators.required], [mimeType] ]
      };

    this.form = this.fb.group(formControls);

    if (this.mode === 'update') {
      console.log(data.club);

      this.form.patchValue({
        ...data.club,
        image: data.club.image_path
      });

      this.imagePreview =  this.envService.getImageUrl(data.club.image_path);

    } else if (this.mode === 'create') {
    }

  }

  onClose() {
    this.dialogRef.close();
  }

  selectImage() {
    const dialogRef = this.dialog.open(FileListDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result != null && result !== '') {
        const filePath = (result as {file_path: string}).file_path;
        this.form.patchValue({image: filePath});
        this.imagePreview = this.envService.getImageUrl(filePath);
      }
    });
  }

  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.form.patchValue({image: file});
    this.form.get('image').updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string;
    };
    reader.readAsDataURL(file);
  }

  onSave() {
    const club: Club = {
      ...this.club,
      ...this.form.value
    };

    if (this.mode === 'update') {
      this.clubService.update(club);
      this.dialogRef.close();
    }  else if (this.mode === 'create') {

      this.clubService.add(club)
        .subscribe(
          newClub => {
            this.dialogRef.close();
          }
        );

    }
  }
  onDelete() {
    //TODO: Show confirmation
    this.clubService.delete(this.club);
    this.dialogRef.close();
  }

}
