import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentTimerComponent } from './current-timer.component';

describe('CurrentTimerComponent', () => {
  let component: CurrentTimerComponent;
  let fixture: ComponentFixture<CurrentTimerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentTimerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentTimerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
