import { Component, OnInit } from '@angular/core';
import { TimerService } from '../timer.service';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import * as fromRoot from '../../app.reducer';
import * as AuthActions from '../../auth/store/auth.actions';
import { Timer, TimerOrder } from '../timer-api.model';

@Component({
  selector: 'app-current-timer',
  templateUrl: './current-timer.component.html',
  styleUrls: ['./current-timer.component.scss']
})
export class CurrentTimerComponent implements OnInit {

  isAuth$: Observable<boolean>;
  currentTimer$: Observable<Timer>;
  currentOrder$: Observable<TimerOrder>;
  isLoading$: Observable<boolean>;


  constructor(
    private store: Store<fromRoot.State>,
    private timerService: TimerService
  ) {}

  ngOnInit() {
    this.timerService.refreshCurrentTimer();
    this.isLoading$ = this.store.select(fromRoot.getIsLoading);
    this.isAuth$ = this.store.select(fromRoot.getIsAuth);
    this.currentTimer$ = this.store.select(fromRoot.getCurrentTimer);
    this.currentOrder$ = this.store.select(fromRoot.getTimerOrder);
  }
  onLogout() {
    this.store.dispatch(new AuthActions.Logout());

  }
  onNew() {

  }
  getColor(co: TimerOrder) {
    return co.number_color;
  }
}
