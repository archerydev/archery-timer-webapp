import { Component, OnInit,  ViewChild, ViewContainerRef } from '@angular/core';
import { Location } from '@angular/common';
import { Store } from '@ngrx/store';

import * as fromRoot from '../../app.reducer';
import { Observable } from 'rxjs';
import { NgForm } from '@angular/forms';

import { take } from 'rxjs/operators';
import { PanelColor } from './color.model';
import { ColorService } from './color.service';

@Component({
  selector: 'app-panel-colors',
  templateUrl: './panel-colors.component.html',
  styleUrls: ['./panel-colors.component.scss']
})
export class PanelColorsComponent implements OnInit {
  @ViewChild('f', { static: true }) colorsForm: NgForm;

  colors2: PanelColor[];

  isLoading$: Observable<boolean>;
  constructor(public vcRef: ViewContainerRef,
              private store: Store<fromRoot.State>,
              private location: Location,
              private colorService: ColorService) { }

  ngOnInit() {
    this.isLoading$ = this.store.select(fromRoot.getIsLoading);
    this.colorService.fetchColorsObs().
      subscribe(colors => this.colors2 = colors);
  }

  onUpdateColors(f: NgForm) {
    this.colorService.storeColorsObs(this.colors2)
        .pipe(take(1))
        .subscribe(colors => this.colors2 = colors);
    this.location.back();
  }


  onDefaultValues() {
    this.colorService.resetColorsObs()
        .pipe(take(1))
        .subscribe(colors => this.colors2 = colors);
  }

}
