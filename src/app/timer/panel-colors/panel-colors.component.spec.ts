import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelColorsComponent } from './panel-colors.component';

describe('PanelColorsComponent', () => {
  let component: PanelColorsComponent;
  let fixture: ComponentFixture<PanelColorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelColorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelColorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
