import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PanelColor } from './color.model';
import { EnvService } from 'src/app/env.service';

@Injectable({ providedIn: 'root' })
export class ColorService {
  private BACKEND_URL: string;
  private panelColorsURL: string;

  constructor(private env: EnvService, private http: HttpClient) {
    this.BACKEND_URL = this.env.apiUrl + '/color/';
    this.panelColorsURL = this.BACKEND_URL + 'list';
  }

  fetchColorsObs() {
    return this.http.get<PanelColor[]>(this.panelColorsURL, {
      observe: 'body',
      responseType: 'json'
    });
  }

  storeColorsObs(colors: PanelColor[]) {
    return this.http.put<PanelColor[]>(this.panelColorsURL, colors);
  }

  resetColorsObs() {
    return this.http.put<PanelColor[]>(this.panelColorsURL, null);
  }

}
