export interface PanelColor {
  name: string;
  color: string;
}
