import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import * as fromRoot from '../../../app.reducer';
import { Timer, TimerOrder } from '../../timer-api.model';


@Component({
  selector: 'app-timer-view',
  templateUrl: './timer-view.component.html',
  styleUrls: ['./timer-view.component.scss']
})
export class TimerViewComponent implements OnInit {

  isAuth$: Observable<boolean>;
  currentTimer$: Observable<Timer>;
  currentOrder$: Observable<TimerOrder>;

  constructor(private store: Store<fromRoot.State>) { }

  ngOnInit() {
    this.isAuth$ = this.store.select(fromRoot.getIsAuth);
    this.currentTimer$ = this.store.select(fromRoot.getCurrentTimer);
    this.currentOrder$ = this.store.select(fromRoot.getTimerOrder);
  }

}
