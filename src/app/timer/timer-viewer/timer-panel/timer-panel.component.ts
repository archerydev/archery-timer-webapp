import { Component, OnInit, Input, OnDestroy, NgZone } from '@angular/core';

import { CanvasService } from '../../canvas.service';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-timer-panel',
  templateUrl: './timer-panel.component.html',
  styleUrls: ['./timer-panel.component.scss']
})
export class TimerPanelComponent implements OnInit, OnDestroy {

  numCols: number;
  numRows: number;
  defaultColor = new Uint8ClampedArray([128, 128, 128]);
  subscription: Subscription;
  @Input() pixels: Uint8ClampedArray;

  constructor(private canvasService: CanvasService, private zone: NgZone) {}

  ngOnInit() {
    this.numCols = this.canvasService.numCols;
    this.numRows = this.canvasService.numRows;
    this.pixels = this.initPanelPixels();

  }
  public ngAfterViewInit() {
    this.subscription = this.canvasService
      .getWsObservable()
      .pipe(map(pixels => {
        this.zone.runOutsideAngular(() =>{
          this.setImageData(pixels)})}))
      .subscribe();
  }

  setImageData(imageData: Uint8ClampedArray) {
    for (let i = 0; i < this.numCols * this.numRows; i++) {
      this.pixels.set(imageData.slice(i * 3, i * 3 + 3 ), i * 3);
    }
  }

  initPanelPixels(): Uint8ClampedArray  {
    const pixelsN: number[] = [];
    for (let i = 0; i < this.numCols * this.numRows; i++) {
      pixelsN.push(...Array.from(this.defaultColor));
    }
    return new Uint8ClampedArray(pixelsN);
  }

  getColor(index: number) {
    return ('rgb(' + this.pixels[ 3 * index] + ',' + this.pixels[3 * index + 1] + ',' + this.pixels[3 * index + 2] + ')'
    );
  }

  getRowPosition(index: number): number {
    return Math.trunc(index / this.numCols);
  }

  getColPosition(index: number) {
    return index % this.numCols;
  }


  setPixelColor(pixels: Uint8ClampedArray, index: number, color: Uint8ClampedArray) {
    pixels.set(color, index * 3);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
