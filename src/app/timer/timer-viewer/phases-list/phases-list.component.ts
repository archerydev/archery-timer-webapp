import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, distinctUntilChanged } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../../app.reducer';
import { MdcChipSelectionEvent } from '@angular-mdc/web';

@Component({
  selector: 'app-phases-list',
  templateUrl: './phases-list.component.html',
  styleUrls: ['./phases-list.component.scss']
})
export class PhasesListComponent implements OnInit {

  phasesList$: Observable<string[]>;
  selectedItem$: Observable<number>;

  constructor(private store: Store<fromRoot.State>) { }

  ngOnInit() {
    this.phasesList$ = this.store.select(fromRoot.getTimerOrder)
            .pipe(map(order => {
              if (order === null) {
                return [];
              }
              return order.phases_names;
            }), distinctUntilChanged());
    this.selectedItem$ =  this.store.select(fromRoot.getTimerOrder).pipe(map(order => order && order.phase), distinctUntilChanged());
  }

  onChipInteraction(evt: MdcChipSelectionEvent): void {
    evt.detail.selected = false;
  }
}
