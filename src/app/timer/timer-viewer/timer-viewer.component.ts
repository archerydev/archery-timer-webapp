import {Component, OnInit, Input  } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { tap, filter, map } from 'rxjs/operators';
import {Howl, Howler} from 'howler';

import * as fromRoot from '../../app.reducer';
import { TimerService } from '../timer.service';
import { PanelSounds, Timer, TimerOrder } from '../timer-api.model';


@Component({
  selector: 'app-timer-viewer',
  templateUrl: './timer-viewer.component.html',
  styleUrls: ['./timer-viewer.component.scss']
})
export class TimerViewerComponent implements OnInit {

  isAuth$: Observable<boolean>;
  currentTimer$: Observable<Timer>;
  sound = new Howl({
    src: ['./assets/Chicharra02.mp3'],
    rate: 2.5,
    preload: true,
  });
  soundSettings: PanelSounds;
  @Input() pixels: Uint8ClampedArray;

  constructor( private store: Store<fromRoot.State>,  private timerService: TimerService) {}

  ngOnInit() {

    Howler._autoResume();
    this.store.select(fromRoot.getPanelSounds).subscribe(ps => {
      this.sound = this.getSoundFromSettings(ps);
      this.soundSettings = ps;
    }
    );
    this.timerService.refreshCurrentTimer();
    this.isAuth$ = this.store.select(fromRoot.getIsAuth);
    this.currentTimer$ = this.store.select(fromRoot.getCurrentTimer);
  
    this.store
      .select(fromRoot.getTimerOrder)
      .pipe(tap((order: TimerOrder) => {
            if (order && order.sound) {
              this.playSound(order.sound);
            }
          }
      )).subscribe();
  }

  getSoundFromSettings(panelSounds: PanelSounds): Howl {
    return new Howl({
      src: ['./assets/' + panelSounds.sound_name + '.mp3'],
      rate: panelSounds.speed,
      preload: true,
    });
  }

  playSound(remainNumber: number) {
    if (this.sound === undefined) {
      //TODO: Show toast with the reason
      return;
    }
    if (remainNumber > 0) {
      this.sound.play();
      setTimeout(() => this.playSound(remainNumber - 1), this.soundSettings.delay);
    }
  }

}

