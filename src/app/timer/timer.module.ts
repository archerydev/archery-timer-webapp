import { NgModule } from '@angular/core';
import { TimerControlComponent } from './timer-control/timer-control.component';
import { TimerComponent } from './timer.component';
import { TimerRoutingModule } from './timer-routing.module';
import { SharedModule } from '../shared/shared.module';
import { CurrentTimerComponent } from './current-timer/current-timer.component';
import { TimerViewerComponent } from './timer-viewer/timer-viewer.component';
import { TimerCanvasComponent } from './timer-canvas/timer-canvas.component';
import { TimerListComponent } from './generate-timer/timer-list/timer-list.component';
import { NewTimerComponent } from './generate-timer/new-timer/new-timer.component';
import { LoadTimerComponent } from './generate-timer/load-timer/load-timer.component';
import { GenerateTimerComponent } from './generate-timer/generate-timer.component';
import { PhasesListComponent } from './timer-viewer/phases-list/phases-list.component';
import { TimerPanelComponent } from './timer-viewer/timer-panel/timer-panel.component';
import { TimerViewComponent } from './timer-viewer/timer-view/timer-view.component';
import { PanelColorsComponent } from './panel-colors/panel-colors.component';
import { PanelSoundComponent } from './panel-sound/panel-sound.component';
import { LogViewerComponent } from './log-viewer/log-viewer.component';
import { AddLogDialogComponent } from './log-viewer/addlog-dialog.component';
import { AddEndDialogComponent } from './timer-control/addend-dialog.component';
import { ClassificationComponent } from './generate-timer/classification/classification.component';
import { DivisionComponent } from './generate-timer/classification/division/division.component';



@NgModule({
  declarations: [
    CurrentTimerComponent,
    TimerComponent,
    TimerControlComponent,
    NewTimerComponent,
    CurrentTimerComponent,
    TimerViewerComponent,
    TimerCanvasComponent,
    TimerListComponent,
    PhasesListComponent,
    GenerateTimerComponent,
    LoadTimerComponent,
    TimerPanelComponent,
    TimerViewComponent,
    PanelColorsComponent,
    PanelSoundComponent,
    LogViewerComponent,
    AddLogDialogComponent,
    AddEndDialogComponent,
    ClassificationComponent,
    DivisionComponent
  ],
  imports: [
    SharedModule,
    TimerRoutingModule
  ],
   entryComponents: [AddLogDialogComponent, AddEndDialogComponent]
})
export class TimerModule {}
