import { Action } from '@ngrx/store';
import { TimerControlAction } from '../timer.model';
import { PanelColors, PanelSounds, Timer, TimerOrder, TimerState, TimerTemplate, ClassificationOrder } from '../timer-api.model';

export const CREATE_TIMER = '[Timer] Create Timer';
export const CURRENT_TIMER = '[Timer] Current Timer'; //from backend
export const CLEAR_TIMER = '[Timer] Clear Current Timer';
export const EXEC_TIMER_ACTION = '[Timer] Execute Timer control Actionr';
export const CHANGE_TIMERORDER = '[Timer] Change timer order'; //from backend
export const LOAD_TIMERORDER = '[Timer] Load timer order';
export const LOAD_TIMER = '[Timer] Load timer and time order on the backend';
export const CHANGE_COLORS = '[Timer] Change panel default colors';
export const CHANGE_SOUNDS = '[Timer] Change panel sound config';
export const SET_TIMERS = '[Timer] Set saved timers';
export const DELETE_TIMER = '[Timer] Delete timer';
export const FETCH_TIMERS = '[Timer] Fetch  saved timers';
export const SELECT_TEMPLATE = '[Timer] Select template';
export const LOAD_CLASSIFICATION = '[Timer] Load Classification';


export class ExecTimerAction implements Action {
  readonly type = EXEC_TIMER_ACTION;
  constructor(public payload: TimerControlAction) {}
}
export class LoadTimer implements Action {
  readonly type = LOAD_TIMER;
  constructor(public payload: TimerState) {}
}

export class CurrentTimer implements Action {
  readonly type = CURRENT_TIMER;
  constructor(public payload: Timer) {}
}

export class ClearTimer implements Action {
  readonly type = CLEAR_TIMER;
}
export class ChangeTimerOrder implements Action {
  readonly type = CHANGE_TIMERORDER;
  constructor(public payload: TimerOrder) {}
}

export class LoadTimerOrder implements Action {
  readonly type = LOAD_TIMERORDER;
  constructor(public payload: TimerOrder) {}
}

export class LoadClassification implements Action {
  readonly type = LOAD_CLASSIFICATION;
  constructor(public payload: ClassificationOrder[]) {}
}

export class CreateTimer implements Action {
  readonly type = CREATE_TIMER;
  constructor(public payload: TimerTemplate) {}
}

export class ChangeColors implements Action {
  readonly type = CHANGE_COLORS;
  constructor(public payload: PanelColors) {}
}

export class UpdateSoundConfig implements Action {
  readonly type = CHANGE_SOUNDS;
  constructor(public payload: PanelSounds) {}
}

export class DeleteTimer implements Action {
  readonly type = DELETE_TIMER;
  constructor(public payload: TimerState) {}
}

export class SetTimers implements Action {
  readonly type = SET_TIMERS;
  constructor(public payload: TimerState[]) {}
}

export class FetchTimers implements Action {
  readonly type = FETCH_TIMERS;
}


export class SelectTemplate implements Action {
  readonly type = SELECT_TEMPLATE;
  constructor(public payload: TimerTemplate) {}
}

export type TimerActions =
  | CreateTimer
  | CurrentTimer
  | ClearTimer
  | ExecTimerAction
  | ChangeTimerOrder
  | LoadTimerOrder
  | LoadTimer
  | DeleteTimer
  | SetTimers
  | FetchTimers
  | SelectTemplate
  | ChangeColors
  | UpdateSoundConfig
  | LoadClassification;


