import {
  TimerActions,
  CURRENT_TIMER ,
  CHANGE_TIMERORDER,
  SET_TIMERS,
  CHANGE_COLORS,
  DELETE_TIMER,
  SELECT_TEMPLATE,
  CHANGE_SOUNDS} from './timer.actions';
import { PanelColors, PanelSounds, Timer, TimerOrder, TimerState, TimerTemplate } from '../timer-api.model';

export interface State {
  currentTimer: Timer;
  timerOrder: TimerOrder;
  savedTimers: TimerState[];
  selectedTemplate: TimerTemplate;
  panelColors: PanelColors;
  panelSounds: PanelSounds;
}

export const initialState: State = {
  currentTimer: null,
  timerOrder: null,
  savedTimers: [],
  selectedTemplate: null,
  panelColors: {
    backcolor: '#000000',
    green: '#07a30c',
    yellow: '#ffed00',
    red: '#ff0000'
  },
  panelSounds: {
    sound_name: 'Chicharra02',
    speed: 1,
    delay: 500,
  }

};

export function timerReducer(state = initialState, action: TimerActions ) {
    switch (action.type) {
      case CURRENT_TIMER:
        return {
        ...state,
        currentTimer: action.payload
        };
      case CHANGE_TIMERORDER:
        return {
        ...state,
        timerOrder: action.payload
        };
      case SET_TIMERS:
        return {
          ...state,
          savedTimers: [...action.payload]
        };
      case DELETE_TIMER:
        const oldTimers = [...state.savedTimers];
        const index = oldTimers.indexOf(action.payload, 0);
        oldTimers.splice(index, 1);
        return {
          ...state,
          savedTimers: oldTimers
        };
      case CHANGE_COLORS:
        return {
        ...state,
        panelColors: action.payload
        };
      case CHANGE_SOUNDS:
          return {
          ...state,
          panelSounds: action.payload
          };
      case SELECT_TEMPLATE:
        return {
        ...state,
        selectedTemplate: action.payload
        };
      default: {
        return state;
      }
    }
}
export const getCurrentTimer = (state: State) => state.currentTimer;
export const getTimerOrder = (state: State) => state.timerOrder;
export const getSavedTimers = (state: State) => state.savedTimers;
export const getRunningStatus = (state: State) => state.timerOrder &&  state.timerOrder.status;
export const getPanelColors = (state: State) => state.panelColors;
export const getPanelSounds = (state: State) => state.panelSounds;
export const getSelectedTemplate = (state: State) => state.selectedTemplate;


