import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, tap, switchMap, mergeMap, catchError, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';
import { Action, Store } from '@ngrx/store';

import * as TimerActions from './timer.actions';
import * as UIActions from '../../shared/ui.actions';
import * as fromRoot from '../../app.reducer';
import { TimerControlAction } from '../timer.model';
import { TimerService } from '../timer.service';
import { UIService } from 'src/app/shared/ui.service';
import { Router } from '@angular/router';
import { TimerOrder, TimerState, TimerTemplate, ClassificationOrder } from '../timer-api.model';


export class TimerError implements Action {
  readonly type = '[Error] Timer Error';
}

export class TimerAction implements Action {
  readonly type = '[Timer] Timer Action Sucess';
}
@Injectable()
export class TimerEffects {
  constructor(private actions$: Actions,
              private timerService: TimerService,
              private uiService: UIService,
              private router: Router,
              private store: Store<fromRoot.State>) { }

  @Effect()
  timerExecAction = this.actions$
    .pipe(
      ofType(TimerActions.EXEC_TIMER_ACTION)
      , map((action: TimerActions.ExecTimerAction) => {
        return action.payload;
      })
      , switchMap((controlAction: TimerControlAction) => {
        this.store.dispatch(new UIActions.StartLoading());
        return this.timerService.executeTimerControlAction(controlAction)
          .pipe(
            mergeMap(response => {
              this.store.dispatch(new UIActions.StopLoading());
              return of(new TimerAction());
            })
            , catchError(error => {
              this.store.dispatch(new UIActions.StopLoading());
              if (error.error.error) {
                this.uiService.showSnackbar(error.error.error, null);
              } else if (error.error) {
                this.uiService.showSnackbar(error.error, null);
              }
              this.uiService.showSnackbar(error.error.msg, null);
              return of(new TimerError());
            })
          );
      }
      )
    );

  @Effect()
  createTimerAction = this.actions$
    .pipe(
      ofType(TimerActions.CREATE_TIMER)
      , map((action: TimerActions.CreateTimer) => {
        return action.payload;
      })
      , switchMap((timer: TimerTemplate) => {
        this.store.dispatch(new UIActions.StartLoading());
        return this.timerService.createTimerObs(timer)
          .pipe(
            mergeMap((ti: any) => {
              this.store.dispatch(new UIActions.StopLoading());
              this.router.navigate(['/', 'timer', 'timer']);
              return [
                {
                  type: TimerActions.CURRENT_TIMER,
                  payload: ti
                }
              ];
            })
            , catchError(error => {
              if (error.error.error) {
                this.uiService.showSnackbar(error.error.error, null);
              } else if (error.error) {
                this.uiService.showSnackbar(error.error, null);
              }
              this.store.dispatch(new UIActions.StopLoading());
              return of(new TimerError());
            })
          );
      }
      )
    );

  @Effect()
  clearTimerAction = this.actions$
    .pipe(
      ofType(TimerActions.CLEAR_TIMER)
      , switchMap((action: TimerActions.ClearTimer) => {
        return this.timerService.clearTimerObs()
          .pipe(
            mergeMap((ti: any) => {
              this.router.navigate(['/', 'timer', 'timer']);
              return [
                {
                  type: TimerActions.CURRENT_TIMER,
                  payload: null
                }
              ];
            })
            , catchError(error => {
              console.log(error);
              if (error.error.error) {
                this.uiService.showSnackbar(error.error.error, null);
              } else if (error.error) {
                this.uiService.showSnackbar(error.error, null);
              }
              return of(new TimerError());
            })
          );
      }
      )
    );

  @Effect()
  loadTimerOrderAction = this.actions$
    .pipe(
      ofType(TimerActions.LOAD_TIMERORDER)
      , map((action: TimerActions.LoadTimerOrder) => {
        return action.payload;
      })
      , switchMap((timerOrder: TimerOrder) => {
        this.store.dispatch(new UIActions.StartLoading());
        return this.timerService.loadTimerOrderObs(timerOrder)
          .pipe(
            mergeMap((ti: any) => {
              this.store.dispatch(new UIActions.StopLoading());
              this.router.navigate(['/', 'timer', 'timer']);
              return of(new TimerAction());
            })
            , catchError(error => {
              if (error.error.error) {
                this.uiService.showSnackbar(error.error.error, null);
              } else if (error.error) {
                this.uiService.showSnackbar(error.error, null);
              }
              this.store.dispatch(new UIActions.StopLoading());
              return of(new TimerError());
            })
          );
      }
      )
    );
    @Effect()
    loadClassificationAction = this.actions$
      .pipe(
        ofType(TimerActions.LOAD_CLASSIFICATION)
        , map((action: TimerActions.LoadClassification) => {
          return action.payload;
        })
        , switchMap((classifications: ClassificationOrder[]) => {
          this.store.dispatch(new UIActions.StartLoading());
          return this.timerService.loadClassificationObs(classifications)
            .pipe(
              mergeMap((ti: any) => {
                this.store.dispatch(new UIActions.StopLoading());
                this.router.navigate(['/', 'timer', 'timer']);
                this.timerService.refreshCurrentTimer();
                return of(new TimerAction());
              })
              , catchError(error => {
                if (error.error.error) {
                  this.uiService.showSnackbar(error.error.error, null);
                } else if (error.error) {
                  this.uiService.showSnackbar(error.error, null);
                }
                this.store.dispatch(new UIActions.StopLoading());
                return of(new TimerError());
              })
            );
        }
        )
      );

  @Effect()
  loadTimerAction = this.actions$
    .pipe(
      ofType(TimerActions.LOAD_TIMER)
      , map((action: TimerActions.LoadTimer) => {
        return action.payload;
      })
      , switchMap((timerState: TimerState) => {
        this.store.dispatch(new UIActions.StartLoading());
        return this.timerService.loadTimerObs(timerState)
          .pipe(
            mergeMap((ti: TimerState) => {
              this.store.dispatch(new UIActions.StopLoading());
              this.router.navigate(['/', 'timer', 'timer']);
              return [
                {
                  type: TimerActions.CURRENT_TIMER,
                  payload: ti.timer
                }
              ];
            })
            , catchError(error => {
              if (error.error.error) {
                this.uiService.showSnackbar(error.error.error, null);
              } else if (error.error) {
                this.uiService.showSnackbar(error.error, null);
              }
              this.store.dispatch(new UIActions.StopLoading());
              return of(new TimerError());
            })
          );
      }
      )
    );

  @Effect()
  timersFetchAction = this.actions$
    .pipe(
      ofType(TimerActions.FETCH_TIMERS)
      , switchMap((action: TimerActions.FetchTimers) => {
        return this.timerService.fetchTimersObs();
      }), map(
        (timerStates) => {
          return {
            type: TimerActions.SET_TIMERS,
            payload: timerStates
          };
        }
      ));

  @Effect({ dispatch: false })
  deleteTimerAction = this.actions$
    .pipe(
      ofType(TimerActions.DELETE_TIMER)
      , map((action: TimerActions.DeleteTimer) => {
        return action.payload;
      })
      , switchMap((timerState: TimerState) => {
        this.store.dispatch(new UIActions.StartLoading());
        return this.timerService.deleteTimerObs(timerState)
          .pipe(
            map((ti: TimerState) => {
              this.store.dispatch(new UIActions.StopLoading());
            })
            , catchError(error => {
              if (error.error.error) {
                this.uiService.showSnackbar(error.error.error, null);
              } else if (error.error) {
                this.uiService.showSnackbar(error.error, null);
              }
              this.store.dispatch(new UIActions.StopLoading());
              return of(new TimerError());
            })
          );
      }
      )
    );

}
