import { NgModule } from '@angular/core';
import { RouterModule,  Routes } from '@angular/router';
import { TimerComponent } from './timer.component';
import { AuthGuard } from '../auth/auth.guard';
import { GenerateTimerComponent } from './generate-timer/generate-timer.component';

import { TimerPanelComponent } from './timer-viewer/timer-panel/timer-panel.component';
import { PanelColorsComponent } from './panel-colors/panel-colors.component';
import { PanelSoundComponent } from './panel-sound/panel-sound.component';
import { LogViewerComponent } from './log-viewer/log-viewer.component';
import { TimerViewerComponent } from './timer-viewer/timer-viewer.component';
import { CurrentTimerComponent } from './current-timer/current-timer.component';

const routes: Routes = [
   { path: '', component: TimerComponent, children: [
     { path: 'timer', component: CurrentTimerComponent},
     { path: 'panel', component: TimerPanelComponent},
     { path: 'generate', component: GenerateTimerComponent, canActivate: [AuthGuard] },
     { path: 'colors', component: PanelColorsComponent, canActivate: [AuthGuard]},
     { path: 'sound', component: PanelSoundComponent, canActivate: [AuthGuard]},
     { path: 'log', component: LogViewerComponent, canActivate: [AuthGuard]}
   ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  // providers: [AuthGuard]

})
export class TimerRoutingModule {}
