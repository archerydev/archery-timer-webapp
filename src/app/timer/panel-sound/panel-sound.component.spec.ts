import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelSoundComponent } from './panel-sound.component';

describe('PanelSoundComponent', () => {
  let component: PanelSoundComponent;
  let fixture: ComponentFixture<PanelSoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelSoundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelSoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
