import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Location } from '@angular/common';
import {Howl, Howler} from 'howler';
import * as fromRoot from '../../app.reducer';
import { UpdateSoundConfig } from '../store/timer.actions';
import { PanelSounds } from '../timer-api.model';
@Component({
  selector: 'app-panel-sound',
  templateUrl: './panel-sound.component.html',
  styleUrls: ['./panel-sound.component.scss']
})
export class PanelSoundComponent implements OnInit {

  sounds = [
      'Chicharra02',
      'Zumb01',
      'Zumb02',
      'Zumb03'
    ];

  speed = 1;
  delay = 500;
  numSounds = 2;


  soundForm: FormGroup;
  isLoading$: Observable<boolean>;
  soundSettings: PanelSounds;

  constructor( private store: Store<fromRoot.State>,
               private formBuilder: FormBuilder,
               private location: Location) { }

  ngOnInit() {
    Howler._autoResume();
    this.store
        .select(fromRoot.getPanelSounds)
        .subscribe(ps => this.soundSettings = ps);
    this.soundForm = this.formBuilder.group({
      sound_name: [this.soundSettings.sound_name, Validators.required],
      speed: [this.soundSettings.speed, Validators.required],
      delay: [this.soundSettings.delay, Validators.required]
    });
  }

  onSoundEdited() {
    this.store.dispatch(new UpdateSoundConfig(this.soundForm.value));
    this.location.back();
  }

  onTest() {
    console.log(this.soundForm);
    const sound = new Howl({
      src: ['./assets/' + this.soundForm.value.sound_name+'.mp3'],
      rate: this.soundForm.value.speed,
      preload: true,
    });
    this.playSound(sound, 3);


  }
  playSound(sound: Howl, remainNumber: number) {
    if (remainNumber > 0) {
      sound.play();
      setTimeout(() => this.playSound(sound, remainNumber - 1), this.soundForm.value.delay);
    }
  }
}
