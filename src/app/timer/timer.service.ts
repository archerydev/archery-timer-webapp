import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { webSocket, WebSocketSubject} from 'rxjs/webSocket';
import { distinctUntilChanged } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { TimerControlAction } from './timer.model';
import * as fromRoot from '../app.reducer';
import * as TimerActions from './store/timer.actions';
import * as UIActions from '../shared/ui.actions';
import { EnvService } from '../env.service';
import { Timer, TimerOrder, TimerState, TimerTemplate, ClassificationOrder } from './timer-api.model';

@Injectable({ providedIn: 'root' })
export class TimerService {

  private BACKEND_URL: string;
  private WEBSOCKET_URL: string;

  private ws2: WebSocketSubject<{timer: TimerOrder}>;

  constructor(private env: EnvService, private http: HttpClient, private store: Store<fromRoot.State>) {
    this.BACKEND_URL = this.env.apiUrl + '/timer/';
    this.WEBSOCKET_URL = this.env.wsUrl + '/timer/';

    this.ws2 = webSocket(this.WEBSOCKET_URL + 'ws');

    this.ws2.pipe(distinctUntilChanged())
      .subscribe(
        msg => this.store.dispatch(new TimerActions.ChangeTimerOrder(msg.timer)), // Called whenever there is a message from the server.
        err => console.log(err), // Called if at any point WebSocket API signals some kind of error.
        () => console.log('complete') // Called when connection is closed (for whatever reason).
    );
  }

  createTimerObs(timer: TimerTemplate): Observable<{timer: Timer}> {
    return this.http
    .post<{timer: Timer}>(this.BACKEND_URL + 'create', timer);
  }

  loadClassificationObs(classification: ClassificationOrder[]): Observable<{classification: ClassificationOrder[]}> {
    return this.http
    .post<{classification: ClassificationOrder[]}>(this.BACKEND_URL + 'load_classification', classification)
  }

  refreshCurrentTimer() {
    this.store.dispatch(new UIActions.StartLoading());
    this.http.get<{timer: Timer, timerorder: TimerOrder}>(this.BACKEND_URL + 'current')
      .subscribe(res => {
        {
          this.store.dispatch(new UIActions.StopLoading());
          this.store.dispatch(new TimerActions.CurrentTimer(res.timer));
          this.store.dispatch(new TimerActions.ChangeTimerOrder(res.timerorder));
        }
      }, () => this.store.dispatch(new UIActions.StopLoading()));
  }

  executeTimerControlAction(status: TimerControlAction): Observable<{message: string, status: string}> {
      return this.http.post<{message: string, status: string}>(this.BACKEND_URL + this.getCommandByControlTimerAction(status), {});
  }

  getCommandByControlTimerAction(status: TimerControlAction) {
    switch (status) {
      case TimerControlAction.Start:
        return 'start';
      case TimerControlAction.Stop:
        return 'stop';
      case TimerControlAction.Pause:
        return 'pause';
      case TimerControlAction.EndPhase:
        return 'end_phase';
    }
    return 'start';
  }

  clearTimerObs() {
    return this.http.post<{token: string}>(this.BACKEND_URL + 'clear', {});
  }

  loadTimerOrderObs(timerOrder: TimerOrder): Observable<{timerOrder: TimerOrder}> {
    return this.http
    .post<{timerOrder: TimerOrder}>(this.BACKEND_URL + 'move_position', timerOrder);
  }

  loadTimerObs(timerState: TimerState): Observable<TimerState> {
    return this.http
    .post<TimerState>(this.BACKEND_URL + 'load_timer', timerState);
  }

  deleteTimerObs(timerState: TimerState): Observable<TimerState> {
    return this.http
    .post<TimerState>(this.BACKEND_URL + 'delete', timerState);
  }

  fetchTimersObs() {
    return this.http.get<TimerState[]>(this.BACKEND_URL + 'timers', {
      observe: 'body',
      responseType: 'json'
    });
  }
  storeTimersObs(timers:TimerState[]) {
    console.log("storeTimers")
    const req = new HttpRequest('PUT', this.BACKEND_URL + 'timers', timers, {reportProgress: true});
    return this.http.request(req);
  }



  addEndToTimer(endInf:{ minNumber: number, useDoubleTurn: boolean, finalEnd: boolean }) {
    const numLetters = endInf.useDoubleTurn ? 4 : 2;
    if (!endInf.finalEnd) {
      return this.http.post(this.BACKEND_URL + 'add_playoff_end', {min_number: endInf.minNumber, letters: numLetters}).subscribe();
    }
    return this.http.post(this.BACKEND_URL + 'add_playoff_end', {min_number: null, letters: numLetters }).subscribe();

  }


}
