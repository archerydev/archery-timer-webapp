import { Component, OnInit} from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as TimerActions from '../../store/timer.actions';
import * as fromRoot from '../../../app.reducer';
import { TimerTemplate } from '../../timer-api.model';


@Component({
  selector: 'app-new-timer',
  templateUrl: './new-timer.component.html',
  styleUrls: ['./new-timer.component.scss']
})
export class NewTimerComponent implements OnInit {
  timer_types = [
    'tournament',
    'warming',
    'arrow',
    'playoff',
    'timer',
    'classification'
  ]

  timerTemplate$: Observable<TimerTemplate>;
  isLoading$: Observable<boolean>;

  timerForm: FormGroup;


  constructor(private store: Store<fromRoot.State>, private formBuilder: FormBuilder ) {
  }

  ngOnInit() {
    this.timerForm = this.formBuilder.group({
      timer_type: ['', Validators.required],
      name: ['', Validators.required] ,
      rounds:['', Validators.required],
      ends: ['', Validators.required],
      letters: ['', Validators.required],
      duration: ['', Validators.required],
      min_number: ['', Validators.required]
    });
    this.isLoading$ = this.store.select(fromRoot.getIsLoading);
    this.store.select(fromRoot.getSelectedTemplate)
          .subscribe(tmpl => {
              this.timerForm.reset();
              if (tmpl) {
                this.timerForm.patchValue(tmpl);
              }
            }
            );
    this.timerForm.get('timer_type').valueChanges.subscribe(val => {
              if (val === 'tournament') {
                  this.timerForm.controls['rounds'].setValidators([Validators.required]);
                  this.timerForm.controls['ends'].setValidators([Validators.required]);
                  this.timerForm.controls['letters'].setValidators([Validators.required]);
                  this.timerForm.controls['rounds'].updateValueAndValidity();
                  this.timerForm.controls['ends'].updateValueAndValidity();
                  this.timerForm.controls['letters'].updateValueAndValidity();
                  this.timerForm.controls['min_number'].clearValidators();
                  this.timerForm.controls['min_number'].updateValueAndValidity();
              } else if (val === 'warming') {
                  this.timerForm.controls['rounds'].clearValidators();
                  this.timerForm.controls['rounds'].updateValueAndValidity();
                  this.timerForm.controls['ends'].setValidators([Validators.required]);
                  this.timerForm.controls['ends'].updateValueAndValidity();
                  this.timerForm.controls['letters'].setValidators([Validators.required]);
                  this.timerForm.controls['letters'].updateValueAndValidity();
                  this.timerForm.controls['min_number'].clearValidators();
                  this.timerForm.controls['min_number'].updateValueAndValidity();
              } else if (val === 'arrow') {
                  this.timerForm.controls['rounds'].clearValidators();
                  this.timerForm.controls['rounds'].updateValueAndValidity();
                  this.timerForm.controls['ends'].clearValidators();
                  this.timerForm.controls['ends'].updateValueAndValidity();
                  this.timerForm.controls['letters'].clearValidators();
                  this.timerForm.controls['letters'].updateValueAndValidity();
                  this.timerForm.controls['min_number'].clearValidators();
                  this.timerForm.controls['min_number'].updateValueAndValidity();
              }  else if (val === 'playoff') {
                  this.timerForm.controls['rounds'].clearValidators();
                  this.timerForm.controls['rounds'].updateValueAndValidity();
                  this.timerForm.controls['ends'].clearValidators();
                  this.timerForm.controls['ends'].updateValueAndValidity();
                  this.timerForm.controls['letters'].setValidators([Validators.required]);
                  this.timerForm.controls['letters'].updateValueAndValidity();
                  this.timerForm.controls['min_number'].setValidators([Validators.required]);
                  this.timerForm.controls['min_number'].updateValueAndValidity();
              }   else {
                  this.timerForm.controls['rounds'].clearValidators();
                  this.timerForm.controls['rounds'].updateValueAndValidity();
                  this.timerForm.controls['ends'].clearValidators();
                  this.timerForm.controls['ends'].updateValueAndValidity();
                  this.timerForm.controls['letters'].clearValidators();
                  this.timerForm.controls['letters'].updateValueAndValidity();
                  this.timerForm.controls['min_number'].clearValidators();
                  this.timerForm.controls['min_number'].updateValueAndValidity();
              }
          });
  }

  onAddTimer() {
    this.store.dispatch(new TimerActions.CreateTimer(this.timerForm.value));
  }
}
