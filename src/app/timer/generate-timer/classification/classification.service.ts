import { Injectable } from '@angular/core';

import { ClubEntityService } from 'src/app/club/services/club-entity.service';
import { Observable } from 'rxjs';
import { Club } from 'src/app/club/model/club.model';

import { map } from 'rxjs/operators';
import { ClassificationOrder } from './classification.model';



@Injectable({ providedIn: 'root' })
export class ClassificationService {


  classification$: Observable<ClassificationOrder[]>;

  constructor(private clubService: ClubEntityService) {
    this.classification$ = clubService.getAll()
        .pipe(map(clubs => {
            return [
              {
                division : 'recurvo hombre',
                order: 3,
                contender: 'Obi Wan Kenobi',
                club: clubs.find(c => c.name === 'Arco Club Navalmoral')
              },
              {
                division : 'recurvo hombre',
                order: 2,
                contender: 'Luke Skywalker',
                club: clubs.find(c => c.name === 'Arqueros de Villa de Valdemorillo')
              },
              {
                division : 'recurvo hombre',
                order: 1,
                contender: 'Han Solo',
                club: clubs.find(c => c.name === 'Arcosoto')
              },
              {
                division : 'recurvo mujer',
                order: 3,
                contender: 'Reina Amidala',
                club: clubs.find(c => c.name === 'Sagitta')
              },
              {
                division : 'recurvo mujer',
                order: 2,
                contender: 'Princesa Leia',
                club: clubs.find(c => c.name === 'Club Águila Imperial')
              },
              {
                division : 'recurvo mujer',
                order: 1,
                contender: 'Juana la Loca',
                club: clubs.find(c => c.name === 'CDETA Rivas Vaciamadrid')
              }
            ];
          }
      ));

  }
}
