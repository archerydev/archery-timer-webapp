import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MdcDialog } from '@angular-mdc/web';
import { SelectClubDialogComponent } from 'src/app/club/select-club-dialog.component';
import { Club } from 'src/app/club/model/club.model';
import { EnvService } from 'src/app/env.service';

@Component({
  selector: 'app-division',
  templateUrl: './division.component.html',
  styleUrls: ['./division.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DivisionComponent implements OnInit {

  @Input() divisionForm: FormGroup;
  @Input() index: number;
  @Output() deleteDivision: EventEmitter<number> = new EventEmitter();

  constructor(public dialog: MdcDialog, private envService: EnvService) { }

  ngOnInit() {
  }

  delete() {
    this.deleteDivision.emit(this.index);
  }

  onClub(controlName: string) {
     const dialogRef = this.dialog.open(SelectClubDialogComponent, {
      clickOutsideToClose: true,
      escapeToClose: true
    });

     dialogRef.afterClosed().subscribe(result => {
      const club = result as Club;
      this.divisionForm.get(controlName).setValue(club.name);
      this.divisionForm.get(controlName + 'Url').setValue(this.envService.getImageUrl(club.image_path));
    });
  }

}
