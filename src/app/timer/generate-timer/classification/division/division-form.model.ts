import { FormControl, Validators, FormArray } from '@angular/forms'
import { Division } from './../classification.model';
import { Injectable } from '@angular/core';
import { EnvService } from 'src/app/env.service';


export class DivisionForm {
  name = new FormControl();
  order = new FormControl();

  firstOrder = new FormControl();
  firstContender = new FormControl();
  firstClub = new FormControl();
  firstClubUrl = new FormControl();

  secondOrder = new FormControl();
  secondContender = new FormControl();
  secondClub = new FormControl();
  secondClubUrl = new FormControl();

  thirdOrder = new FormControl();
  thirdContender = new FormControl();
  thirdClub = new FormControl();
  thirdClubUrl = new FormControl();



constructor( division: Division, envService: EnvService
) {
    if (division.name) {
      this.name.setValue(division.name);
    }
    this.name.setValidators([Validators.required]);

    if (division.order) {
      console.log("aqui");
      console.log(division.order);
      this.order.setValue(division.order);
      console.log("despues");
    }
    this.order.setValidators([Validators.required]);

    this.firstOrder.setValue(division.divisionClassification[0].order);
    this.firstContender.setValue(division.divisionClassification[0].contender);
    if (division.divisionClassification[0].club) {
      this.firstClub.setValue(division.divisionClassification[0].club.name);
      this.firstClubUrl.setValue(envService.getImageUrl(division.divisionClassification[0].club.image_path));
    }
    if (division.divisionClassification.length > 1){


      this.secondOrder.setValue(division.divisionClassification[1].order);
      this.secondContender.setValue(division.divisionClassification[1].contender);
      if (division.divisionClassification[1].club) {
        this.secondClub.setValue(division.divisionClassification[1].club.name);
        this.secondClubUrl.setValue(envService.getImageUrl(division.divisionClassification[1].club.image_path));
      }
    }
    if (division.divisionClassification.length > 2){
    this.thirdOrder.setValue(division.divisionClassification[2].order);
    this.thirdContender.setValue(division.divisionClassification[2].contender);
    if (division.divisionClassification[2].club) {
      this.thirdClub.setValue(division.divisionClassification[2].club.name);
      this.thirdClubUrl.setValue(envService.getImageUrl(division.divisionClassification[2].club.image_path));
    }
  }
  }

}
