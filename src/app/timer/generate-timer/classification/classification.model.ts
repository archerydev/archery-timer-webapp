import { Club } from 'src/app/club/model/club.model';


export class Classification {

  name: string;
  divisions: Division[];

  constructor(name: string, divisions?: Division[]) {
    this.name = name;
    this.divisions = divisions;
  }

}

export class ClassificationOrder {
  order: number;
  contender: string;
  club: Club;

  constructor(order: number, contender: string, club: Club) {
    this.order = order;
    this.contender = contender;
    this.club = club;
  }

}

export class Division {
  name: string;
  order: number;
  divisionClassification: ClassificationOrder[];

  constructor(name: string, order?: number, divisionClassification?: ClassificationOrder[] ) {
    this.name = name;

    if (order) {
      this.order = order;
    }

    if (divisionClassification) {
      this.divisionClassification = divisionClassification;
    } else {
      this.divisionClassification = [];
      this.divisionClassification.push(new ClassificationOrder(1, '', null));
      this.divisionClassification.push(new ClassificationOrder(2, '', null));
      this.divisionClassification.push(new ClassificationOrder(3, '', null));
    }
  }
}

