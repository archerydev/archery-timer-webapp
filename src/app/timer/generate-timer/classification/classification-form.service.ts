import { Injectable } from '@angular/core'
import { Observable, BehaviorSubject } from 'rxjs'
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms'
import { ClassificationForm } from './classification-form.model';
import { Classification, Division } from './classification.model';
import { DivisionForm } from './division/division-form.model';
import { EnvService } from 'src/app/env.service';
//https://medium.com/@joshblf/dynamic-nested-reactive-forms-in-angular-654c1d4a769a

@Injectable({ providedIn: 'root' })
export class ClassificationFormService {


  private classificationForm: BehaviorSubject<FormGroup | undefined>
   =
    new BehaviorSubject(
      this.fb.group( new ClassificationForm(new Classification('Classification'))
    ));


  classificationForm$: Observable<FormGroup> = this.classificationForm.asObservable();

  constructor(private fb: FormBuilder, private envService: EnvService) {
  }

  loadClassification(classification: Classification) {

    this.classificationForm.next( this.fb.group(new ClassificationForm(classification)));
  }

  addDivision(division: Division = new Division('')) {

    const currentClassification = this.classificationForm.getValue();

    const currentDivisions = currentClassification.get('divisions') as FormArray;
    if (!division.order) {
      division.order = currentDivisions.length + 1;
    }
    console.log(division);
    currentDivisions.push(
      this.fb.group(
        new DivisionForm(division, this.envService)
      )
    );

    this.classificationForm.next(currentClassification);
  }

  clearDivisions() {
    const currentClassification = this.classificationForm.getValue();

    const currentDivisions = currentClassification.get('divisions') as FormArray;
    currentDivisions.clear();
  }

  deleteDivision(i: number) {
    const currentClassification = this.classificationForm.getValue();
    const currentDivisions = currentClassification.get('divisions') as FormArray;
    currentDivisions.removeAt(i);
    this.classificationForm.next(currentClassification);
  }
} // end class
