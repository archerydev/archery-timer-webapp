import { FormArray, FormControl, Validators } from '@angular/forms'
import { Classification } from './classification.model';

export class ClassificationForm {

  name = new FormControl();
  divisions = new FormArray([]);


  constructor(classification: Classification) {
      if (classification.name) {
        this.name.setValue([classification.name]);
      }

      if (classification.divisions) {
        this.divisions.setValue([classification.divisions]);
      }
      // this.divisions.setValidators([Validators.required]);

  }
}
