import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';


import * as fromRoot from '../../../app.reducer';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { ClubEntityService } from 'src/app/club/services/club-entity.service';
import { FormGroup, FormArray } from '@angular/forms';
import { MdcDialog } from '@angular-mdc/web';
import { ClassificationOrder, Division } from './classification.model';
import { map } from 'rxjs/operators';
import { ClassificationFormService } from './classification-form.service';
import * as TimerActions from '../../store/timer.actions';
import  * as csv2json from 'csvjson-csv2json';
import * as _ from 'lodash';

@Component({
  selector: 'app-classification',
  templateUrl: './classification.component.html',
  styleUrls: ['./classification.component.scss']
})
export class ClassificationComponent implements OnInit, OnDestroy {

  classificationForm: FormGroup;
  classificationFormSub: Subscription;
  divisions: FormArray;

  @ViewChild('fileInput', {static: true}) public myInputVariable: ElementRef;

  constructor( private store: Store<fromRoot.State>,
               private classificationFormService: ClassificationFormService,
               public dialog: MdcDialog,
               private clubService: ClubEntityService) { }

  ngOnInit() {
    this.classificationFormSub = this.classificationFormService.classificationForm$
      .subscribe(classificaton => {
        this.classificationForm = classificaton;
        this.divisions = this.classificationForm.get('divisions') as FormArray;
      });
  }

  onLoadClassification() {
      const json = _.flatMap(this.classificationForm.value['divisions'],
                  x => [{div_order: x.order, division: x.name, order: 1, contender: x.firstContender, club: x.firstClub},
                        {div_order: x.order, division: x.name, order: 2, contender: x.secondContender, club: x.secondClub},
                        {div_order: x.order, division: x.name, order: 3, contender: x.thirdContender, club: x.thirdClub}]);
      console.log(json);
      this.store.dispatch(new TimerActions.LoadClassification(json));
  }

  ngOnDestroy() {
    this.classificationFormSub.unsubscribe();
  }

  addDivision() {
    this.classificationFormService.addDivision();
  }

  deleteDivision(index: number) {
    this.classificationFormService.deleteDivision(index);
  }

  fileChangeListener($event): void {
    const file = $event.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {

      const json = csv2json(reader.result, {parseNumbers: true});
      console.log(json);
      this.myInputVariable.nativeElement.value = '';
      this.classificationFormService.clearDivisions();
      this.clubService.getAll()
        .pipe(map(clubs => {
          json.map(x => x.club = clubs.find(c => c.name === x.club));
          const divisions = _.mapValues(_.groupBy(json, 'division'),  x => x.map(y => _.omit(y, 'division')));
          console.log(divisions);
          return divisions;
        }))
        .subscribe(j => {
            for (const key in j) {
              if (j.hasOwnProperty(key)) {
                console.log(j[key][0].d_order);
                this.classificationFormService.addDivision(
                    new Division(key, j[key][0].d_order, j[key].map(c => new ClassificationOrder(c.order, c.contender, c.club))));
              }
            }
          }

        );
    };
    reader.readAsText(file);
}
}
