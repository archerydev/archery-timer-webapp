import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../app.reducer';

import { FetchTimers } from '../store/timer.actions';
import { Timer,TimerState, TimerTemplate } from '../timer-api.model';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-generate-timer',
  templateUrl: './generate-timer.component.html',
  styleUrls: ['./generate-timer.component.scss']
})
export class GenerateTimerComponent implements OnInit {
  // currentTimer$: Observable<Timer>;
  // timerList$: Observable<TimerState[]>;
  timerTemplate$: Observable<string>;


  constructor(private store: Store<fromRoot.State>) { }

  ngOnInit() {
    this.store.dispatch(new FetchTimers());
    // this.currentTimer$ = this.store.select(fromRoot.getCurrentTimer);
    // this.timerList$ = this.store.select(fromRoot.getSavedTimers);
    this.timerTemplate$ = this.store.select(fromRoot.getSelectedTemplate).pipe(filter (tmpl => tmpl !== null), map( tmpl => tmpl.timer_type));

  }

}
