import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { MdcDialog } from '@angular-mdc/web';
import { OkCancelDialogComponent } from '../../../shared/okcancel-dialog.component';

import * as fromRoot from '../../../app.reducer';
import { LoadTimer, DeleteTimer, SelectTemplate, ClearTimer } from '../../store/timer.actions';
import { TimerState, TimerTemplate } from '../../timer-api.model';

@Component({
  selector: 'app-timer-list',
  templateUrl: './timer-list.component.html',
  styleUrls: ['./timer-list.component.scss']
})
export class TimerListComponent implements OnInit {

  savedVisible = true;
  tournamentVisible = true;
  warmingVisible = false;
  variousVisible = false;
  playoffVisible = false;
   variousTemplates: TimerTemplate[] = [
    {
      name: 'Personalizado',
      timer_type: 'tournament'
     },
     {
      name: 'Recuperación 40s',
      duration: 40,
      letters: 2,
      timer_type: 'arrow'
     },
     {
      name: 'Cuenta atrás',
      timer_type: 'timer'
     },
     {
      name: 'Logo',
      timer_type: 'drawable'
     },
     {
       name: 'Classification',
       timer_type: 'classification'
     }
  ];

  tournamentTemplates: TimerTemplate[] = [
    {
      name: '240s Doble Turno',
      duration: 240,
      letters: 4,
      timer_type: 'tournament'
     },
     {
      name: '240s Turno Único',
      duration: 240,
      letters: 2,
      timer_type: 'tournament'
     },
     {
      name: '120s Doble Turno',
      duration: 120,
      letters: 4,
      timer_type: 'tournament'
     },
     {
      name: '120s Turno Único',
      duration: 120,
      letters: 2,
      timer_type: 'tournament'
     },
  ];

  warmingTemplates: TimerTemplate[] = [
     {
      name: '240 Turno Doble',
      duration: 240,
      letters: 4,
      timer_type: 'warming'
     },
     {
      name: '240 Turno Único',
      duration: 240,
      letters: 2,
      timer_type: 'warming'
     },
     {
      name: '120 Turno Doble',
      duration: 120,
      letters: 4,
      timer_type: 'warming'
     },
     {
      name: '120 Turno Único',
      duration: 120,
      letters: 2,
      timer_type: 'warming'
     },
  ];
  playoffTemplates: TimerTemplate[] = [
    {
      name: 'Doble turno Precision',
      duration: 120,
      letters: 4,
      timer_type: 'playoff',
      min_number: 26
    },
    {
      name: 'Turno único Precision',
      duration: 120,
      letters: 2,
      timer_type: 'playoff',
      min_number: 26
    },
    {
      name: 'Doble turno Trad',
      duration: 120,
      letters: 4,
      timer_type: 'playoff',
      min_number: 12
    },
    {
      name: 'Turno único Trad',
      duration: 120,
      letters: 2,
      timer_type: 'playoff',
      min_number: 12
    }
  ];



  timerList$: Observable<TimerState[]>;

  constructor(private store: Store<fromRoot.State>, public dialog: MdcDialog) { }

  ngOnInit() {
    this.timerList$ = this.store.select(fromRoot.getSavedTimers);
  }

  onLoadTimer(timerState: TimerState) {
    const timerText = timerState.timer.name
                + " R" + timerState.order.num_rounds
                + " E"  + timerState.order.num_ends
                + " L" + timerState.timer.letters
                +  " N" + timerState.order.number;

    const dialogRef = this.dialog.open(OkCancelDialogComponent, {
      escapeToClose: false,
      clickOutsideToClose: false,
      buttonsStacked: false,
      id: 'my-dialog',
      data: {mainText:'¿Cargar el cronómetro sustituyendo el actual?' , secondaryText: timerText, buttonText: "Cargar"}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'accept') {
        this.store.dispatch(new LoadTimer(timerState));
      }
    });
  }
  onDeleteTimer(timerState: TimerState) {
    const dialogRef = this.dialog.open(OkCancelDialogComponent, {
      escapeToClose: false,
      clickOutsideToClose: false,
      buttonsStacked: false,
      id: 'my-dialog',
      data: {mainText: "¿Borrar el cronómetro?" , buttonText: "Borrar"}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'accept') {
        this.store.dispatch(new DeleteTimer(timerState));
      }
    });

  }

  onSelectTemplate(timerTemplate: TimerTemplate) {
    if (timerTemplate.timer_type === 'drawable') {
      this.store.dispatch(new ClearTimer());
    } else {
      this.store.dispatch(new SelectTemplate(timerTemplate));
    }
  }
}
