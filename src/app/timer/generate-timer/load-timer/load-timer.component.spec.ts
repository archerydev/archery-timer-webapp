import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadTimerComponent } from './load-timer.component';

describe('LoadTimerComponent', () => {
  let component: LoadTimerComponent;
  let fixture: ComponentFixture<LoadTimerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadTimerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadTimerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
