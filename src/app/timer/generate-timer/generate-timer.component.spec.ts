import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateTimerComponent } from './generate-timer.component';

describe('GenerateTimerComponent', () => {
  let component: GenerateTimerComponent;
  let fixture: ComponentFixture<GenerateTimerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateTimerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateTimerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
