import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { EnvService } from '../env.service';
import { WebSocketSubject, webSocket } from 'rxjs/webSocket';
import { HttpClient } from '@angular/common/http';


@Injectable({ providedIn: 'root' })
export class CanvasService {
  private BACKEND_URL: string;
  WEBSOCKET_URL: string;
  private canvasws: WebSocketSubject<Uint8ClampedArray>;

  $canvasPixels = new Subject<any>();
  defaultColor = new Uint8ClampedArray([128, 128, 128]);

  numRows = 64;
  numCols = 128;
  pixels: Uint8ClampedArray;

  private pc: RTCPeerConnection;
  private dc: RTCDataChannel;

  constructor(private env: EnvService, private http: HttpClient) {
    this.WEBSOCKET_URL = this.env.wsUrl + '/canvas/';
    this.BACKEND_URL = this.env.apiUrl + '/canvas/';
    //this.createPeerConnection();
    this.canvasws = webSocket({
          url: this.WEBSOCKET_URL + 'ws'
        , deserializer: ({data}) =>  new Uint8ClampedArray(data)
        , binaryType: 'arraybuffer'
      });
  }

  getWsObservable() {
    return this.canvasws;
  }


  async createPeerConnection() {
    const config = {
        sdpSemantics: 'unified-plan'
    };

    this.pc = new RTCPeerConnection(null);

    this.pc.addEventListener('iceconnectionstatechange', function() {
      console.log('iceConnectionState: ' + this.pc.iceConnectionState);
    }.bind(this), false);


    this.pc.onicegatheringstatechange = ev => {
      let connection = ev.target as RTCPeerConnection;
      console.log(connection.iceGatheringState);
      switch(connection.iceGatheringState) {
        case "gathering":
          /* collection of candidates has begun */
          break;
        case "complete":

          let offer2 = connection.localDescription;
          console.log("offer2");
          console.log(offer2.sdp);
          this.http.post<any>(this.BACKEND_URL + 'offer', {sdp: offer2.sdp, type: offer2.type})
          .subscribe( v => {
                console.log(v.sdp);
                console.log(v.type);
                connection.setRemoteDescription(v);
            });
            /* collection of candidates is finished */
            break;
      }
    }

    this.pc.addEventListener('signalingstatechange', function() {
      console.log('signalingState: ' + this.pc.signalingState);

    }.bind(this), false);



    this.dc = this.pc.createDataChannel('chat2', {ordered: false, maxRetransmits: 0});
    this.dc.binaryType = "arraybuffer";
    this.dc.onopen = event => {
      console.log('open channel');
    };
    this.dc.onclose = event => {
      console.log('closed channel');
    };

    this.dc.onmessage = event => {
          const ar = new Uint8ClampedArray(event.data);
          this.$canvasPixels.next(ar);
          //this.setImageData(ar);
    };

    console.log('peerconnection ' + this.pc);
    const offer = await this.pc.createOffer();
    console.log("offer created");
    this.pc.setLocalDescription(offer);
    console.log(offer.sdp );
    console.log(offer.type);
    // const a = null;
    // const v = await this.http.post<any>(this.BACKEND_URL + 'offer', {sdp: offer.sdp, type: offer.type, video_transform: a}).toPromise();
    // console.log(v.sdp);
    // await this.pc.setRemoteDescription(v);


  }
}
