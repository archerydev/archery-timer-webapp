import { Round } from './timer.model';

export interface PanelColors {
  backcolor: string;
  green: string;
  yellow: string;
  red: string;
}


export interface PanelSounds {
  sound_name: string;
  speed: number;
  delay: number;
}

export interface Timer {
  timer_hash: string;
  name: string;
  rounds: Round[];
  letters: number;
  number: number;
  timer_type: string;
}


export interface TimerOrder {
  timer_hash?: string;
  round?: number;
  num_rounds?: number;
  end?: number;
  end_name?: string;
  num_ends?: number;
  phase?: number;
  phases_names?: string[];
  can_be_finished?: boolean;
  can_create_end?: boolean;
  status?: number;
  number?: number;
  finished?: boolean;
  number_color?: string;
  turn1?: string;
  turn1_color?: string;
  turn2?: string;
  turn2_color?: string;
  sound?: number;
  text?: string;
  secondary_text?: string;

}

export interface TimerState {
  timer: Timer;
  order: TimerOrder;
}

export interface TimerTemplate {
  timer_hash?: string;
  name: string;
  rounds?: number;
  ends?: number;
  duration?: number;
  letters?: number;
  timer_type: string;
  min_number?: number;
}

export interface ClassificationOrder {
  div_order: number;
  division: string;
  order: number;
  contender: string;
  club: string;
}

