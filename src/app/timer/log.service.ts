import { Injectable } from '@angular/core';
import { EnvService } from '../env.service';
import { WebSocketSubject, webSocket } from 'rxjs/webSocket';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { LogEntry } from './timer.model';
import { map, distinctUntilChanged } from 'rxjs/operators';


@Injectable({ providedIn: 'root' })
export class LogService {
  private WEBSOCKET_URL: string;
  private BACKEND_URL:string;

  private logws: WebSocketSubject<string[]>;
  private logSubject = new BehaviorSubject<LogEntry[]>([]);

  public logList: LogEntry[] = [] ;

  constructor(private env: EnvService, private http: HttpClient) {
    this.WEBSOCKET_URL = this.env.wsUrl + '/log/';
    this.BACKEND_URL = this.env.apiUrl + '/log/';


    this.logws = webSocket({
          url: this.WEBSOCKET_URL + 'ws?tail=1'
         , deserializer: ({data}) => data.split('\n')
         , binaryType: 'arraybuffer'
      });

    this.logws
        .subscribe(
          lg => {
            const entries = lg.map(l => l.split(';'))
              .map(l => {
                  const logEntry: LogEntry = {
                    time: l[0],
                    timer_hash: l[1],
                    round: l[2],
                    end: l[3],
                    action: l[4],
                    description: l[5]
                  };
                  return logEntry;
                }
              );
            //TODO: check why an empty line is created
            this.logList.push(...entries.filter(log => typeof log.time !== 'undefined' && log.time));
            this.logSubject.next(this.logList);

          }, // Called whenever there is a message from the server.
          err => console.log(err), // Called if at any point WebSocket API signals some kind of error.
          () => console.log('complete') // Called when connection is closed (for whatever reason).
      );
  }

  clearLog() {
    this.http.post(this.BACKEND_URL + 'clear', {})
      .subscribe(() => {
        this.logList = [];
        this.logSubject.next(this.logList);
      });
   }

   addLog(log: string) {
     return this.http.put(this.BACKEND_URL, {description: log}).subscribe();
   }

   connect(): Observable<LogEntry[]> {
    console.log("Connecting data source");
    return this.logSubject.asObservable();//.pipe(distinctUntilChanged());
  }

  disconnect(): void {
      this.logSubject.complete();
  }

}
