import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../app.reducer';
import * as TimerActions from '../store/timer.actions';
import { TimerControlAction, RunningStatus } from '../timer.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MdcDialog } from '@angular-mdc/web';
import { AddEndDialogComponent } from './addend-dialog.component';

@Component({
  selector: 'app-timer-control',
  templateUrl: './timer-control.component.html',
  styleUrls: ['./timer-control.component.scss']
})
export class TimerControlComponent implements OnInit {
  canEndPhase$: Observable<boolean>;
  canCreateEnd$: Observable<boolean>;
  finished$: Observable<boolean>;
  runningStatus$: Observable<RunningStatus>;
  RunningStatus: any = RunningStatus;
  constructor(private store: Store<fromRoot.State>, public dialog: MdcDialog) { }

  ngOnInit() {
    this.runningStatus$ = this.store.select(fromRoot.getTimerOrder).pipe(map(st => st && st.status));
    this.canEndPhase$ = this.store.select(fromRoot.getTimerOrder).pipe(map(st => st && st.can_be_finished));
    this.canCreateEnd$ = this.store.select(fromRoot.getTimerOrder).pipe(map(st => st && st.can_create_end));
    this.finished$ = this.store.select(fromRoot.getTimerOrder).pipe(map(st => st && st.finished));
  }

  onPlay() {
    this.store.dispatch(new TimerActions.ExecTimerAction(TimerControlAction.Start));
  }

  onStop() {
    this.store.dispatch(new TimerActions.ExecTimerAction(TimerControlAction.Stop));
  }

  onPause() {
    this.store.dispatch(new TimerActions.ExecTimerAction(TimerControlAction.Pause));
  }

  onEndPhase() {
    this.store.dispatch(new TimerActions.ExecTimerAction(TimerControlAction.EndPhase));
  }
  onNewEnd() {
    const dialogRef = this.dialog.open(AddEndDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });

  }
}
