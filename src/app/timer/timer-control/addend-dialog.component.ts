import { Component, OnInit } from '@angular/core';
import { MdcDialogRef } from '@angular-mdc/web';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../app.reducer';
import { End} from '../timer.model';
import { Timer, TimerOrder } from '../timer-api.model';
import { map } from 'rxjs/operators';
import { TimerService } from '../timer.service';

const range = (start, end) => Array.from({length: (end - start)}, (v, k) => k + start);
@Component({
    templateUrl: 'addend-dialog.component.html',
  })
export class AddEndDialogComponent  implements OnInit {

  currentTimer$: Observable<Timer>;
  currentOrder$: Observable<TimerOrder>;
  isLoading$: Observable<boolean>;


  end: End;
  numLetters: number;
  minNumber = 26;

  endForm = new FormGroup({
    minNumber: new FormControl(''),
    useDoubleTurn: new FormControl(''),
    finalEnd: new FormControl(false)
  });

  constructor(public dialogRef: MdcDialogRef<AddEndDialogComponent>,
              private store: Store<fromRoot.State>,
              private timerService: TimerService) { }

  timer: Timer;
  ngOnInit() {
    this.currentOrder$ = this.store.select(fromRoot.getTimerOrder);
    this.store
        .select(fromRoot.getTimerOrder)
        .pipe(
          map(to => ({ minNumber: +to.end_name + 1, useDoubleTurn: to.phases_names.length > 4 }))
          )
        .subscribe(to => this.endForm.patchValue(to)
      );
    this.endForm.get('finalEnd').valueChanges.subscribe(val => {
        console.log(val);
        if (val) {
          this.endForm.controls['minNumber'].clearValidators();
          this.endForm.controls['minNumber'].updateValueAndValidity();
        }
        else {
          this.endForm.controls['minNumber'].setValidators([Validators.required]);
          this.endForm.controls['minNumber'].updateValueAndValidity();
        }
        
      })

  }

  submit(): void {
    if (this.endForm.invalid) {
      return;
    }
    console.log(this.endForm.value);
    this.timerService.addEndToTimer(this.endForm.value);
    this.dialogRef.close();
  }

}
