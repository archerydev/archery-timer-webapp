export interface Round {
  name: string;
  num_ends: number;
  ends: End[];
}
export interface End {
  name: string;
  phases: Phase[];
}

export interface Phase {
  name: string;
  init_value: number;
  end_value: number;
  first_letter: string;
  second_letter: string;
  number_coloring: Coloring;
  sound_coloring: Coloring;
  can_be_finished: boolean;
  text: string;
  secondary_text: string;
}

export interface Coloring {
  limits: number[];
  colors: string[];
}

export enum RunningStatus {
  Stopped,
  Running,
  Paused
}

export enum TimerControlAction {
  Start,
  Pause,
  Stop,
  EndPhase
}

export interface LogEntry {
  time: string;
  timer_hash: string;
  round: string;
  end: string;
  action: string;
  description: string;
}


