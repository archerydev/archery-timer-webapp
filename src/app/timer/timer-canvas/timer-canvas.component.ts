import { Component, OnInit, ElementRef, AfterViewInit, ViewChild, OnDestroy, NgZone } from '@angular/core';
import { Observable, Subscription, asyncScheduler } from 'rxjs';
import { Timer } from '../timer-api.model';

import { CanvasService } from '../canvas.service';
import { PanelColors } from '../timer-api.model';
import { map, observeOn, subscribeOn } from 'rxjs/operators';
import { animationFrame } from 'rxjs/internal/scheduler/animationFrame';

@Component({
  selector: 'app-timer-canvas',
  templateUrl: './timer-canvas.component.html',
  styleUrls: ['./timer-canvas.component.scss']
})
export class TimerCanvasComponent implements OnInit, AfterViewInit, OnDestroy {
  panelColors: PanelColors;
  numCols: number;
  numRows: number;

  subscription: Subscription;


  constructor(private canvasService: CanvasService, private zone: NgZone) { }

  @ViewChild('canvas', { static: true }) public canvas: ElementRef;
  private cx: CanvasRenderingContext2D;
  currentTimer$: Observable<Timer>;
  imageData: ImageData;
  buf: ArrayBuffer;
  data32:Uint32Array;

  timer: Timer;

  ngOnInit() {
    this.numCols = this.canvasService.numCols;
    this.numRows = this.canvasService.numRows;
  }

  public ngAfterViewInit() {
    const canvasEl: HTMLCanvasElement = this.canvas.nativeElement;
    canvasEl.width = this.numCols;
    canvasEl.height = this.numRows;
    this.cx = this.canvas.nativeElement.getContext('2d');
    this.imageData = this.cx.getImageData(0, 0, this.numCols, this.numRows);
    this.data32 = new Uint32Array(this.imageData.data.buffer);

    this.subscription = this.canvasService.getWsObservable()
        .pipe(
          subscribeOn(asyncScheduler),
          map( pixels => {
            this.zone.runOutsideAngular(() =>{
              for (let y = 0; y < this.numRows; ++y) {
                for (let x = 0; x < this.numCols; ++x) {
                  const index = (y * this.numCols + x);
                  const pos = index * 3;
                  // tslint:disable-next-line:no-bitwise
                  this.data32[index] =  (255 << 24) | (pixels[pos + 2] << 16) | (pixels[ pos + 1] << 8) | pixels[pos];
                }
            }
          });
      }
      )).subscribe(() =>  this.cx.putImageData(this.imageData, 0, 0),
        err => console.log(err), // Called if at any point WebSocket API signals some kind of error.
      () => console.log('complete'));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


}
