import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimerCanvasComponent } from './timer-canvas.component';

describe('TimerCanvasComponent', () => {
  let component: TimerCanvasComponent;
  let fixture: ComponentFixture<TimerCanvasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimerCanvasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimerCanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
