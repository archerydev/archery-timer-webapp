
import { Component } from '@angular/core';
import { MdcDialogRef } from '@angular-mdc/web';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LogService } from '../log.service';


@Component({
  templateUrl: 'addlog-dialog.component.html',
})
export class AddLogDialogComponent  {

  logForm = new FormGroup({
    logDescription: new FormControl('', Validators.required),
  });

  constructor(public dialogRef: MdcDialogRef<AddLogDialogComponent>,
              private logService: LogService) { }


  submit(): void {
      if (this.logForm.invalid) {
        return;
      }
      console.log(this.logForm.value);
      //TODO: use store
      this.logService.addLog(this.logForm.value.logDescription);
      this.dialogRef.close();
  }

}
