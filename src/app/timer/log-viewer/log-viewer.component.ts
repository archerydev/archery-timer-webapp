import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MdcDialog } from '@angular-mdc/web';
import { AddLogDialogComponent } from './addlog-dialog.component';
import { LogService } from '../log.service';
import { OkCancelDialogComponent } from 'src/app/shared/okcancel-dialog.component';
import { LogEntry } from '../timer.model';

@Component({
  selector: 'app-log-viewer',
  templateUrl: './log-viewer.component.html',
  styleUrls: ['./log-viewer.component.scss']
})
export class LogViewerComponent implements OnInit {

  public log$: Observable<LogEntry[]>;
  constructor(private logService: LogService, public dialog: MdcDialog) { }

  ngOnInit() {
    this.log$ = this.logService.connect();
  }

  onClear() {

    const dialogRef = this.dialog.open(OkCancelDialogComponent, {
      escapeToClose: false,
      clickOutsideToClose: false,
      buttonsStacked: false,
      id: 'my-dialog',
      data: {mainText:'¿Limpiar el fichero de Logs?' , secondaryText: "Esta operacion no se podrá deshacer", buttonText: "Limpiar"}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'accept') {
        this.logService.clearLog();
      }
    });
  }

  onNewEntry() {
    const dialogRef = this.dialog.open(AddLogDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }
}
