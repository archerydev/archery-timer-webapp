import { Injectable } from '@angular/core';
import { DefaultDataServiceConfig } from '@ngrx/data';
import { EnvService } from './env.service';


@Injectable()
export class CustomDefaultDataServiceConfig extends DefaultDataServiceConfig {
  constructor(private envService: EnvService) {
    super();
    this.root =  this.envService.apiUrl;

    this.entityHttpResourceUrls =  {
      // Case matters. Match the case of the entity name.
      Club: {
        // You must specify the root as part of the resource URL.
        entityResourceUrl: this.root + '/club/club/',
        collectionResourceUrl: this.root + '/club/clubs/'
      }
    }
  }

}
