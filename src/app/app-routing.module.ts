import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { SwaggerComponent } from './swagger/swagger.component';
import { ClubsComponent } from './club/clubs.component';
import { SystemComponent } from './system/system.component';

const routes: Routes = [
  { path: 'timer' ,  loadChildren: () => import('./timer/timer.module').then(m => m.TimerModule) },
  { path: 'auth', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)},
  { path: 'swagger', component: SwaggerComponent },
  { path: 'system', component: SystemComponent },
  { path: 'club', loadChildren: () => import('./club/archery.module').then(m => m.ArcheryModule) },
  { path: '',   redirectTo: '/timer/timer', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes ) ],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule {}
