import { Component, OnInit } from '@angular/core';
import { MdcDialogRef, MdcDialog, MdcDialogConfig } from '@angular-mdc/web';
import { Observable, from } from 'rxjs';
import { SystemService } from '../system/system.service';
import { EnvService } from '../env.service';
import { tap, map, mergeMap, switchMap } from 'rxjs/operators';
import { UploadFileDialogComponent } from './upload-file-dialog.component';

@Component({
    templateUrl: 'file-list-dialog.component.html',
    styleUrls: ['file-list-dialog.component.scss']

  })
export class FileListDialogComponent implements OnInit {

  filesPath$: Observable<string[]>;

  constructor(public dialogRef: MdcDialogRef<FileListDialogComponent>,
              private envService: EnvService,
              private systemService: SystemService,
              public dialog: MdcDialog) {

  }

  ngOnInit() {
    this.filesPath$ = this.systemService.getFilesPathObs()
          .pipe(map( fl => fl.files));
  }

  getFilesUrl(relativePath: string) {
      return this.envService.getFilesUrl() + '/' + relativePath;
  }

  upload() {
    const dialogConfig = new MdcDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      mode: 'singleFile'
    };
    const dref = this.dialog.open(UploadFileDialogComponent, dialogConfig);
    dref.afterClosed().subscribe(result => {
      if (result !== null && result !== '') {
       this.dialogRef.close((result as {file_path: string}));
      }
    });
  }

  selectFile(filepath: string){
    this.dialogRef.close({file_path: filepath});
  }
}
