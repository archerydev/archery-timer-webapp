import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { featureReducer } from './store/reducer';
import { UploadFileEffects } from './store/effects';
import { UploadFileDialogComponent } from './upload-file-dialog.component';
import { MaterialModule } from '../material.module';
import { FileListDialogComponent } from './file-list-dialog.component';
//https://blog.ag-grid.com/managing-file-uploads-with-ngrx-2/


@NgModule({
  declarations: [UploadFileDialogComponent, FileListDialogComponent],
  imports: [
    MaterialModule,
    CommonModule,
    StoreModule.forFeature('uploadFile', featureReducer),
    EffectsModule.forFeature([UploadFileEffects])
  ],
  entryComponents: [UploadFileDialogComponent, FileListDialogComponent]
})
export class UploadFileStoreModule { }
