import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';

import { Observable } from 'rxjs';
import { EnvService } from '../env.service';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  private BACKEND_URL: string;

  constructor(private httpClient: HttpClient, private env: EnvService) {

    this.BACKEND_URL = this.env.apiUrl + '/system/file';
  }

  public uploadFile(file: File): Observable<HttpEvent<{}>> {
    const formData = new FormData();
    formData.append('files', file, file.name);

    const options = {
      reportProgress: true
    };

    const req = new HttpRequest(
      'POST',
      `${this.BACKEND_URL}`,
      formData,
      options
    );
    return this.httpClient.request(req);
  }
}
