import { Component, OnInit,  Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import * as fromRoot from '../../app.reducer';
import * as AuthActions from '../../auth/store/auth.actions';
import * as TimerActions from '../../timer/store/timer.actions';
import { map } from 'rxjs/operators';
import { version } from '../../../../package.json';
import { RunningStatus } from 'src/app/timer/timer.model';
import { MdcDialog, MdcIconRegistry } from '@angular-mdc/web';
import { SetOrderDialogComponent } from 'src/app/shared/setorder-dialog.component';
import { DomSanitizer } from '@angular/platform-browser';
import { Timer } from 'src/app/timer/timer-api.model';
//import { csv2json } from  'csvjson-csv2json';
import  * as csv2json from 'csvjson-csv2json';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public version: string = version;
  @Output() sidenavToggle = new EventEmitter<void>();

  isTimerRunning$: Observable<boolean>;
  currentTimer$: Observable<Timer>;
  isAuth$: Observable<boolean>;
  isInLogin$: Observable<boolean>;

  constructor(private store: Store<fromRoot.State>, public dialog: MdcDialog, iconRegistry: MdcIconRegistry,  sanitizer: DomSanitizer) {
    iconRegistry
    .addSvgIcon('logo',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/logo.svg'));

   }

  ngOnInit() {
    this.isAuth$ = this.store.select(fromRoot.getIsAuth);
    this.isInLogin$ = this.store.select(fromRoot.getUrl).pipe(map( url => url === '/auth/login'));
    this.currentTimer$ = this.store.select(fromRoot.getCurrentTimer);
    this.isTimerRunning$ = this.store.select(fromRoot.getRunningStatus)
        .pipe( map(status => status === RunningStatus.Running));
  }

  onLogout() {
    this.store.dispatch(new AuthActions.Logout());
  }

  onClear() {
    this.store.dispatch(new TimerActions.ClearTimer());
  }

  onToggleSidenav() {
    console.log("evento");
    this.sidenavToggle.emit();
  }

  onSetOrder() {
    const dialogRef = this.dialog.open(SetOrderDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }


}
