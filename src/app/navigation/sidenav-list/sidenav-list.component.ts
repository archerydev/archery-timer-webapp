import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../app.reducer';
import * as TimerActions from '../../timer/store/timer.actions';
import { map } from 'rxjs/operators';
import { RunningStatus } from 'src/app/timer/timer.model';
import * as AuthActions from '../../auth/store/auth.actions';
import { version } from '../../../../package.json';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.scss']
})
export class SidenavListComponent implements OnInit {
  public version: string = version;
  @Output() closeSidenav = new EventEmitter<void>();
  isTimerRunning$: Observable<boolean>;
  isAuth$: Observable<boolean>;
  isInLogin$: Observable<boolean>;
  constructor(private store: Store<fromRoot.State>) { }

  ngOnInit() {
    this.isAuth$ = this.store.select(fromRoot.getIsAuth);
    this.isInLogin$ = this.store.select(fromRoot.getUrl).pipe(map( url => url === '/auth/login'));
    this.isTimerRunning$ = this.store.select(fromRoot.getRunningStatus)
        .pipe( map(status => status === RunningStatus.Running));
  }

  onLogout() {
    this.onClose();
    this.store.dispatch(new AuthActions.Logout());
  }

  onClear() {
    this.onClose();
    this.store.dispatch(new TimerActions.ClearTimer());
  }

  onClose() {
    this.closeSidenav.emit();
  }
}
