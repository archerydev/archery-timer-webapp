import { ActionReducerMap, createFeatureSelector, createSelector, ActionReducer, MetaReducer } from '@ngrx/store';
import { localStorageSync } from 'ngrx-store-localstorage';

import * as fromUi from './shared/ui.reducer';
import * as fromAuth from './auth/store/auth.reducer';
import * as fromTimer from './timer/store/timer.reducer';
import { Params, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { RouterStateSerializer, RouterReducerState, routerReducer } from '@ngrx/router-store';

export interface State {
  ui: fromUi.State;
  auth: fromAuth.State;
  timer: fromTimer.State;
  router: RouterReducerState<RouterStateUrl>;
}

export const reducers: ActionReducerMap<State> = {
  ui: fromUi.uiReducer,
  auth: fromAuth.authReducer,
  timer: fromTimer.timerReducer,
  router: routerReducer
};

export interface RouterStateUrl {
  url: string;
  params: Params;
  queryParams: Params;
}

@Injectable()
export class CustomSerializer implements RouterStateSerializer<RouterStateUrl> {
  serialize(routerState: RouterStateSnapshot): RouterStateUrl {
    let route = routerState.root;

    while (route.firstChild) {
      route = route.firstChild;
    }
    const {
      url,
      root: { queryParams }
    } = routerState;
    const { params } = route;

    // Only return an object including the URL, params and query params
    // instead of the entire snapshot
    return { url, params, queryParams };
  }
}

export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({keys: ['auth'], rehydrate: true})(reducer);
}

export const metaReducers: Array<MetaReducer<any, any>> = [localStorageSyncReducer];

export const getUiState = createFeatureSelector<fromUi.State>('ui');
export const getIsLoading = createSelector(getUiState, fromUi.getIsLoading);

export const getAuthState = createFeatureSelector<fromAuth.State>('auth');
export const getIsAuth = createSelector(getAuthState, fromAuth.getIsAuth);
export const getExpDate = createSelector(getAuthState, fromAuth.getExpDate);
export const getToken = createSelector(getAuthState, fromAuth.getToken);

export const getRouterState = createFeatureSelector<RouterReducerState<RouterStateUrl>>('router');
export const getRouterInfo = createSelector(getRouterState, state => state.state);
export const getUrl = createSelector(getRouterState, state => state.state  && state.state.url);

export const getTimerState = createFeatureSelector<fromTimer.State>('timer');
export const getCurrentTimer = createSelector(getTimerState, fromTimer.getCurrentTimer);
export const getTimerOrder = createSelector(getTimerState, fromTimer.getTimerOrder);
export const getRunningStatus = createSelector(getTimerState, fromTimer.getRunningStatus);
export const getSavedTimers = createSelector(getTimerState, fromTimer.getSavedTimers);
export const getPanelColors = createSelector(getTimerState, fromTimer.getPanelColors);
export const getPanelSounds = createSelector(getTimerState, fromTimer.getPanelSounds);
export const getSelectedTemplate = createSelector(getTimerState, fromTimer.getSelectedTemplate);
