import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler, Injectable, isDevMode, Provider } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  StoreRouterConnectingModule,
  NavigationActionTiming
} from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import * as Sentry from '@sentry/browser';

import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { HeaderComponent } from './navigation/header/header.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthModule } from './auth/auth.module';

import { AuthInterceptor } from './auth/auth-interceptor';
import { reducers, CustomSerializer } from './app.reducer';
import { metaReducers } from './app.reducer';
import { AuthEffects } from './auth/store/auth.effects';
import { environment } from './../environments/environment';
import { TimerEffects } from './timer/store/timer.effects';
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';
import { VERSION } from 'src/environments/version';
import { SidenavListComponent } from './navigation/sidenav-list/sidenav-list.component';
import { EnvServiceProvider } from './env.service.provider';
import { SwaggerComponent } from './swagger/swagger.component';
import { EntityDataModule, DefaultDataServiceConfig } from '@ngrx/data';
import { CustomDefaultDataServiceConfig } from './custom-dataservice-config';
import { SystemComponent } from './system/system.component';


Sentry.init({
  dsn: 'https://3a11d61d9ea54c30ba5532314f2c1a9c@sentry.io/1408005',
  release: "archeryTimer@" + VERSION.tag,
  beforeSend(event) {
    // Check if it is an exception, if so, show the report dialog
    if (event.exception) {
      Sentry.showReportDialog();
    }
    return event;
  }
});

@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  constructor() { }
  handleError(error) {
    console.log("Error, Sentry capturing")
    Sentry.captureException(error.originalError || error);
    throw error;
  }
}

const providers: Provider[] = [
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  JwtHelperService,
  EnvServiceProvider,
  { provide: DefaultDataServiceConfig, useClass: CustomDefaultDataServiceConfig}
];

if (environment.production) {

  providers.push({ provide: ErrorHandler, useClass: SentryErrorHandler });
}
else {
  providers.push({ provide: ErrorHandler, useClass: ErrorHandler });
}

@NgModule({
  declarations: [AppComponent,
    HeaderComponent,
    WelcomeComponent,
    SidenavListComponent,
    SwaggerComponent,
    SystemComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MaterialModule,
    FlexLayoutModule,
    AuthModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers, {
      metaReducers,
      initialState: {
        router: {
          state: {
            url: '/',
            params: {},
            queryParams: {}
          },
          navigationId: 0
        }
      }
    }),
    EffectsModule.forRoot([AuthEffects, TimerEffects]),
    StoreRouterConnectingModule.forRoot({
      serializer: CustomSerializer,
      navigationActionTiming: NavigationActionTiming.PostActivation
    }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EntityDataModule.forRoot({})
  ],
  providers,
  bootstrap: [AppComponent]
})
export class AppModule { }
