import { NgModule } from '@angular/core';
import {
  MdcButtonModule,
  MdcIconModule,
  MdcIconButtonModule,
  MdcFabModule,
  MdcTopAppBarModule,
  MdcFormFieldModule,
  MdcTextFieldModule,
  MdcTypographyModule,
  MdcTabBarModule,
  MdcCardModule,
  MdcSelectModule,
  MdcSnackbarModule,
  MdcLinearProgressModule,
  MdcListModule,
  MdcDialogModule,
  MdcDrawerModule,
  MdcSliderModule,
  MdcChipsModule,
  MdcCheckboxModule,
  MDCDataTableModule,
  MdcImageListModule,
  MdcRippleModule
} from '@angular-mdc/web';

@NgModule({
  exports: [
    MdcButtonModule,
    MdcIconModule,
    MdcIconButtonModule,
    MdcFabModule,
    MdcTopAppBarModule,
    MdcFormFieldModule,
    MdcTextFieldModule,
    MdcTypographyModule,
    MdcTabBarModule,
    MdcCardModule,
    MdcSelectModule,
    MdcSnackbarModule,
    MdcLinearProgressModule,
    MdcListModule,
    MdcDialogModule,
    MdcDrawerModule,
    MdcIconModule,
    MdcSliderModule,
    MdcChipsModule,
    MdcCheckboxModule,
    MDCDataTableModule,
    MdcImageListModule,
    MdcRippleModule
  ]
})
export class MaterialModule {}
